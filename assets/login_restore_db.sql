-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 10, 2019 at 11:56 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ekushey_4.3_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_permission`
--

DROP TABLE IF EXISTS `account_permission`;
CREATE TABLE `account_permission` (
  `account_permission_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `account_permission`
--

INSERT INTO `account_permission` (`account_permission_id`, `name`, `description`) VALUES
(1, 'Manage Assigned Project', 'User can view and manage only assigned projects to him. Project status update, document upload/view, project discussion will be available to assigned projects'),
(2, 'Manage All Projects', ''),
(3, 'Manage Clients', ''),
(4, 'Manage Staffs', ''),
(5, 'Manage Payment', ''),
(6, 'Manage Assigned Support Ticket', ''),
(7, 'Manage All Support Tickets', '');

-- --------------------------------------------------------

--
-- Table structure for table `account_role`
--

DROP TABLE IF EXISTS `account_role`;
CREATE TABLE `account_role` (
  `account_role_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `account_permissions` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `account_role`
--

INSERT INTO `account_role` (`account_role_id`, `name`, `account_permissions`) VALUES
(4, 'New Staff', '1,3,4,5,6,'),
(5, 'Manager', '1,2,3,4,5,6,7,'),
(6, 'Director', '1,2,7,'),
(7, 'Programmer', '2,3,6,7,'),
(8, 'Support staff', '2,7,');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `email` longtext COLLATE utf8_unicode_ci,
  `password` longtext COLLATE utf8_unicode_ci,
  `phone` longtext COLLATE utf8_unicode_ci,
  `address` longtext COLLATE utf8_unicode_ci,
  `chat_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'offline',
  `owner_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 owner, 0 not owner'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `email`, `password`, `phone`, `address`, `chat_status`, `owner_status`) VALUES
(1, 'Carlos Melo', 'admin@example.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '(18) 2136-3414', '4010 Dufferin Toronto, M6H 4B6', 'offline', 1),
(2, 'Toni Meador', 'toni@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '+1 (207) 131-8036', '3171 Robson St-BC V6B 3K9', 'offline', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

DROP TABLE IF EXISTS `bookmark`;
CREATE TABLE `bookmark` (
  `bookmark_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `url` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `calendar_event`
--

DROP TABLE IF EXISTS `calendar_event`;
CREATE TABLE `calendar_event` (
  `calendar_event_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `user_type` longtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `start_timestamp` longtext COLLATE utf8_unicode_ci,
  `end_timestamp` int(11) DEFAULT NULL,
  `colour` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `calendar_event`
--

INSERT INTO `calendar_event` (`calendar_event_id`, `title`, `description`, `user_type`, `user_id`, `start_timestamp`, `end_timestamp`, `colour`) VALUES
(1, 'Office', 'Maintain office services.', 'admin', 1, '1554847200', 1564092000, NULL),
(2, 'Work time', 'This time is enough', 'admin', 1, '1554674400', 1563832800, NULL),
(3, 'Project creation time', 'Project upload', 'admin', 1, '1556668800', 1559260800, NULL),
(4, 'Office', 'Maintain office services.', 'admin', 1, '1575676800', 1579651200, '#279ACB'),
(5, 'Work time', 'This time is enough', 'admin', 1, '1573776000', 1595462400, '#FDA330'),
(6, 'Project creation time', 'Project upload', 'admin', 1, '1574899200', 1575072000, '#E93339');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` varchar(255) NOT NULL DEFAULT '',
  `to` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `client_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `email` longtext COLLATE utf8_unicode_ci,
  `password` longtext COLLATE utf8_unicode_ci,
  `address` longtext COLLATE utf8_unicode_ci,
  `phone` longtext COLLATE utf8_unicode_ci,
  `website` longtext COLLATE utf8_unicode_ci,
  `skype_id` longtext COLLATE utf8_unicode_ci,
  `facebook_profile_link` longtext COLLATE utf8_unicode_ci,
  `linkedin_profile_link` longtext COLLATE utf8_unicode_ci,
  `twitter_profile_link` longtext COLLATE utf8_unicode_ci,
  `short_note` longtext COLLATE utf8_unicode_ci,
  `chat_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'offline'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`client_id`, `name`, `email`, `password`, `address`, `phone`, `website`, `skype_id`, `facebook_profile_link`, `linkedin_profile_link`, `twitter_profile_link`, `short_note`, `chat_status`) VALUES
(1, 'Milton C. Blount', 'client@example.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '4544 Smith AvenueHamilton, ON L9H 1E6', '(48) 2554-5527', 'CampingFilm.com', 'https://www.skype.com/milton', 'https://www.facebook.com/milton', 'https://www.linkedin.com/milton', 'https://www.twitter.com/milton', 'Aperiam sit odio il', 'offline'),
(2, 'Layan Maysan Hanania', 'layan@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Rua Zélia, 294\r\nRecife-PE\r\n52140-600', '+1 (311) 791-9348', 'https://www.nafekogopazatyz.co.uk', 'https://www.skype.com/layan', 'https://www.facebook.com/layan', 'https://www.linkedin.com/layan', 'https://www.twitter.com/layan', 'I have several lists of this and that, and after a while it becomes hard for me to find what I want. I prefer my lists to be alphabetically ordered.', 'offline'),
(3, 'Sigourney Townsend', 'buvekeqaqu@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '7454 Somodor\r\nEötvös út 34.', '+1 (403) 547-6846', 'https://www.xop.me.uk', 'https://www.skype.com/sigourney', 'https://www.facebook.com/sigourney', 'https://www.linkedin.com/sigourney', 'https://www.twitter.com/sigourney', 'TRACKING NUMBERS\r\nUPS tracking number\r\n1Z 404 862 14 9383 965 2\r\nWestern Union MTCN\r\n4694330049', 'offline'),
(4, 'Rafa Sayyidah', 'rafa443@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Willem Albert Scholtenweg 193\r\n9636 BR  Zuidbroek', '+1 (961) 702-4903', 'https://www.jojiq.net', 'https://www.skype.com/rafa', 'https://www.facebook.com/rafa', 'https://www.linkedin.com/rafa', 'https://www.twitter.com/rafa', 'Non iure sunt id acc', 'offline'),
(5, 'Norman Cherry', 'coqubized@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Haagwinde 118\r\n9103 RE  Dokkum', '+1 (447) 316-1683', 'https://www.benowodiluhy.info', 'https://www.skype.com/norman', 'https://www.facebook.com/norman', 'https://www.linkedin.com/norman', 'https://www.twitter.com/norman', 'Proident molestias ', 'offline'),
(6, 'Vielka Duke', 'cola@mailinator.net', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Aalscholverstraat 128\r\n1121 EK  Landsmeer', '+1 (552) 892-4942', 'https://www.resafexew.ws', 'https://www.skype.com/vielka', 'https://www.facebook.com/vielka', 'https://www.linkedin.com/vielka', 'https://www.twitter.com/vielka', 'Magna consequatur V', 'offline'),
(7, 'Robin Barron', 'dopovece@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Antoon Slotstraat 77\r\n7131 BW  Lichtenvoorde', '+1 (179) 279-2884', 'https://www.jujahadesaze.org.au', 'https://www.skype.com/robin', 'https://www.facebook.com/robin', 'https://www.linkedin.com/robin', 'https://www.twitter.com/robin', 'Officiis sunt amet', 'offline'),
(8, 'Suhayr Khawlah', 'suhayr@mailinator.net', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Van Kinsbergenstraat 126\r\n6826 JD  Arnhem', '+1 (597) 318-8642', 'https://www.zywi.cm', 'https://www.skype.com/suhayr', 'https://www.facebook.com/suhayr', 'https://www.linkedin.com/suhayr', 'https://www.twitter.com/suhayr', 'Provident voluptate', 'offline'),
(9, 'Quamar Bonner', 'menukotyf@mailinator.net', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', 'Veniam est autem ad', '+1 (152) 621-8209', 'https://www.fisejox.com', 'https://www.skype.com', 'https://www.facebook.com', 'https://www.linkedin.com', 'https://www.twitter.com', 'Voluptatibus enim as', 'offline');

-- --------------------------------------------------------

--
-- Table structure for table `client_pending`
--

DROP TABLE IF EXISTS `client_pending`;
CREATE TABLE `client_pending` (
  `client_pending_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `email` longtext COLLATE utf8_unicode_ci,
  `password` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `email` longtext COLLATE utf8_unicode_ci,
  `address` longtext COLLATE utf8_unicode_ci,
  `phone` longtext COLLATE utf8_unicode_ci,
  `website` longtext COLLATE utf8_unicode_ci,
  `client_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `name`, `email`, `address`, `phone`, `website`, `client_id`) VALUES
(1, 'SBC Ltd', 'sbc03@gmail.com', '4440 Rogers RoadToronto, ON M6E 1R1', '613-532-6589', 'https://www.sbc.com', 1),
(2, 'RiO', 'rio@mailinator.com', '3187 Tanner StreetVancouver, BC V5R 2T4', '+1 (834) 221-8987', 'https://www.rio.com', 4),
(3, 'BINGO BRAND', 'bingo@mailinator.com', '6 Akiraho StreetMt EdenAuckland 1024', '+1 (215) 964-4318', 'https://www.bingobrand.org.uk', 5),
(4, 'ZEUDS Ltd.', 'zeuds@mailinator.com', 'Enzersdorfer Strasse 94312 GERERSDORF', '+1 (403) 784-8342', 'https://www.zeuds.in', 1),
(5, 'Aird IN', 'aird@mailinator.com', 'Grazer Strasse 302286 HARINGSEE', '+1 (824) 845-5764', 'https://www.aird.co', 7),
(6, 'LBDS Ltd.', 'lbds@mailinator.net', 'Prager Str 1485356 Freising', '+1 (706) 265-3121', 'https://www.lbds.org', 3);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency` (
  `currency_id` int(11) NOT NULL,
  `currency_code` longtext COLLATE utf8_unicode_ci,
  `currency_symbol` longtext COLLATE utf8_unicode_ci,
  `currency_name` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`currency_id`, `currency_code`, `currency_symbol`, `currency_name`) VALUES
(1, 'USD', '$', 'US dollar'),
(2, 'GBP', '£', 'Pound'),
(3, 'EUR', '€', 'Euro'),
(4, 'AUD', '$', 'Australian Dollar'),
(5, 'CAD', '$', 'Canadian Dollar'),
(6, 'JPY', '¥', 'Japanese Yen'),
(7, 'NZD', '$', 'N.Z. Dollar'),
(8, 'CHF', 'Fr', 'Swiss Franc'),
(9, 'HKD', '$', 'Hong Kong Dollar'),
(10, 'SGD', '$', 'Singapore Dollar'),
(11, 'SEK', 'kr', 'Swedish Krona'),
(12, 'DKK', 'kr', 'Danish Krone'),
(13, 'PLN', 'zł', 'Polish Zloty'),
(14, 'HUF', 'Ft', 'Hungarian Forint'),
(15, 'CZK', 'Kč', 'Czech Koruna'),
(16, 'MXN', '$', 'Mexican Peso'),
(17, 'CZK', 'Kč', 'Czech Koruna'),
(18, 'MYR', 'RM', 'Malaysian Ringgit');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
CREATE TABLE `document` (
  `document_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `file_name` longtext COLLATE utf8_unicode_ci,
  `file_type` longtext COLLATE utf8_unicode_ci,
  `class_id` longtext COLLATE utf8_unicode_ci,
  `teacher_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
CREATE TABLE `email_template` (
  `email_template_id` int(11) NOT NULL,
  `task` longtext COLLATE utf8_unicode_ci,
  `subject` longtext COLLATE utf8_unicode_ci,
  `body` longtext COLLATE utf8_unicode_ci,
  `instruction` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`email_template_id`, `task`, `subject`, `body`, `instruction`) VALUES
(1, 'new_project_opening', 'New project created', '<span>\r\n<div>Hello, [CLIENT_NAME], <br>we have created a new project with your account.<br><br>Project name : [PROJECT_NAME]<br>Please follow the link below to view status and updates of the project.<br>[PROJECT_LINK]</div></span>', ''),
(2, 'new_client_account_opening', 'Client account creation', '<span><div>Hi [CLIENT_NAME],</div></span>Your client account is created !<span>Please login to your client account panel here :&nbsp;<br></span>[SYSTEM_URL]<br>Login credential :<br>email : [CLIENT_EMAIL]<br>password : [CLIENT_PASSWORD]', ''),
(3, 'new_staff_account_opening', 'Staff account creation', '<span>\n<div><div>Hi [STAFF_NAME],</div>Your staff account is created !&nbsp;Please login to your staff account panel here :&nbsp;<br>[SYSTEM_URL]<br>Login credential :<br>email : [STAFF_EMAIL]<br>password : [STAFF_PASSWORD]<br></div></span>', ''),
(4, 'payment_completion_notification', 'Payment completion notification', '<span>\n<div>Your payment of invoice [INVOICE_NUMBER] is completed.<br>You can review your payment history here :<br>[SYSTEM_PAYMENT_URL]</div></span>', ''),
(5, 'new_support_ticket_notify_admin', 'New support ticket notification', 'Hi [ADMIN_NAME],<br>A new support ticket is submitted. Ticket code : [TICKET_CODE]<br><br>Review all opened support tickets here :<br>[SYSTEM_OPENED_TICKET_URL]<br>', ''),
(6, 'support_ticket_assign_staff', 'Support ticket assignment notification', 'Hi [STAFF_NAME],<br>A new support ticket is assigned. Ticket code : [TICKET_CODE]<br><br>Review all opened support tickets here :<br>[SYSTEM_OPENED_TICKET_URL]', ''),
(7, 'new_message_notification', 'New message notification.', 'A new message has been sent by [SENDER_NAME].<br><br><span class=\"wysiwyg-color-silver\">[MESSAGE]<br></span><br><span>To reply to this message, login to your account :<br></span>[SYSTEM_URL]', ''),
(8, 'password_reset_confirmation', 'Password reset notification', 'Hi [NAME],<br>Your password is reset. New password : [NEW_PASSWORD]<br>Login here with your new password :<br>[SYSTEM_URL]<br><br>You can change your password after logging in to your account.', ''),
(9, 'new_client_account_confirm', 'New Client account confirmed', '<span><div>Hi [CLIENT_NAME],</div></span>Your client account is confirmed!<span>Please login to your client account panel here :&nbsp;<br></span>[SYSTEM_URL]<br>', ''),
(10, 'new_admin_account_creation', 'Admin Account Creation', '<span><div>Hi [ADMIN_NAME],</div></span>Your admin account is created !<span>Please login to your admin account panel here :&nbsp;<br></span>[SYSTEM_URL]<br>Login credential :<br>email : [ADMIN_EMAIL]<br>password : [ADMIN_PASSWORD]', '');

-- --------------------------------------------------------

--
-- Table structure for table `expense_category`
--

DROP TABLE IF EXISTS `expense_category`;
CREATE TABLE `expense_category` (
  `expense_category_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expense_category`
--

INSERT INTO `expense_category` (`expense_category_id`, `title`, `description`) VALUES
(1, 'Salary', 'New 10 employee add'),
(2, 'Automobile', 'A car is a wheeled motor vehicle used for transportation.'),
(3, 'Disposal', 'Waste disposal officer: job description'),
(4, 'Credit card', 'A credit card is the most common way to access a line of credit.'),
(5, 'Dues & Subscriptions', 'A subscription is an amount of money that you pay'),
(6, 'Insurance', 'nsurance is a broad work category with many job types'),
(7, 'Internet', 'The Internet is not synonymous with World Wide Web');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `invoice_number` longtext COLLATE utf8_unicode_ci,
  `client_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `invoice_entries` longtext COLLATE utf8_unicode_ci,
  `creation_timestamp` longtext COLLATE utf8_unicode_ci,
  `due_timestamp` longtext COLLATE utf8_unicode_ci,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'paid or unpaid',
  `vat_percentage` longtext COLLATE utf8_unicode_ci,
  `discount_amount` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `message_id` int(11) NOT NULL,
  `message_thread_code` longtext,
  `message` longtext,
  `sender` longtext,
  `timestamp` longtext,
  `read_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 unread 1 read'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`message_id`, `message_thread_code`, `message`, `sender`, `timestamp`, `read_status`) VALUES
(1, '8c8936d3', 'hi', 'admin-1', '1554985764', 1),
(2, '8c8936d3', 'Hello', 'client-1', '1554985792', 1),
(3, 'aff14da3', 'hi', 'admin-1', '1554986071', 1),
(4, 'aff14da3', 'hello', 'staff-1', '1554986252', 1),
(5, '440475e5', 'hi', 'admin-1', '1555315078', 0),
(6, 'd3e58285', 'hi', 'admin-1', '1555315097', 0),
(7, 'f2c21e63', 'hi', 'admin-1', '1555315123', 0);

-- --------------------------------------------------------

--
-- Table structure for table `message_thread`
--

DROP TABLE IF EXISTS `message_thread`;
CREATE TABLE `message_thread` (
  `message_thread_id` int(11) NOT NULL,
  `message_thread_code` longtext COLLATE utf8_unicode_ci,
  `sender` longtext COLLATE utf8_unicode_ci,
  `reciever` longtext COLLATE utf8_unicode_ci,
  `last_message_timestamp` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `message_thread`
--

INSERT INTO `message_thread` (`message_thread_id`, `message_thread_code`, `sender`, `reciever`, `last_message_timestamp`) VALUES
(1, '8c8936d3', 'admin-1', 'client-1', NULL),
(2, 'aff14da3', 'admin-1', 'staff-1', NULL),
(3, '440475e5', 'admin-1', 'staff-3', NULL),
(4, 'd3e58285', 'admin-1', 'client-5', NULL),
(5, 'f2c21e63', 'admin-1', 'client-8', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE `note` (
  `note_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_create` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp_last_update` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `note`
--

INSERT INTO `note` (`note_id`, `title`, `note`, `user_type`, `user_id`, `timestamp_create`, `timestamp_last_update`) VALUES
(1, 'Project create', 'New project creation date in 28-11-2019 to 31-03-2020.', 'admin', 1, '1572861338', '1572862789'),
(3, 'Project update', 'Running project updating date in 01-02-2020 to 31-03-2020.', 'admin', 1, '1572861670', '1572862754'),
(4, '', '', 'staff', 1, '1573112615', ''),
(5, 'Upcoming App', '', 'client', 1, '1573129122', '1573129140');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `published_by` int(11) DEFAULT NULL,
  `visible_for` int(11) NOT NULL DEFAULT '1' COMMENT '1-all, 2-staff, 3-client',
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `title`, `description`, `published_by`, `visible_for`, `date_added`, `last_modified`) VALUES
(1, 'Discount provide', 'Permanent membership', 1, 1, 1554983744, NULL),
(2, 'Website', 'If my website is unsuccessful in running a staff or client, you can report it', 1, 1, 1554983924, NULL),
(3, 'New Features for project', 'Four separate note generators\r\nCreates musical melodies and patterns\r\nControl variations for note and rhythm\r\nUse freeze function to capture perfect motifs\r\nRecord performances to Reason’s main sequencer\r\nComes with a variety of inspiring patches to get you started', 1, 1, 1555395316, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `project_code` longtext COLLATE utf8_unicode_ci,
  `type` longtext COLLATE utf8_unicode_ci,
  `amount` int(11) NOT NULL DEFAULT '0',
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `payment_method` longtext COLLATE utf8_unicode_ci,
  `timestamp` longtext COLLATE utf8_unicode_ci,
  `milestone_id` int(11) DEFAULT NULL,
  `expense_category_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `project_code`, `type`, `amount`, `title`, `description`, `payment_method`, `timestamp`, `milestone_id`, `expense_category_id`, `client_id`, `company_id`) VALUES
(2, '8cfeb239d0', 'income', 45, 'Network packet tracer ', NULL, 'Paypal', '1574545200', 1, 4, 1, 1),
(8, 'd46ea9d9ad', 'income', 250, 'Initial Milestone', 'There you go.', 'Cash', '1574355200', 9, 5, 1, 3),
(9, '24565ab31b', 'income', 250, 'Second Milestone', 'Thanks', 'Paypal', '1574356200', 8, 7, 1, 2),
(10, '24565ab31b', 'income', 150, 'First Milestone', 'Thanks', 'Stripe', '1574256200', 7, 4, 1, 2),
(11, '24565ab31b', 'income', 4500, 'Final Milestone', 'Thanks for sticking with us', 'Manual', '1574156200', 10, 3, 1, 2),
(15, '3b63f60b86', 'income', 40, 'Data Engineer with python ', NULL, 'paypal', '1573084800', 2, 1, 1, 1),
(16, 'd46ea9d9ad', 'income', 2560, 'Final Milestone', NULL, 'paypal', '1573084800', 11, 2, 1, 3),
(17, 'ba40244e21', 'income', 35, 'Hotel management VRMO', NULL, 'paypal', '1573084800', 5, 1, 2, 2),
(18, '24565ab31b', 'expense', 30, 'Updating', NULL, 'Paypal', '1573157940', 2, 5, 1, 1),
(19, '24565ab31b', 'expense', 180, 'Payment getaway', NULL, 'Paypal', '1573157940', 3, 3, 1, 3),
(20, '24565ab31b', 'expense', 90, 'Google maps', NULL, 'Paypal', '1573157940', 12, 2, 1, 3),
(21, '24565ab31b', 'income', 45, 'IOS App development', NULL, 'paypal', '1573084800', 4, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `project_id` int(11) NOT NULL,
  `project_code` longtext COLLATE utf8_unicode_ci,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `demo_url` longtext COLLATE utf8_unicode_ci,
  `project_category_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `staffs` longtext COLLATE utf8_unicode_ci,
  `budget` int(11) NOT NULL DEFAULT '0',
  `timer_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 running 0stopped',
  `timer_starting_timestamp` longtext COLLATE utf8_unicode_ci,
  `total_time_spent` int(11) NOT NULL DEFAULT '0' COMMENT 'second',
  `progress_status` longtext COLLATE utf8_unicode_ci,
  `timestamp_start` longtext COLLATE utf8_unicode_ci,
  `timestamp_end` longtext COLLATE utf8_unicode_ci,
  `project_status` int(11) NOT NULL DEFAULT '1' COMMENT '1 for running, 0 for archived',
  `project_note` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `project_code`, `title`, `description`, `demo_url`, `project_category_id`, `client_id`, `company_id`, `staffs`, `budget`, `timer_status`, `timer_starting_timestamp`, `total_time_spent`, `progress_status`, `timestamp_start`, `timestamp_end`, `project_status`, `project_note`) VALUES
(1, '8cfeb239d0', 'Network packet tracer', '<p><font color=\"#1f2836\"> Network packet tracer, network packet tracer network, wan network packet tracer, create wan network packet tracer, implement firewall network packet tracer, steps wan network packet tracer, wide area network packet tracer, create wide area network packet tracer.</font><br></p>', 'http://ipsonics.com', NULL, 1, 1, '1,2,3,4,', 42, 0, NULL, 0, '69', '04/25/2019', '07/03/2019', 1, NULL),
(2, '3b63f60b86', 'Data Engineer with python', '<p><span style=\"color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">Hi looking for some one who can do good data modelling and has good python experience. We can talk about payment once the task is talked about..</span><br></p>', 'http://pintloop.com', NULL, 1, 1, '1,3,4,', 40, 0, NULL, 0, '45', '05/20/2019', '07/26/2019', 1, NULL),
(3, '80e3414016', 'Mobile stock take app ', '<p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">looking to create a mobile app to link with our inventory system.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">Similar to the examples in the files attached.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">Main purpose is for inventory management and stocktaking.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">Must be react native cross platform.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">API calls are HTML requests.</p>', 'http://findappjobs.com', NULL, 1, 3, '1,3,4,', 35, 0, NULL, 0, '33', '04/27/2019', '06/13/2019', 0, NULL),
(4, 'f7c7d22d86', 'Need Image to WordPress Design', '<p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">Hello wordpress developer,</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">i need urgent ui to wordpress design.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">i will send you ui [.ai] file for landing page to selected candidate.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">only apply if you can start work now.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">Thanks</p>', 'http://iuqoudk.com', NULL, 1, 2, '1,2,3,4,5,6,', 30, 0, NULL, 0, '50', '04/27/2019', '05/07/2019', 1, NULL),
(5, 'eac2d4ff03', 'Whmcs provision module -- 3', '<p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">i have a product panel with API. I have also a whmcs. I need to make a provision module so my whmcs and my other panel can communicate. the module is server module. and these function I need.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">create /terminate</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">enable disable</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">set the expiry date of the product same like whmcs next invoice date.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\"></p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">if you work with whmcs module before then plz apply</p>', 'http://pescasubmarina.com', NULL, 1, 5, '1,2,3,4,5,', 45, 0, NULL, 0, '90', '05/01/2019', '07/31/2019', 0, NULL),
(9, '0369bfe568', 'Node.js Queue System', '<p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">This is an queue system.</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">There are different pages with functions:</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">(1) kiosk pages: create queue ticket</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">(2) counter pages: show servicing ticket</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">(3) control pages: press to assign queue ticket to different counters</p><p style=\"margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">All frontend pages has been built by react.js.</p>', 'http://njuesc.com', NULL, 1, 4, '1,2,6,', 42, 0, NULL, 0, '95', '05/14/2019', '07/31/2019', 0, NULL),
(6, '073ed3e1b7', 'Website bot or some crawler solution', '<p><span style=\"color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">We working on server website. The website will offer VPS and Dedicated servers from our main provider. They have many dedicated servers from them on they website that is possible to customize per requirements. With this customization as well the price is different. As well they have the offer from different data centers. We need some solution like some website crawler or some bot to download all possible options from they offer and to be automatically uploaded on our website. We will provide complete documentation.</span><br></p>', 'http://marketingSlick.com', NULL, 2, 4, '2,4,', 25, 0, NULL, 0, '70', '04/19/2019', '06/12/2019', 1, NULL),
(7, 'a70b211c5a', 'Logo animation', '<p><span style=\"color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">I want to bring our logo alive for video introductions. Something not to over the top but very eye catching. Preferably want sound included (clanging metal would be good...) only want 3-4 seconds.</span><br></p>', 'http://plpkvzi.com', NULL, 1, 3, '1,3,4,5,', 30, 0, NULL, 0, '54', '04/30/2019', '07/17/2019', 0, NULL),
(8, '58cbc3ad65', 'Need a .net software ', '<p><p style=\"box-sizing: border-box; margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"></p></p><p style=\"box-sizing: border-box; margin-block-start: 0px; margin-block-end: 0px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Reading COM port for serial communication and converting data to excel file</p>', 'http://mamakels.com', NULL, 3, 2, '3,4,5,', 35, 0, NULL, 0, '60', '04/20/2019', '05/31/2019', 1, NULL),
(10, 'ba40244e21', 'Hotel management VRMO', 'The Hotel Felix is an upscale boutique hotel that wants to have a luxurious, sustainable and\r\ntrendy image. The lighting design needs to engage with the community, patrons and the architecture in\r\norder to create the desired atmosphere. Spring work will include a lighting depth, MAE supplemental\r\nstudy, Honors supplemental study, architectural breadth, mechanical breadth and an electrical depth.', 'http://vrmo.com', NULL, 2, 2, '2,3,6,', 35, 0, NULL, 0, '71', '04/25/2019', '06/28/2019', 1, NULL),
(11, 'd46ea9d9ad', 'Shopping mall order (pcwe)', '<p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\">Shopping mall shopping cart</p><p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\"></p><p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\">[login to view URL]</p><p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\">1.1Load commodity info for current user from server by call provided api.</p><p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\">1.2Load receipt address info for current user from server by call provided api.</p><p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\">1.3Show a input to get user input for balance amount.</p><p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\">1.4Submit the order.</p><p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\">1.5Must use React and material-ui.</p><p style=\"font-size: 16px; margin-bottom: 24px; line-height: 1.4; color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-variant-ligatures: normal; orphans: 2; widows: 2;\">April 17, 2019.<br></p>', 'http://shoppingmo.com', NULL, 1, 3, '1,2,4,5,', 30, 0, NULL, 0, '63', '04/25/2019', '07/17/2019', 1, NULL),
(12, '24565ab31b', 'IOS App development', '<p><span style=\"color: rgb(31, 40, 54); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 16px;\">Need to develop very simple iOS application. It will display Quran-A-Pak Tafseer contents. Currently contents are available in PDF format. There are six volumes and each PDF has approximately 400 pages. Developer can place the contents in Cloud, once user install and run application, he has the option to view online or download the of line copy. Developer can change the format if required. Look and Feel: 1- Main Page of app will have a background image of tafseer book.</span><br></p>', 'http://isoprofile.com', NULL, 1, 2, '1,2,3,4,5,6,', 45, 0, '1132403878', 31513508, '81', '04/11/2019', '06/30/2019', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_bug`
--

DROP TABLE IF EXISTS `project_bug`;
CREATE TABLE `project_bug` (
  `project_bug_id` int(11) NOT NULL,
  `project_code` longtext COLLATE utf8_unicode_ci,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `user_type` longtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `file` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL COMMENT '0 for pending, 1 for solved',
  `timestamp` longtext COLLATE utf8_unicode_ci,
  `assigned_staff` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_bug`
--

INSERT INTO `project_bug` (`project_bug_id`, `project_code`, `title`, `description`, `user_type`, `user_id`, `file`, `status`, `timestamp`, `assigned_staff`) VALUES
(1, '24565ab31b', 'Youtube video not response.', 'Assuming everything is up and running, there are a few common issues that can interrupt your service. The most common problem? Videos won’t play. This can be a real pain, for obvious reasons, especially for those who use YouTube on a regular basis.', 'client', 1, NULL, 0, '1572985140', NULL),
(2, '24565ab31b', '404 Error', 'http://localhost/ekushey_4.3_demo/index.php/client/projectroom/bug/24565ab31b#', 'client', 1, NULL, 0, '1573157940', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_category`
--

DROP TABLE IF EXISTS `project_category`;
CREATE TABLE `project_category` (
  `project_category_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_file`
--

DROP TABLE IF EXISTS `project_file`;
CREATE TABLE `project_file` (
  `project_file_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `name` longtext COLLATE utf8_unicode_ci,
  `visibility_client` int(11) NOT NULL DEFAULT '1' COMMENT '1visible 0hidden',
  `visibility_staff` int(11) NOT NULL DEFAULT '1' COMMENT '1visible 0hidden',
  `size` longtext COLLATE utf8_unicode_ci,
  `file_type` longtext COLLATE utf8_unicode_ci,
  `uploader_type` longtext COLLATE utf8_unicode_ci,
  `uploader_id` int(11) DEFAULT NULL,
  `timestamp_upload` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_message`
--

DROP TABLE IF EXISTS `project_message`;
CREATE TABLE `project_message` (
  `project_message_id` int(11) NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `project_id` int(11) DEFAULT NULL,
  `date` longtext COLLATE utf8_unicode_ci,
  `user_type` longtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `message_file_name` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_message`
--

INSERT INTO `project_message` (`project_message_id`, `message`, `project_id`, `date`, `user_type`, `user_id`, `message_file_name`) VALUES
(1, 'hi', 12, '15 Apr 2019', 'admin', 1, NULL),
(2, 'This project perfect for me.', 12, '15 Apr 2019', 'client', 1, NULL),
(3, 'Hi', 11, '15 Apr 2019', 'client', 1, NULL),
(4, 'hello man', 11, '15 Apr 2019', 'admin', 1, NULL),
(5, 'it\'s very good product', 11, '15 Apr 2019', 'client', 1, NULL),
(6, 'Thanks for telling me about my product.', 11, '15 Apr 2019', 'admin', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_milestone`
--

DROP TABLE IF EXISTS `project_milestone`;
CREATE TABLE `project_milestone` (
  `project_milestone_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `project_code` longtext COLLATE utf8_unicode_ci,
  `client_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `timestamp` longtext COLLATE utf8_unicode_ci,
  `currency_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0 for unpaid, 1 for paid',
  `note` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_milestone`
--

INSERT INTO `project_milestone` (`project_milestone_id`, `title`, `project_code`, `client_id`, `company_id`, `amount`, `timestamp`, `currency_id`, `status`, `note`) VALUES
(1, 'Network packet tracer ', '8cfeb239d0', 1, 1, 45, '1556668800', NULL, 1, 'Network packet tracer \r\n for fixed'),
(2, 'Data Engineer with python ', '3b63f60b86', 1, 1, 40, '1557878400', NULL, 1, 'Data Engineer with python new project'),
(3, 'Shopping mall order (pcwe)', 'd46ea9d9ad', 1, 3, 30, '1557878400', NULL, 1, ''),
(4, 'IOS App development', '24565ab31b', 1, 2, 45, '1557878400', NULL, 1, ''),
(5, 'Hotel management VRMO', 'ba40244e21', 2, 2, 35, '1559174400', NULL, 1, ''),
(7, 'First Milestone', '24565ab31b', 1, 2, 150, '1555113600', NULL, 1, 'It would be great if you pay the first milestone.'),
(8, 'Second Milestone', '24565ab31b', 1, 2, 250, '1555286400', NULL, 1, 'It would be great if you pay the second milestone.'),
(9, 'Initial Milestone', 'd46ea9d9ad', 1, 3, 250, '1555545600', NULL, 1, 'Great!'),
(10, 'Final Milestone', '24565ab31b', 1, 2, 4500, '1555632000', NULL, 1, 'Closing milestone.'),
(11, 'Final Milestone', 'd46ea9d9ad', 1, 3, 2560, '1555632000', NULL, 1, 'Thanks'),
(12, 'IOS App', '24565ab31b', 1, 2, 255, '1573603200', NULL, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `project_task`
--

DROP TABLE IF EXISTS `project_task`;
CREATE TABLE `project_task` (
  `project_task_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `staff_id` int(11) DEFAULT NULL,
  `complete_status` int(11) DEFAULT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp_start` longtext COLLATE utf8_unicode_ci,
  `timestamp_end` longtext COLLATE utf8_unicode_ci,
  `task_color` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_task`
--

INSERT INTO `project_task` (`project_task_id`, `project_id`, `title`, `description`, `staff_id`, `complete_status`, `status`, `timestamp_start`, `timestamp_end`, `task_color`) VALUES
(1, 12, 'For project', 'Project check', 1, 0, '', '1556323200', '1558569600', '#0acf97'),
(2, 12, 'Payment manipulation', 'Important', 0, 1, '', '1552435200', '1560470400', '#fa5c7c'),
(3, 11, 'This product Updating', '', 1, 0, '', '1556323200', '1559260800', '#6c757d');

-- --------------------------------------------------------

--
-- Table structure for table `project_timesheet`
--

DROP TABLE IF EXISTS `project_timesheet`;
CREATE TABLE `project_timesheet` (
  `project_timesheet_id` int(11) NOT NULL,
  `start_timestamp` longtext COLLATE utf8_unicode_ci,
  `end_timestamp` longtext COLLATE utf8_unicode_ci,
  `project_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_timesheet`
--

INSERT INTO `project_timesheet` (`project_timesheet_id`, `start_timestamp`, `end_timestamp`, `project_id`) VALUES
(2, '1132403844', '1132403849', 12),
(3, '1132403853', '1132403876', 12),
(4, '1132403878', '1163917356', 12);

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

DROP TABLE IF EXISTS `quote`;
CREATE TABLE `quote` (
  `quote_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `files` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quote_message`
--

DROP TABLE IF EXISTS `quote_message`;
CREATE TABLE `quote_message` (
  `quote_message_id` int(11) NOT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `file` longtext COLLATE utf8_unicode_ci,
  `user_type` longtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE `schedule` (
  `schedule_id` int(11) NOT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci,
  `user_id` longtext COLLATE utf8_unicode_ci,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `timestamp` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `type` longtext,
  `description` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `type`, `description`) VALUES
(1, 'system_name', 'Sistema Génesis'),
(2, 'system_title', 'Sistema Génesis'),
(3, 'address', 'Sydney, Australia'),
(4, 'phone', '2560565'),
(5, 'paypal_email', ''),
(6, 'currency', 'usd'),
(7, 'system_email', 'admin@example.com'),
(8, 'buyer', '[ your-codecanyon-username-here ]'),
(9, 'purchase_code', 'Don\'t-have'),
(10, 'language', 'english'),
(11, 'text_align', 'left-to-right'),
(12, 'system_currency_id', '1'),
(13, 'theme', NULL),
(14, 'stripe_publishable_key', ''),
(15, 'stripe_api_key', ''),
(16, 'dropbox_data_app_key', 'guhgjv76vt677968976x765'),
(17, 'skin_colour', 'default'),
(18, 'paypal_type', 'live'),
(27, 'smtp_host', 'ssl://smtp.googlemail.com'),
(24, 'paypal', '[{\"active\":\"1\",\"mode\":\"sandbox\",\"sandbox_client_id\":\"AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R\",\"production_client_id\":\"SomeId\"}]'),
(25, 'stripe_keys', '[{\"active\":\"1\",\"testmode\":\"on\",\"public_key\":\"pk_test_c6VvBEbwHFdulFZ62q1IQrar\",\"secret_key\":\"sk_test_9IMkiM6Ykxr1LCe2dJ3PgaxS\",\"public_live_key\":\"pk_live_xxxxxxxxxxxxxxxxxxxxxxxx\",\"secret_live_key\":\"sk_live_xxxxxxxxxxxxxxxxxxxxxxxx\"}]'),
(26, 'version', '4.3'),
(28, 'smtp_port', '465'),
(34, 'protocol', 'smtp'),
(35, 'smtp_user', 'Your real email'),
(36, 'smtp_pass', 'Your real password');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `email` longtext COLLATE utf8_unicode_ci,
  `password` longtext COLLATE utf8_unicode_ci,
  `account_role_id` int(11) DEFAULT NULL,
  `phone` longtext COLLATE utf8_unicode_ci,
  `skype_id` longtext COLLATE utf8_unicode_ci,
  `facebook_profile_link` longtext COLLATE utf8_unicode_ci,
  `twitter_profile_link` longtext COLLATE utf8_unicode_ci,
  `linkedin_profile_link` longtext COLLATE utf8_unicode_ci,
  `chat_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'offline'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `name`, `email`, `password`, `account_role_id`, `phone`, `skype_id`, `facebook_profile_link`, `twitter_profile_link`, `linkedin_profile_link`, `chat_status`) VALUES
(6, 'Jean J. Darst', 'reed@mailinator.net', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 4, '+1 (531) 217-2578', 'https://www.skype.com/reed', 'https://www.facebook.com/reed', 'https://www.twitter.com/reed', 'https://www.linkedin.com/reed', 'offline'),
(1, 'Christopher Scanlan', 'staff@example.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 5, '+1 (652) 824-7327', 'https://www.skype.com/christopher', 'https://www.facebook.com/christopher', 'https://www.twitter.com/christopher', 'https://www.linkedin.com/christopher', 'offline'),
(2, 'Nelle Christensen', 'fuzy@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 4, '+1 (516) 787-8634', 'https://www.skype.com/nelle', 'https://www.facebook.com/nelle', 'https://www.twitter.com/nelle', 'https://www.linkedin.com/nelle', 'offline'),
(3, 'Warren Marsh', 'warren@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 5, '+1 (399) 114-6983', 'https://www.skype.com/warren', 'https://www.facebook.com/warren', 'https://www.twitter.com/warren', 'https://www.linkedin.com/warren', 'offline'),
(4, 'Camden French', 'camden66@mailinator.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 5, '+1 (292) 807-9799', 'https://www.skype.com/camden', 'https://www.facebook.com/camden', 'https://www.twitter.com/camden', 'https://www.linkedin.com/camden', 'offline'),
(5, 'Shoshana Joyner', 'shoshana@mailinator.net', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 4, '+1 (584) 172-8871', 'https://www.skype.com/shoshana', 'https://www.facebook.com/shoshana', 'https://www.twitter.com/shoshana', 'https://www.linkedin.com/shoshana', 'offline');

-- --------------------------------------------------------

--
-- Table structure for table `support_canned_message`
--

DROP TABLE IF EXISTS `support_canned_message`;
CREATE TABLE `support_canned_message` (
  `support_canned_message_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `support_canned_message`
--

INSERT INTO `support_canned_message` (`support_canned_message_id`, `title`, `description`) VALUES
(1, 'Welcome message', '<span>Thank you very much for writing to us. This is Carlos Melo from <b>Ekushey Project Manager CRM</b>.</span>'),
(2, 'Waiting Message', 'I\'m forwarding your message to our developer team. They will be in touch with you soon.&nbsp;<br>Really appreciate your patience.'),
(3, 'Cpanel access request', 'Please provide your <b><u>application url, admin login and cpanel login credentials</u></b>. We will look into the issue.'),
(4, 'Customer not responding', 'Hello Comercial. We have been trying to contact you about this support request but we haven\'t heard back from you yet. Please let us know if we can be there for further assistance. Thanks.'),
(5, '500 internal server error', 'This is a file permission issue.\r\n\r\nMake sure that your root folders (application, assets, system, uploads) has the permission of 755 and \"index.php\" file has the permission of 644.\r\n\r\n\r\n\r\n'),
(6, 'Unable tusking custom project', 'This is a file permission issue.\r\n\r\nMake sure that your root folders (application, assets, system, uploads) has the permission of 755 and \"index.php\" file has the permission of 644.\r\n\r\n\r\n\r\nPlease be informed that currently our team is heavily engaged with couple of running projects.\r\n\r\nI am afraid that we are unable to take any new project at this moment.\r\n\r\nWe are sorry for the inconvenience.\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `team_subtask`
--

DROP TABLE IF EXISTS `team_subtask`;
CREATE TABLE `team_subtask` (
  `team_subtask_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `team_task_id` int(11) DEFAULT NULL,
  `subtask_status` int(11) NOT NULL DEFAULT '1' COMMENT '1 for incomplete , 0 for complete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `team_task`
--

DROP TABLE IF EXISTS `team_task`;
CREATE TABLE `team_task` (
  `team_task_id` int(11) NOT NULL,
  `task_title` longtext COLLATE utf8_unicode_ci,
  `task_note` longtext COLLATE utf8_unicode_ci,
  `assigned_staff_ids` longtext COLLATE utf8_unicode_ci,
  `creation_timestamp` longtext COLLATE utf8_unicode_ci,
  `due_timestamp` longtext COLLATE utf8_unicode_ci,
  `task_status` int(11) NOT NULL DEFAULT '1' COMMENT '0 for archived, 1 for running'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `team_task`
--

INSERT INTO `team_task` (`team_task_id`, `task_title`, `task_note`, `assigned_staff_ids`, `creation_timestamp`, `due_timestamp`, `task_status`) VALUES
(1, 'Confluence', 'Create, organize, and discuss work with your team', '1,2,3,5,', '1555884000', '1563314400', 1),
(2, 'Jira Software', 'Build better products with end-to-end traceability between your issues and project docs', '1,2,3,4,5,', '1556575200', '1563919200', 1),
(3, 'IT Teams', 'Share knowledge and solve problems faster with an integrated knowledge base and service desk', '2,4,', '1557784800', '1566511200', 0),
(4, 'Create project banner', 'Very important work', '1,2,5,', '1557446400', '1556582400', 0),
(5, 'Payment check', '', '1,3,6,', '1557878400', '1559174400', 0),
(6, 'Logo setup', '', '1,3,4,', '1555027200', '1559174400', 1),
(7, 'WordPress Design', 'Common project', '1,4,5,', '1558656000', '1561593600', 1),
(8, 'Mobile softwer', '', '2,4,6,', '1556150400', '1558569600', 1),
(9, 'Demo fix', 'Important', '1,4,5,6,', '1557878400', '1561593600', 0),
(10, 'project bug', 'fix bug', '2,4,', '1558656000', '1561507200', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team_task_file`
--

DROP TABLE IF EXISTS `team_task_file`;
CREATE TABLE `team_task_file` (
  `team_task_file_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `team_task_id` int(11) DEFAULT NULL,
  `upload_timestamp` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `ticket_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `ticket_code` longtext COLLATE utf8_unicode_ci,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'opened closed',
  `priority` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `client_id` int(11) DEFAULT NULL,
  `assigned_staff_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`ticket_id`, `title`, `ticket_code`, `status`, `priority`, `description`, `client_id`, `assigned_staff_id`, `project_id`, `timestamp`) VALUES
(2, 'I cannot install the system in my hosting', '8432e576ea5e18d', 'opened', 'medium', NULL, 1, NULL, 12, '05 Nov,2019'),
(3, 'DATABASE connection error', '6d240223f672ba0', 'opened', 'high', NULL, 1, 6, 12, '05 Nov,2019'),
(4, 'Can I restrict downloading of video from the websit', '930fcb116617b04', 'opened', 'high', NULL, 1, NULL, 9, '07 Nov,2019');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_message`
--

DROP TABLE IF EXISTS `ticket_message`;
CREATE TABLE `ticket_message` (
  `ticket_message_id` int(11) NOT NULL,
  `ticket_code` longtext COLLATE utf8_unicode_ci,
  `message` longtext COLLATE utf8_unicode_ci,
  `file` longtext COLLATE utf8_unicode_ci,
  `sender_type` longtext COLLATE utf8_unicode_ci,
  `sender_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ticket_message`
--

INSERT INTO `ticket_message` (`ticket_message_id`, `ticket_code`, `message`, `file`, `sender_type`, `sender_id`, `timestamp`) VALUES
(1, '8432e576ea5e18d', '<div><div><div>Good afternoon, I can not install the system on my server, even if I enter the data from my server and database the system gives me an error when installing, what problem can it have?</div></div></div><br>', NULL, 'client', 1, '05 Nov,2019'),
(2, '6d240223f672ba0', '<span>A Database Error Occurred<br>Unable to connect to your database server using the provided settings.</span>Filename: controllers/Install.php<span>Line Number: 147</span><br>', NULL, 'client', 1, '05 Nov,2019'),
(3, '6d240223f672ba0', '<span>Thank you very much for writing to us. This is Carlos Melo from <b>Ekushey Project Manager CRM</b>.</span>', NULL, 'admin', 1, '05 Nov,2019'),
(4, '6d240223f672ba0', 'Please provide your <b><u>application url, admin login and cpanel login credentials</u></b>. We will look into the issue.', NULL, 'staff', 1, '05 Nov,2019'),
(5, '930fcb116617b04', '<span>Can I restrict downloading of video from the website?<br>Can I use secured videos from Amazon S3 Video Bucket?</span><span>Thank you,<br>Jaywant Mahajan</span><br>', '4.jpg', 'client', 1, '07 Nov,2019'),
(6, '930fcb116617b04', '<span>Thank you very much for writing to us. This is Carlos Melo from <b>Ekushey Project Manager CRM</b>.</span>', NULL, 'admin', 1, '10 Nov,2019'),
(7, '930fcb116617b04', 'Please provide your <b><u>application url, admin login and cpanel login credentials</u></b>. We will look into the issue.', NULL, 'admin', 1, '10 Nov,2019');

-- --------------------------------------------------------

--
-- Table structure for table `todo`
--

DROP TABLE IF EXISTS `todo`;
CREATE TABLE `todo` (
  `todo_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `user` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_permission`
--
ALTER TABLE `account_permission`
  ADD PRIMARY KEY (`account_permission_id`);

--
-- Indexes for table `account_role`
--
ALTER TABLE `account_role`
  ADD PRIMARY KEY (`account_role_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
  ADD PRIMARY KEY (`bookmark_id`);

--
-- Indexes for table `calendar_event`
--
ALTER TABLE `calendar_event`
  ADD PRIMARY KEY (`calendar_event_id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `client_pending`
--
ALTER TABLE `client_pending`
  ADD PRIMARY KEY (`client_pending_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indexes for table `expense_category`
--
ALTER TABLE `expense_category`
  ADD PRIMARY KEY (`expense_category_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `message_thread`
--
ALTER TABLE `message_thread`
  ADD PRIMARY KEY (`message_thread_id`);

--
-- Indexes for table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `project_bug`
--
ALTER TABLE `project_bug`
  ADD PRIMARY KEY (`project_bug_id`);

--
-- Indexes for table `project_category`
--
ALTER TABLE `project_category`
  ADD PRIMARY KEY (`project_category_id`);

--
-- Indexes for table `project_file`
--
ALTER TABLE `project_file`
  ADD PRIMARY KEY (`project_file_id`);

--
-- Indexes for table `project_message`
--
ALTER TABLE `project_message`
  ADD PRIMARY KEY (`project_message_id`);

--
-- Indexes for table `project_milestone`
--
ALTER TABLE `project_milestone`
  ADD PRIMARY KEY (`project_milestone_id`);

--
-- Indexes for table `project_task`
--
ALTER TABLE `project_task`
  ADD PRIMARY KEY (`project_task_id`);

--
-- Indexes for table `project_timesheet`
--
ALTER TABLE `project_timesheet`
  ADD PRIMARY KEY (`project_timesheet_id`);

--
-- Indexes for table `quote`
--
ALTER TABLE `quote`
  ADD PRIMARY KEY (`quote_id`);

--
-- Indexes for table `quote_message`
--
ALTER TABLE `quote_message`
  ADD PRIMARY KEY (`quote_message_id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`schedule_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `support_canned_message`
--
ALTER TABLE `support_canned_message`
  ADD PRIMARY KEY (`support_canned_message_id`);

--
-- Indexes for table `team_subtask`
--
ALTER TABLE `team_subtask`
  ADD PRIMARY KEY (`team_subtask_id`);

--
-- Indexes for table `team_task`
--
ALTER TABLE `team_task`
  ADD PRIMARY KEY (`team_task_id`);

--
-- Indexes for table `team_task_file`
--
ALTER TABLE `team_task_file`
  ADD PRIMARY KEY (`team_task_file_id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `ticket_message`
--
ALTER TABLE `ticket_message`
  ADD PRIMARY KEY (`ticket_message_id`);

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`todo_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_permission`
--
ALTER TABLE `account_permission`
  MODIFY `account_permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `account_role`
--
ALTER TABLE `account_role`
  MODIFY `account_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
  MODIFY `bookmark_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `calendar_event`
--
ALTER TABLE `calendar_event`
  MODIFY `calendar_event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `client_pending`
--
ALTER TABLE `client_pending`
  MODIFY `client_pending_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_template`
--
ALTER TABLE `email_template`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `expense_category`
--
ALTER TABLE `expense_category`
  MODIFY `expense_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `message_thread`
--
ALTER TABLE `message_thread`
  MODIFY `message_thread_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `note`
--
ALTER TABLE `note`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `project_bug`
--
ALTER TABLE `project_bug`
  MODIFY `project_bug_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_category`
--
ALTER TABLE `project_category`
  MODIFY `project_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_file`
--
ALTER TABLE `project_file`
  MODIFY `project_file_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_message`
--
ALTER TABLE `project_message`
  MODIFY `project_message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `project_milestone`
--
ALTER TABLE `project_milestone`
  MODIFY `project_milestone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `project_task`
--
ALTER TABLE `project_task`
  MODIFY `project_task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_timesheet`
--
ALTER TABLE `project_timesheet`
  MODIFY `project_timesheet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quote`
--
ALTER TABLE `quote`
  MODIFY `quote_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quote_message`
--
ALTER TABLE `quote_message`
  MODIFY `quote_message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `schedule_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `support_canned_message`
--
ALTER TABLE `support_canned_message`
  MODIFY `support_canned_message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `team_subtask`
--
ALTER TABLE `team_subtask`
  MODIFY `team_subtask_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `team_task`
--
ALTER TABLE `team_task`
  MODIFY `team_task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `team_task_file`
--
ALTER TABLE `team_task_file`
  MODIFY `team_task_file_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ticket_message`
--
ALTER TABLE `ticket_message`
  MODIFY `ticket_message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `todo`
--
ALTER TABLE `todo`
  MODIFY `todo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
