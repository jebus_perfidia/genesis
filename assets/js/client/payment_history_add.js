$(document).ready(function($) {

    $('#pacienteHijos').change(function () { 
        ValidarTieneHijos()
    });

    $('#pacienteGenero').change(function () { 
        ValidarPacienteGenero()
    });

    $('#pacienteEmbarazo').change(function () { 
        ValidarPacienteEmbarazo()
    });

    $('#pacienteLactancia').change(function () { 
        ValidarPacienteLactancia()
    });

    $('#anticonceptivos').change(function () { 
        ValidarAnticonceptivos()
    });

    $('#menopausia').change(function () { 
        ValidarMenopausia()
    });

    ValidarTieneHijos()
    ValidarPacienteGenero()
    ValidarPacienteEmbarazo()
    ValidarPacienteLactancia()
    ValidarAnticonceptivos()
    ValidarMenopausia()
});

function ValidarTieneHijos() 
{
    var tieneHijos = $('#pacienteHijos').val()

    if (tieneHijos == 'si') 
    {
        $('#lblCuantosHijos').removeClass('hide')
        $('#divCantidadHijos').removeClass('hide')
        $('#cantidadHijos').val('');
    }
    else 
    {
        $('#lblCuantosHijos').addClass('hide')
        $('#divCantidadHijos').addClass('hide')
    }
}

function ValidarPacienteGenero()
{
    console.log('Holis')
    var pacienteGenero = $('#pacienteGenero').val()
    
    if (pacienteGenero == 'femenino')
    {
        $('#divEmbarazo').removeClass('hide')
        $('#divLactancia').removeClass('hide')
        $('#DivGineObs').removeClass('hide')
    }
    else
    {
        $('#divEmbarazo').addClass('hide');
        $('#divLactancia').addClass('hide');
        $('#DivGineObs').addClass('hide');
    }
    $("#pacienteEmbarazo").val('').change();
    $("#pacienteLactancia").val('').change();

    $("#edadMenarca").val('');
    $("#ultimaMestruacion").val('');
    $("#regular").val('').change();
    $("#anticonceptivos").val('').change();
    $("#menopausia").val('').change();
}

function ValidarPacienteEmbarazo()
{
    var pacienteEmbarazo = $('#pacienteEmbarazo').val()
    
    if (pacienteEmbarazo == 'si')
    {
        $('#divSemanaEmbarazo').removeClass('hide');
    }
    else
    {
        $('#divSemanaEmbarazo').addClass('hide')
    }
    $('#semanaEmbarazo').val('');
}

function ValidarPacienteLactancia()
{
    var pacienteLactancia = $('#pacienteLactancia').val()
    
    if (pacienteLactancia == 'si')
    {
        $('#divSemanaLactancia').removeClass('hide');
    }
    else
    {
        $('#divSemanaLactancia').addClass('hide')
    }
    $('#semanaLactancia').val('');
}

function ValidarAnticonceptivos()
{
    var anticonceptivos = $('#anticonceptivos').val()
    
    if (anticonceptivos == 'si')
    {
        $('#divTipoAnticonceptivos').removeClass('hide');
    }
    else
    {
        $('#divTipoAnticonceptivos').addClass('hide')
    }
    $('#tipoAnticonceptivos').val('');
}

function ValidarMenopausia()
{
    var menopausia = $('#menopausia').val()
    
    if (menopausia == 'si')
    {
        $('#divFechaMenopausia').removeClass('hide');
    }
    else
    {
        $('#divFechaMenopausia').addClass('hide')
    }
    $('#fechaMenopausia').val('');
}