<?php
$profile_info  =  $this->db->get_where('empresas', array('idEmpresa' => $param2))->result_array();
foreach ($profile_info as $row) : ?>

  <div class="profile-env">

    <header class="row">

      <!-- <div class="col-sm-3">
			
			<a href="#" class="profile-picture">
				<img src="<?php echo $this->crud_model->get_image_url('staff', $row['staff_id']); ?>" 
                	class="img-responsive img-circle" />
			</a>
			
		</div> -->

      <div class="col-sm-5" style=" text-align:center;">
        <h1>Perfil De Empresa</h1>
        <ul class="profile-info-sections">
          <li style="padding:0px; margin:0px;">
            <div class="profile-name">
              <h3><?php echo $row['nombre']; ?></h3>
            </div>
          </li>
        </ul>

      </div>


    </header>

    <section class="profile-info-tabs">

      <div class="row">

        <div class="">
          <br>
          <h4>
            <table class="table ">
              <tr>
                <!-- <td width="40%">
    								    <i class="entypo-paper-plane"></i> &nbsp;
    								        <?php echo get_phrase('role'); ?></td>
                          <td>
                            <b>
                              <?php if ($row['owner_status'] == 1) echo get_phrase('owner');
                              if ($row['owner_status'] == 0) echo get_phrase('administrator');
                              ?>

                            </b>
                          </td> -->
              </tr>

              <tr>
                <td>
                  <i class="entypo-doc-text"></i> &nbsp;
                  <?php echo 'RFC' ?></td>
                <td>
                  <b><?php echo $row['rfc']; ?></b>
                </td>
              </tr>

              <tr>
                <td>
                  <i class="entypo-location"></i> &nbsp;
                  <?php echo get_phrase('Domicilio'); ?></td>
                <td>
                  <b><?php echo $row['domicilio']; ?></b>
                </td>
              </tr>

              <tr>
                <td>
                  <i class="entypo-clock"></i> &nbsp;
                  <?php echo 'Tiempo De Contrato'; ?></td>
                <td>
                  <b><?php echo $row['tiempoContrato']; ?></b>
                </td>
              </tr>


              <tr>
                <td>
                  <i class="entypo-briefcase"></i> &nbsp;
                  <?php echo 'Nutriólogo asignado'; ?></td>
                <td>
                  <b><?php
                  $nombrenutriologo  =  $this->db->get_where('admin', array('admin_id' => $row['admin_id']))->result_array();
                  foreach ($nombrenutriologo as $row2) :
                  echo $row2['nombre'].' '.$row2['aPaterno']; endforeach; ?></b>
                </td>
              </tr>


              <tr>
                <?php if ($row['estatus'] == 'activa') { ?>
                  <td>
                    <i class="entypo-lock-open"></i> &nbsp;
                    <?php echo get_phrase('Estatus'); ?></td>
                  <td>
                    <b><?php echo $row['estatus']; ?></b>
                  </td>
                <?php } elseif ($row['estatus'] == 'deshabilitada') { ?>
                  <td>
                    <i class="entypo-lock"></i> &nbsp;
                    <?php echo get_phrase('Estatus'); ?></td>
                  <td>
                    <b><?php echo $row['estatus']; ?></b>
                  </td>
                <?php }  ?>
              </tr>



            </table>
        </div>
      </div>
    </section>
    </h4>


  </div>


<?php endforeach; ?>