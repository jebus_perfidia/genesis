		<div class="panel panel-primary" id="charts_env" style="padding: 0px !important; margin: 0px;">
		    <div class="panel-heading">
		        <div class="panel-title">
		            <i class="fa fa-user"></i>
		            <?php echo 'Clientes del sistema'; ?>
		        </div>
		    </div>
		    <div class="panel-default">
		        <div class="table-responsive">
		            <table class="table datatable table-hover specialCollapse" id="table_export">
		                <thead>
		                    <tr>
		                        <th style="width:30px;">
		                        </th>
		                        <th>
		                            <div><?php echo 'Nombre'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Apellido Paterno'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Apellido Materno'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Domicilio'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Teléfono 1'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Teléfono 2'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Correo'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Usuario'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Tipo'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'CURP'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'RFC'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Administrador empresa'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Empresa'; ?></div>
		                        </th>
		                        <th>
		                            <div><?php echo 'Fecha de nacimiento'; ?></div>
		                        </th>

		                        <th>
		                            <div><?php echo get_phrase('Opciones'); ?></div>
		                        </th>
		                    </tr>
		                </thead>
		                <tbody>
		                    <!-- Obtener datos de usuario y la empresa a la que pertenece con un JOIN -->
		                    <?php
                            $this->db->select('admin.*,empresas.nombreEmpresa');
                            $this->db->from('admin');
                            $this->db->join('empresas', 'admin.idEmpresa=empresas.idEmpresa', 'left');
							$this->db->where('tipo','cliente');
							$this->db->order_by('admin_id','asc');
                            /* $this->db->where('tbl_usercategory','admin'); */
                            $query = $this->db->get();
                            $admins = $query->result_array();
                            $counter = 1;
                            foreach ($admins as $row) :


                                /* $counter = 1;
				$admins	=	$this->db->get('admin' )->result_array();
				foreach($admins as $row): */
                            ?>
		                        <tr>
		                            <td style="width:30px;">
		                                <?php echo $counter++; ?>
		                            </td>
		                            <td><?php echo $row['nombre']; ?></td>
		                            <td><?php echo $row['aPaterno']; ?></td>
		                            <td><?php echo $row['aMaterno']; ?></td>
		                            <td><?php echo $row['domicilio']; ?></td>
		                            <td><?php echo $row['telefono1']; ?></td>
		                            <td><?php echo $row['telefono2']; ?></td>
		                            <td><?php echo $row['email']; ?></td>
		                            <td><?php echo $row['usuario']; ?></td>
		                            <td><?php echo $row['tipo']; ?></td>
		                            <td><?php echo $row['curp']; ?></td>
		                            <td><?php echo $row['rfc']; ?></td>
		                            <td><?php
                                        if (($row['adminEmpresa'] == 0) || ($row['adminEmpresa'] == '')) {
                                            echo 'No';
                                        } else {
                                            echo 'Sí';
                                        }
                                        // echo $row['adminEmpresa'];($row['adminEmpresa'==1])
                                        ?></td>
		                            <td><?php echo $row['nombreEmpresa']; ?></td>
		                            <td><?php echo $row['fechaNacimiento']; ?></td>
		                            <!-- <td>
		             <?php if ($row['email'] != '') : ?>
		              <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
		                  data-original-title="<?php echo get_phrase('send_email'); ?>"	
		                  href="mailto:<?php echo $row['email']; ?>" style="color:#bbb;">
		                          <i class="entypo-mail"></i>
		                 </a>
		             <?php endif; ?>
		             <?php if ($row['phone'] != '') : ?>
		              <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
		                  data-original-title="<?php echo get_phrase('call_phone'); ?>"	
		                  href="tel:<?php echo $row['phone']; ?>" style="color:#bbb;">
		                          <i class="entypo-phone"></i>
		                 </a>
		             <?php endif; ?>
		           </td> -->
		                            <!--   <td>
		           		<?php if ($row['owner_status'] == 1) : ?>
		           			<div class="badge badge-info">
		           				<?php echo get_phrase('owner'); ?>
		           			</div>
		           		<?php endif; ?>
		           		<?php if ($row['owner_status'] == 0) : ?>
		           			<div class="badge badge-default">
		           				<?php echo get_phrase('administrator'); ?>
		           			</div>
		           		<?php endif; ?>
		           </td> -->
		                            <td>
		                                <div class="btn-group">
		                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
		                                        Acciones <span class="caret"></span>
		                                    </button>
		                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

		                                        <!-- PROFILE LINK -->
		                                        <li>
		                                            <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/client_profile/' . $row['admin_id']); ?>');">
		                                                <i class="entypo-user"></i>
		                                                <?php echo get_phrase('Perfil'); ?>
		                                            </a>
		                                        </li>

		                                      
		                                            <!-- EDITING LINK -->
		                                            <li>
		                                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/client_edit/' . $row['admin_id']); ?>');">
		                                                    <i class="entypo-pencil"></i>
		                                                    <?php echo get_phrase('Editar'); ?>
		                                                </a>
		                                            </li>

		                                        


		                                      
		                                            <!-- DELETION LINK -->
		                                            <li>
		                                                <a href="#" onclick="confirm_modal('<?php echo site_url('staff/staff_clients/delete/' . $row['admin_id']); ?>' , '<?php echo site_url('staff/reload_client_list'); ?>');">
		                                                    <i class="entypo-trash"></i>
		                                                    <?php echo get_phrase('Eliminar'.$param2); ?>
		                                                </a>
		                                            </li>
		                                        

		                                    </ul>
		                                </div>
		                            </td>
		                        </tr>
		                    <?php endforeach; ?>
		                </tbody>
		            </table>
		        </div>
		    </div>
		</div>




		<!-- calling ajax form submission plugin for specific form -->
		<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/neon-custom-ajax.js'); ?>"></script>
		<script type="text/javascript">
		    jQuery(document).ready(function($) {
		        //convert all checkboxes before converting datatable
		        replaceCheckboxes();

		        // Highlighted rows
		        $("#table_export tbody input[type=checkbox]").each(function(i, el) {
		            var $this = $(el),
		                $p = $this.closest('tr');

		            $(el).on('change', function() {
		                var is_checked = $this.is(':checked');

		                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
		            });
		        });

		        // convert datatable
		        var datatable = $("#table_export").dataTable({
		            "scrollX": true,
		            "sPaginationType": "bootstrap",
		            /*  "scrollX": true, */
		            // "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
		            // "aoColumns": [
		            // 	{ "bSortable": false}, 	//0,checkbox
		            // 	{ "bVisible": true},		//1,name
		            // 	{ "bVisible": true},		//2,role
		            // 	{ "bVisible": true},		//3,contact
		            // 	{ "bVisible": true}		//4,option
		            // ],
		            "oTableTools": {
		                "aButtons": [

		                    {
		                        "sExtends": "xls",
		                        "mColumns": [1, 2, ]
		                    },
		                    {
		                        "sExtends": "pdf",
		                        "mColumns": [1, 2]
		                    },
		                    {
		                        "sExtends": "print",
		                        "fnSetText": "Press 'esc' to return",
		                        "fnClick": function(nButton, oConfig) {
		                            datatable.fnSetColumnVis(0, false);
		                            datatable.fnSetColumnVis(3, false);
		                            datatable.fnSetColumnVis(6, false);

		                            this.fnPrint(true, oConfig);

		                            window.print();

		                            $(window).keyup(function(e) {
		                                if (e.which == 27) {
		                                    datatable.fnSetColumnVis(0, true);
		                                    datatable.fnSetColumnVis(3, true);
		                                    datatable.fnSetColumnVis(6, true);
		                                }
		                            });
		                        },

		                    },
		                ]
		            },

		        });

		        //customize the select menu
		        $(".dataTables_wrapper select").select2({
		            minimumResultsForSearch: -1
		        });




		    });
		</script>