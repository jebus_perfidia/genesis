<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('Crear nuevo cliente'); ?>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(site_url('staff/staff_clients/create'), array('class' => 'form-horizontal form-groups-bordered ajax-submit', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Nombre *'; ?></label>

                    <div class="col-sm-7">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                            <input type="text" class="form-control" name="name" value="" autofocus placeholder="Introduzca el nombre completo" title="Este campo es obligatorio" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Apellido Paterno *'; ?></label>
                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                            <input type="text" class="form-control" name="aPaterno" value="" placeholder="Introduzca el apellido paterno" title="Este campo es obligatorio" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Apellido Materno *'; ?></label>
                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                            <input type="text" class="form-control" name="aMaterno" value="" placeholder="Introduzca el apellido materno" title="Este campo es obligatorio" required>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Domicilio'; ?></label>
                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-location"></i></span>
                            <input type="text" class="form-control" name="domicilio" value="" placeholder="Introduzca el domicilio" title="Este campo es opcional">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Teléfono 1 *'; ?></label>
                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-phone"></i></span>
                            <input type="text" class="form-control" pattern="[0-9]{10}" name="telefono1" value="" placeholder="Introduzca el teléfono a 10 dígitos" title="Este campo es obligatorio" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Teléfono 2'; ?></label>
                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-phone"></i></span>
                            <input type="text" class="form-control" pattern="[0-9]{10}" name="telefono2" value="" placeholder="Introduzca el teléfono a 10 dígitos" title="Este campo es opcional">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Correo *'; ?></label>
                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-mail"></i></span>
                            <input type="text" class="form-control" name="email" value="" placeholder="Introduzca el correo electrónico" title="Este campo es obligatorio" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Usuario *'; ?></label>
                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                            <input type="text" class="form-control" name="usuario" value="" pattern="[A-Za-z0-9]{1,150}" placeholder="Solo letras y números" title="Este campo es obligatorio" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-4 control-label"><?php echo get_phrase('password *'); ?></label>

                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-key"></i></span>
                            <input type="password" class="form-control" name="password" value="" pattern="{5,150}" placeholder="Introduzca una contraseña" title="Este campo es obligatorio" required>
                        </div>
                        Mínimo 5 caracteres.
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-4 control-label"><?php echo 'CURP'; ?></label>

                    <div class="col-sm-7">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="entypo-doc-text"></i></span>
                            <input type="text" class="form-control" onkeyup="mayus(this);" pattern="[A-Za-z0-9]{18}" name="curp" value="" placeholder="CURP a 18 dígitos" title="Este campo es opcional">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-4 control-label"><?php echo 'RFC'; ?></label>

                    <div class="col-sm-7">
                        <div class="input-group ">
                            <span class="input-group-addon"><i class="entypo-doc-text"></i></span>
                            <input type="text" class="form-control" onkeyup="mayus(this);" pattern="[A-Za-z0-9]{12,13}" name="rfc" placeholder="<?php echo 'RFC a 12 o 13 dígitos'; ?>" title="Este campo es opcional">
                        </div>
                    </div>
                </div>

                <script>
                    function mayus(e) {
                        e.value = e.value.toUpperCase();
                    }

                    // function habilitar(value) {
                    // 	if (value == true) {
                    // 		// habilitamos
                    // 		document.getElementById("idEmpresa").disabled = false;
                    // 	} else if (value == false) {
                    // 		// deshabilitamos
                    // 		document.getElementById("idEmpresa").disabled = true;
                    // 	}
                    // }
                </script>



                <input name="adminEmpresa" type="hidden" value="0">

                <!-- 
				<div class="form-group">
					<label for="field-2" class="col-sm-4 control-label"><?php echo 'RH'; ?></label>

					<div class="col-sm-7">
						<div class="input-group "> -->
                <!-- <span class="input-group-addon"><i class="entypo-doc-text"></i></span> -->
                <!-- <input type="checkbox" name="adminEmpresa" value="1" onchange="habilitar(this.checked);" checked> Administrador de empresa (RH) <br> (Solo para usuario tipo cliente) -->
                <!-- <input type="checkbox" name="adminEmpresa" value="1" checked> Administrador de empresa (RH) <br>  (Solo para usuario tipo cliente) -->

                <!-- <input type="text" class="form-control" onkeyup="mayus(this);" pattern="[A-Za-z0-9]{12,13}" name="rfc" value="" placeholder="<?php echo 'Introduzca RFC del usuario'; ?>" title="Este campo es opcional"> -->
                <!-- </div>
					</div>
				</div> -->

                <!-- <input type="checkbox" name="transporte" value="1">Coche -->

                <!-- <div class="form-group">
                    <label for="field-2" class="col-sm-4 control-label"><?php echo 'Usuario RH'; ?></label>
                    <div class="col-sm-7">
                        <select class="selectboxit" name="adminEmpresa">
                            <option value="" selected disabled hidden>Seleccione Si/No</option>
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                        Seleccionar solo si el usuario es cliente.
                    </div>
                </div> -->

                <!-- <input name="idEmpresa" type="hidden" value=""> -->

                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label"><?php echo 'Empresa'; ?></label>
                    <div class="col-sm-7">
                        <select class="selectboxit" name="idEmpresa" id="idEmpresa" required>
                            <option><?php echo 'Selecciona una empresa...'; ?></option>
                            <?php
                            $empresas        =    $this->db->get('empresas')->result_array();
                            foreach ($empresas as $row2) :
                            ?>
                                <option value="<?php echo $row2['idEmpresa']; ?>" <?php if ($row['idEmpresa'] == $row2['idEmpresa']) echo 'seleccionar'; ?>>
                                    <?php echo $row2['nombreEmpresa']; ?></option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>

                <!-- <div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Empresa'; ?></label>
                        
						<div class="col-sm-7">
                      	<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
								<input type="text" class="form-control" name="idEmpresa" value=""  >
							</div>
						</div> 
					</div> -->



                <!-- <div class="form-group">
					<label for="field-2" class="col-sm-4 control-label"><?php echo 'Administrador de empresa'; ?></label>

					<div class="col-sm-7">
						<div class="input-group ">
							<span class="input-group-addon"><i class="entypo-key"></i></span>
							<input type="text" class="form-control" name="adminEmpresa" value="">
						</div>
					</div>
				</div> -->

                <div class="form-group">
                    <label for="field-2" class="col-sm-4 control-label"><?php echo 'Fecha de nacimiento'; ?></label>

                    <div class="col-sm-7">
                        <div class="input-group ">

                            <span class="input-group-addon"><i class="entypo-clock"></i></span>
                            <input type="date" class="form-control" name="fechaNacimiento" value="">
                        </div>
                    </div>
                </div>


                <input name="tipo" type="hidden" value="cliente">

                <!-- 

                
                <div class="form-group">
                    <label for="field-2" class="col-sm-4 control-label"><?php echo 'Tipo *'; ?></label>
                    <div class="col-sm-7">
                        <select class="selectboxit" name="tipo" required title="Seleccione el tipo" required>
                            <option value="" selected disabled hidden>Seleccione el tipo...</option>
                            <option value="administrador">administrador</option>
                            <option value="staff">staff</option>
                            <option value="nutriologo">nutriologo</option>
                            <option value="chef">chef</option>
                            <option value="cliente">cliente</option>
                        </select>
                    </div>
                </div> -->


                <!-- 
						<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Tipo'; ?></label>
                        
						<div class="col-sm-7">
                      	<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-user"></i></span>
								<input type="text" class="form-control" name="tipo" value=""  >
							</div>
						</div> 
					</div> -->

                <!-- 	
					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('photo'); ?></label>
                        
						<div class="col-sm-7">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<img src="uploads/user.jpg" alt="...">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
							</div>
						</div>
					</div> -->

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-7">
                        <button type="submit" class="btn btn-info" id="submit-button"><?php echo get_phrase('Agregar cliente'); ?></button>
                        <span id="preloader-form"></span>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script>
    // url for refresh data after ajax form submission
    var post_refresh_url = '<?php echo site_url('staff/reload_client_list'); ?>';
    var post_message = 'Usuario creado correctamente';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>