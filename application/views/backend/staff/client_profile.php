<?php

$this->db->select('admin.*,empresas.nombreEmpresa');
$this->db->from('admin');
$this->db->join('empresas', 'admin.idEmpresa=empresas.idEmpresa', 'left');
$this->db->where('admin.admin_id', $param2);
// $this->db->where('admin_id', $param2);
$query = $this->db->get();
$profile_info = $query->result_array();
$counter = 1;
foreach ($profile_info as $row) :


    /* $profile_info	=	$this->db->get_where('admin' , array('admin_id' => $param2))->result_array();
foreach($profile_info as $row):
 */
?>

    <div class="profile-env">

        <header class="row">

            <!-- 	<div class="col-sm-3">
			
			<a href="#" class="profile-picture">
				<img src="<?php echo $this->crud_model->get_image_url('admin', $row['admin_id']); ?>" 
                	class="img-responsive img-circle" />
			</a>
			
		</div>
		 -->

            <div class="col-sm-5" style=" text-align:center;">
                <h1>Perfil Personal</h1>
                <ul class="profile-info-sections">
                    <li style="padding:0px; margin:0px;">
                        <div class="profile-name">
                            <h3><?php echo $row['nombre'];
                                echo ' '; ?> <br> <?php echo $row['aPaterno'];
                                                    echo ' ';
                                                    echo $row['aMaterno']; ?></h3>
                        </div>
                    </li>
                </ul>

            </div>


        </header>

        <section class="profile-info-tabs">

            <div class="row">

                <div class="">
                    <br>
                    <h4>
                        <table class="table ">
                            <tr>
                                <!-- <td width="40%">
    								    <i class="entypo-paper-plane"></i> &nbsp;
    								        <?php echo get_phrase('role'); ?></td>
                          <td>
                            <b>
                              <?php if ($row['owner_status'] == 1) echo get_phrase('owner');
                                if ($row['owner_status'] == 0) echo get_phrase('administrator');
                                ?>

                            </b>
                          </td> -->
                            </tr>

                            <tr>
                                <td>
                                    <i class="entypo-user"></i> &nbsp;
                                    <?php echo get_phrase('Tipo'); ?></td>
                                <td>
                                    <b><?php echo $row['tipo']; ?></b>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <i class="entypo-mail"></i> &nbsp;
                                    <?php echo get_phrase('Correo'); ?></td>
                                <td>
                                    <b><?php echo $row['email']; ?></b>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <i class="entypo-phone"></i> &nbsp;
                                    <?php echo get_phrase('Tel&eacute;fono'); ?></td>
                                <td>
                                    <b><?php echo $row['telefono1']; ?></b>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <i class="entypo-phone"></i> &nbsp;
                                    <?php echo get_phrase('Tel&eacute;fono 2'); ?></td>
                                <td>
                                    <b><?php echo $row['telefono2']; ?></b>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <i class="entypo-location"></i> &nbsp;
                                    <?php echo get_phrase('Domicilio'); ?></td>
                                <td>
                                    <b><?php echo $row['domicilio']; ?></b>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <i class="entypo-user"></i> &nbsp;
                                    <?php echo get_phrase('Usuario'); ?></td>
                                <td>
                                    <b><?php echo $row['usuario']; ?></b>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <i class="entypo-doc-text"></i> &nbsp;
                                    <?php echo get_phrase('RFC'); ?></td>
                                <td>
                                    <b><?php echo $row['rfc']; ?></b>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <i class="entypo-doc-text"></i> &nbsp;
                                    <?php echo get_phrase('CURP'); ?></td>
                                <td>
                                    <b><?php echo $row['curp']; ?></b>
                                </td>
                            </tr>



                            <?php

                            if (($row['tipo'] == 'cliente') && ($row['adminEmpresa']) == 1) { ?>

                                <tr>
                                    <td>
                                        <i class="entypo-flow-tree"></i> &nbsp;
                                        <?php echo 'Administrador de empresa'; ?></td>
                                    <td>
                                        <b><?php echo 'Módulos de empresa activados'; ?></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="entypo-key"></i> &nbsp;
                                        <?php echo get_phrase('Nombre de la empresa'); ?></td>
                                    <td>
                                        <b><?php echo $row['nombreEmpresa']; ?></b>
                                    </td>

                                <?php } ?>
                                <br>
                                <br>
                                </tr>

                                <?php
                                if ($row['tipo'] == 'nutriologo') { ?>
                                    <tr>
                                        <td>
                                            <i class="entypo-flow-tree"></i> &nbsp;
                                            <?php echo get_phrase('Empresas supervisadas:'); ?></td>
                                        <!-- <td>
											<b><?php echo $row['curp']; ?></b>
										</td> -->
                                    </tr>

                                    <?php
                                    $this->db->select('nombreEmpresa');
                                    $this->db->from('empresas');
                                    $this->db->where('admin_id', $row['admin_id']);
                                    // $this->db->where('admin_id', $param2);
                                    $query = $this->db->get();
                                    $empresasn = $query->result_array();
                                    $counter = 1;
                                    foreach ($empresasn as $row3) :
                                    ?>
                                        <tr>
                                            <td>
                                                <li>
                                                    <b>
                                                        <?php
                                                        echo  $row3['nombreEmpresa']
                                                        ?>
                                                <?php endforeach;
                                        } ?>
                                                    </b>
                                                </li>
                                            </td>
                                        </tr>

                                        <!-- <td>
								        <i class="entypo-key"></i> &nbsp;
								          <?php echo get_phrase('Administrador de empresa'); ?></td>
                        <td>
                        		<b><?php echo $row['adminEmpresa']; ?></b>
                        </td> -->



                        </table>
                </div>
            </div>
        </section>
        </h4>


    </div>


<?php endforeach; ?>