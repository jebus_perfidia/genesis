<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('Agregar Empresa');?>
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(site_url('admin/staff/create'), array('class' => 'form-horizontal form-groups-bordered ajax-submit', 'enctype' => 'multipart/form-data'));?>
	
					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Nombre');?></label>
                        
						<div class="col-sm-7">
                      		<div class="input-group">
								<span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
								<input type="text" class="form-control" name="name" value="" autofocus required>
                         	</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'RFC'?></label>
                        
						<div class="col-sm-7">
                      		<div class="input-group">
								<span class="input-group-addon"><i class="entypo-doc-text"></i></span>
								<input type="text" class="form-control" name="rfc" value="" autofocus required>
                         	</div>
						</div>
					</div>                    
				
                    
					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Domicilio');?></label>
						<div class="col-sm-7">
                      		<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-location"></i></span>
								<input type="text" class="form-control" name="domicilio" value="" required>
                         	</div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo get_phrase('Tiempo De Contrato');?></label>
                        
						<div class="col-sm-7">
                      		<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-clock"></i></span>
								<input type="date" class="form-control" name="tiempoContrato" value="" required>
                         	</div>
						</div> 
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'estatus';?></label>
						<div class="col-sm-7">
                            <select class="selectboxit" name="estatus" required title="Seleccione alguna opción">
								<option value="" selected disabled hidden>Seleccione una opción...</option>
                                <option value="activa">activa</option>
                                <option value="deshabilitada">deshabilitada</option>
                            </select>
						</div>
					</div>
		
                    <div class="form-group">
						<div class="col-sm-offset-4 col-sm-7">
							<button type="submit" class="btn btn-info" id="submit-button"><?php echo 'Agregar Empresa';?></button>
                         <span id="preloader-form"></span>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<script>
	// url for refresh data after ajax form submission
	var post_refresh_url	=	'<?php echo site_url('admin/reload_staff_list');?>';
	var post_message		=	'Empresa Registrada Correctamente';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js');?>"></script>