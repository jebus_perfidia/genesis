<?php $edit_data	=	$this->db->get_where('admin', array('admin_id' => $param2))->result_array();
foreach ($edit_data as $row) :
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						<?php echo get_phrase('Editar usuario'); ?>
					</div>
				</div>
				<div class="panel-body">

					<?php echo form_open(site_url('staff/staff_clients/edit/' . $row['admin_id']), array('class' => 'form-horizontal form-groups-bordered ajax-submit', 'enctype' => 'multipart/form-data')); ?>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'Nombre *'; ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="entypo-user"></i></span>
								<input type="text" class="form-control" name="name" value="<?php echo $row['nombre']; ?>" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'Apellido paterno *'; ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="entypo-user"></i></span>
								<input type="text" class="form-control" name="aPaterno" value="<?php echo $row['aPaterno']; ?>" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'Apellido materno *'; ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="entypo-user"></i></span>
								<input type="text" class="form-control" name="aMaterno" value="<?php echo $row['aMaterno']; ?>" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Domicilio'); ?></label>
						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-location"></i></span>
								<input type="text" class="form-control" name="domicilio" value="<?php
																								if ($row['domicilio'] == '') {
																									echo '';
																								} else {
																									echo $row['domicilio'];
																								}  ?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Teléfono 1 *'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-phone"></i></span>
								<input type="text" class="form-control" name="telefono1" value="<?php echo $row['telefono1']; ?>" pattern="[0-9]{10}" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Teléfono 2'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-phone"></i></span>
								<input type="text" class="form-control" name="telefono2" value="<?php
																								if ($row['telefono2'] == '') {
																									echo '';
																								} else {
																									echo $row['telefono2'];
																								}  ?>" pattern="[0-9]{10}">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Correo *'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-mail"></i></span>
								<input type="text" class="form-control" name="email" value="<?php echo $row['email']; ?>" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Usuario *'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-user"></i></span>
								<input type="text" class="form-control" name="usuario" value="<?php echo $row['usuario']; ?>" pattern="[A-Za-z0-9]{1,150}" required>
							</div>
						</div>
					</div>


					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo get_phrase('Password *'); ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-key"></i></span>
								<input type="password" class="form-control" name="password" value="<?php echo $row['password']; ?>" pattern="{5,150}" required>
							</div>
							Mínimo 5 caracteres.
						</div>
					</div>


					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'CURP'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-doc-text"></i></span>
								<input type="text" class="form-control" name="curp" value="<?php
																							if ($row['curp'] == '') {
																								echo '';
																							} else {
																								echo $row['curp'];
																							}  ?>" pattern="[A-Za-z0-9]{18}">
							</div>
						</div>
					</div>



					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'RFC'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-doc-text"></i></span>
								<input type="text" class="form-control" name="rfc" value="<?php
																							if ($row['rfc'] == '') {
																								echo '';
																							} else {
																								echo $row['rfc'];
																							}  ?>" pattern="[A-Za-z0-9]{12,13}">
							</div>
						</div>
					</div>


					<?php if ($row['adminEmpresa'] == '1') : ?>

						<input name="adminEmpresa" type="hidden" value="1">

					<?php endif;
					if ($row['adminEmpresa'] == '0') : ?>

						<input name="adminEmpresa" type="hidden" value="0">

					<?php endif; ?>


					<!-- <div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Usuario RH'; ?></label>
						<div class="col-sm-7">
							<select class="selectboxit" name="adminEmpresa">
								<option value="" selected disabled hidden><?php
																			if (($row['adminEmpresa'] == 0) || ($row['adminEmpresa'] == '')) {
																				echo 'No';
																			} else {
																				echo 'Si';
																			}
																			?></option>
								<option value="1">Si</option>
								<option value="0">No</option>
							</select>
							Seleccionar solo si el usuario es tipo cliente.
						</div>
					</div> -->


					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'Empresa'; ?></label>
						<div class="col-sm-7">
							<select class="selectboxit" name="idEmpresa" required>
								<option value="<?php echo $row['idEmpresa'] ?>"><?php echo 'Cambiar de empresa...'; ?></option>
								<?php
								$empresas		=	$this->db->get('empresas')->result_array();
								foreach ($empresas as $row2) :
								?>
									<option value="<?php echo $row2['idEmpresa']; ?>" <?php if ($row['idEmpresa'] == $row2['idEmpresa']) echo 'seleccionar'; ?>>
										<?php echo $row2['nombreEmpresa']; ?></option>
								<?php
								endforeach;
								?>
							</select>
						</div>
					</div>



					<!-- 
					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Empresa'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
								<input type="text" class="form-control" name="idEmpresa" value="<?php echo $row['idEmpresa']; ?>">
							</div>
						</div>
					</div> -->


					<!-- 
					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Administrador de empresa'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-key"></i></span>
								<input type="text" class="form-control" name="adminEmpresa" value="<?php echo $row['adminEmpresa']; ?>">
							</div>
						</div>
					</div> -->
					<div class="form-group">
						<label for="field-2" class="col-sm-4 control-label"><?php echo 'Fecha de nacimiento'; ?></label>

						<div class="col-sm-7">
							<div class="input-group ">

								<span class="input-group-addon"><i class="entypo-clock"></i></span>
								<input type="date" class="form-control" name="fechaNacimiento" value="<?php echo $row['fechaNacimiento'] ?>">
							</div>
						</div>
					</div>


					<input name="tipo" type="hidden" value="cliente">

					<!-- <div class="form-group">
						<label for="field-1" class="col-sm-4 control-label">Tipo</label>
						<div class="col-sm-7">
							<select class="selectboxit" name="tipo" required>
								<option value="<?php echo $row['tipo']; ?>" selected disabled hidden><?php echo $row['tipo']; ?></option>
								<option value="administrador">administrador</option>
								<option value="staff">staff</option>
								<option value="nutriologo">nutriologo</option>
								<option value="chef">chef</option>
								<option value="cliente">cliente</option>
							</select>
						</div>
					</div> -->



					<!-- 		<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('admin_type'); ?></label>
						<div class="col-sm-7">
                            <select class="selectboxit" name="owner_status">
                                <option value="0" <?php if ($row['owner_status'] == 0) echo 'selected'; ?>><?php echo get_phrase('administrator'); ?></option>
                                <option value="1" <?php if ($row['owner_status'] == 1) echo 'selected'; ?>><?php echo get_phrase('owner'); ?></option>
                            </select>
						</div>
					</div> -->

					<!-- <div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('photo'); ?></label>
                        
						<div class="col-sm-7">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<img src="<?php echo $this->crud_model->get_image_url('admin', $row['admin_id']); ?>" alt="...">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
							</div>
						</div>
					</div> -->

					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-7">
							<button type="submit" class="btn btn-info" id="submit-button"><?php echo 'Editar usuario'; ?></button>
							<span id="preloader-form"></span>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<script>
	// url for refresh data after ajax form submission
	var post_refresh_url = '<?php echo site_url('staff/reload_client_list'); ?>';
	var post_message = 'Información actualizada correctamente';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>