<div class="panel panel-primary" id="charts_env">
	<div class="panel-heading">
		<div class="panel-title">
			<i class="fa fa-users"></i>
			<?php echo 'Empresas'; ?>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<div class="table-responsive">
				<table class="table   datatable" id="table_export">
					<thead>
						<tr>
							<th>
							</th>
							<th>
								<div><?php echo get_phrase('Nombre'); ?></div>
							</th>
							<th>
								<div><?php echo 'Razón social'; ?></div>
							</th>
							<th>
								<div><?php echo 'RFC'; ?></div>
							</th>
							<th>
								<div><?php echo get_phrase('Domicilio'); ?></div>
							</th>
							<th>
								<div><?php echo get_phrase('Tiempo Contrato'); ?></div>
							</th>
							<th>
								<div><?php echo get_phrase('Estatus'); ?></div>
							</th>
							<th>
								<div><?php echo 'Nutriólogo asignado'; ?></div>
							</th>
							<th>
								<div><?php echo get_phrase('Opciones'); ?></div>
							</th>
						</tr>
					</thead>
					<tbody>

						<?php
						$this->db->select('empresas.*,admin.nombre,admin.aPaterno,admin.aMaterno');
						$this->db->from('empresas');
						$this->db->join('admin', 'empresas.admin_id=admin.admin_id', 'left');
						/* $this->db->where('tbl_usercategory','admin'); */
						$query = $this->db->get();
						$staffs = $query->result_array();
						$counter = 1;
						foreach ($staffs as $row) :

						?>

							<tr>
								<td>
									<?php echo $counter++; ?>
								</td>
								<td>
									<?php echo $row['nombreEmpresa']; ?>
								</td>
								<td>
									<?php echo $row['razonSocial'] ?>
								</td>
								<td>
									<?php echo $row['rfc']; ?>
								</td>
								<td>
									<?php echo $row['domicilio']; ?>
								</td>
								<td>
									<?php echo $row['tiempoContrato']; ?>
								</td>
								<td>
									<?php echo $row['estatus']; ?>
								</td>
								<td>
									<?php echo $row['nombre'] . ' ' . $row['aPaterno'] . ' ' . $row['aMaterno']; ?>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
											Acciones<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-default pull-right" role="menu">

											<!-- PROFILE LINK -->
											<li>
												<a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/staff_profile/' . $row['idEmpresa']); ?>');">
													<i class="entypo-user"></i>
													<?php echo 'Perfil'; ?>
												</a>
											</li>

											<!-- EDITING LINK -->
											<li>
												<a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/staff_edit/' . $row['idEmpresa']); ?>');">
													<i class="entypo-pencil"></i>
													<?php echo 'Editar'; ?>
												</a>
											</li>
											<li class="divider"></li>

											<!-- DELETION LINK -->
											<li>
												<a href="#" onclick="confirm_modal('<?php echo site_url('admin/staff/delete/' . $row['idEmpresa']); ?>' , '<?php echo site_url('admin/reload_staff_list'); ?>');">
													<i class="entypo-trash"></i>
													<?php echo get_phrase('Eliminar'); ?>
												</a>
											</li>
										</ul>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				</div>
			</div>
		</div>

		<!-- calling ajax form submission plugin for specific form -->
		<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/neon-custom-ajax.js'); ?>"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				//convert all checkboxes before converting datatable
				replaceCheckboxes();

				// Highlighted rows
				$("#table_export tbody input[type=checkbox]").each(function(i, el) {
					var $this = $(el),
						$p = $this.closest('tr');

					$(el).on('change', function() {
						var is_checked = $this.is(':checked');

						$p[is_checked ? 'addClass' : 'removeClass']('highlight');
					});
				});

				// convert datatable
				var datatable = $("#table_export").dataTable({
					"sPaginationType": "bootstrap",
					// "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",

					// "aoColumns": [
					// 	{ "bSortable": false}, 	//0,checkbox
					// 	{ "bVisible": true},		//1,name
					// 	{ "bVisible": true},		//1,photo
					// 	{ "bVisible": true},		//2,role
					// 	{ "bVisible": true},		//3,contact
					// 	{ "bVisible": true},		//4,contact
					// 	{ "bVisible": true},		//5,contact
					// 	{ "bVisible": true}		//6,option
					// ],
					"oTableTools": {
						"aButtons": [

							{
								"sExtends": "xls",
								"mColumns": [1, 2, ]
							},
							{
								"sExtends": "pdf",
								"mColumns": [1, 2]
							},
							{
								"sExtends": "print",
								"fnSetText": "Press 'esc' to return",
								"fnClick": function(nButton, oConfig) {
									datatable.fnSetColumnVis(0, false);
									datatable.fnSetColumnVis(3, false);
									datatable.fnSetColumnVis(5, false);

									this.fnPrint(true, oConfig);

									window.print();

									$(window).keyup(function(e) {
										if (e.which == 27) {
											datatable.fnSetColumnVis(0, true);
											datatable.fnSetColumnVis(3, true);
											datatable.fnSetColumnVis(5, true);
										}
									});
								},

							},
						]
					},

				});

				//customize the select menu
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});




			});
		</script>