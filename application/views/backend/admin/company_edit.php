<?php
$edit_data = $this->db->get_where('alimentos', array('idAlimento' => $param2))->result_array();
foreach ($edit_data as $row) :
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						<?php echo get_phrase('Editar alimentos'); ?>
					</div>
				</div>
				<div class="panel-body">

					<?php echo form_open(site_url('admin/company/edit/' . $row['idAlimento']), array('class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data')); ?>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Nombre'); ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-coffee"></i></span>
								<input type="text" class="form-control" name="nombreAlimento" value="<?php echo $row['nombreAlimento']; ?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'Porción'; ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-coffee"></i></span>
								<input type="text" class="form-control" name="proporcionAlimento" value="<?php echo $row['proporcionAlimento']; ?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Cantidad'); ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-coffee"></i></span>
								<input type="text" class="form-control" name="cantidadAlimento" value="<?php echo $row['cantidadAlimento']; ?>">
							</div>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label">Tipo</label>
						<div class="col-sm-7">
							<select class="selectboxit" name="tipoAlimento" required>
								<option value="<?php echo $row['tipoAlimento']; ?>" selected disabled hidden><?php echo $row['tipoAlimento']; ?></option>
								<option value="verduras">Verdura</option>
								<option value="fruta">Futa</option>
								<option value="cereal">Cereal</option>
								<option value="producto animal">Producto animal</option>
								<option value="grasa proteina">Grasa con proteina</option>
								<option value="grasa">Grasas</option>
								<option value="leguminosa">Leguminosas</option>
								<option value="libre">LIbre</option>
							</select>
						</div>
					</div>


					<!-- <div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Tipo'); ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-coffee"></i></span>
								<input type="text" class="form-control" name="email" value="<?php echo $row['tipoAlimento']; ?>">
							</div>
						</div>
					</div> -->

					<!-- <div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('website'); ?></label>
                        
						<div class="col-sm-7">
                      	<div class="input-group">
								<span class="input-group-addon"><i class="entypo-globe"></i></span>
								<input type="text" class="form-control" name="website" value="<?php echo $row['website']; ?>">
                         </div>
						</div>
					</div>

					<div class="form-group">
	                    <label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('associated_person'); ?></label>

	                    <div class="col-sm-7">
	                        <select name="client_id" class="select2">
	                            <option><?php echo get_phrase('select_associated_person'); ?></option>
	                            <?php
								$clients = $this->db->get('client')->result_array();
								foreach ($clients as $row2) :
								?>
	                                <option value="<?php echo $row2['client_id']; ?>" 
	                                	<?php if ($row['client_id'] == $row2['client_id'])
											echo 'selected'; ?>>
	                                    <?php echo $row2['name']; ?></option>
	                            <?php endforeach; ?>
	                        </select>
	                    </div>
	                </div>

	                <div class="form-group">
	                    <label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('company_logo'); ?></label>

	                    <div class="col-sm-7">
	                        <div class="fileinput fileinput-new" data-provides="fileinput">
	                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
	                            	<?php if (file_exists('uploads/company_logo/' . $row['company_id'] . '.jpg')) : ?>
					                    <img src="<?php echo base_url('uploads/company_logo/' . $row['company_id'] . '.jpg'); ?>" alt="">
					                <?php else : ?>
					                    <img src="<?php echo base_url('uploads/company_logo/company.png'); ?>" alt="" style="width: 100%; height: 100%;">
					                <?php endif; ?>
	                            </div>
	                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
	                            <div>
	                                <span class="btn btn-white btn-file">
	                                    <span class="fileinput-new">Select image</span>
	                                    <span class="fileinput-exists">Change</span>
	                                    <input type="file" name="company_logo" accept="image/*">
	                                </span>
	                                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
	                            </div>
	                        </div>
	                    </div>
					</div> -->

					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-7">
							<button type="submit" class="btn btn-info" id="submit-button"><?php echo 'Editar alimento'; ?></button>
							<span id="preloader-form"></span>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<script>
	// url for refresh data after ajax form submission
	var post_refresh_url = '<?php echo site_url('admin/reload_company_list'); ?>';
	var post_message = 'Información actualizada correctamente';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>