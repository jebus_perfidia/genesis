<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('Agregar Empresa'); ?>
				</div>
			</div>
			<div class="panel-body">

				<?php echo form_open(site_url('admin/staff/create'), array('class' => 'form-horizontal form-groups-bordered ajax-submit', 'enctype' => 'multipart/form-data')); ?>

				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Nombre comercial *'); ?></label>

					<div class="col-sm-7">
						<div class="input-group">
							<span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
							<input type="text" class="form-control" name="name" value="" placeholder="Introduzca el nombre comercial" title="Este campo es obligatorio" autofocus required>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo 'Razón social'; ?></label>

					<div class="col-sm-7">
						<div class="input-group">
							<span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
							<input type="text" class="form-control" name="razonSocial" placeholder="Introduzca la razón social" title="Este campo es obligatorio" value="" autofocus required>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo 'RFC' ?></label>

					<div class="col-sm-7">
						<div class="input-group">
							<span class="input-group-addon"><i class="entypo-doc-text"></i></span>
							<input type="text" class="form-control" name="rfc" value="" onkeyup="mayus(this);" placeholder="RFC a 12 o 13 dígitos" pattern="[A-Za-z0-9]{12,13}" title="Este campo es obligatorio" autofocus required>
						</div>
					</div>
				</div>

				<script>
					function mayus(e) {
						e.value = e.value.toUpperCase();
					}

					// function habilitar(value) {
					// 	if (value == true) {
					// 		// habilitamos
					// 		document.getElementById("idEmpresa").disabled = false;
					// 	} else if (value == false) {
					// 		// deshabilitamos
					// 		document.getElementById("idEmpresa").disabled = true;
					// 	}
					// }
				</script>


				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Domicilio'); ?></label>
					<div class="col-sm-7">
						<div class="input-group ">
							<span class="input-group-addon"><i class="entypo-location"></i></span>
							<input type="text" class="form-control" name="domicilio" placeholder="Introduzca el domicilio" title="Este campo es obligatorio" value="" required>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-2" class="col-sm-4 control-label"><?php echo get_phrase('Tiempo De Contrato'); ?></label>

					<div class="col-sm-7">
						<div class="input-group ">
							<span class="input-group-addon"><i class="entypo-clock"></i></span>
							<input type="date" class="form-control" name="tiempoContrato" value="" title="Este campo es obligatorio" required>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo 'Nutriologo asignado'; ?></label>
					<div class="col-sm-7">
						<select class="selectboxit" name="admin_id" id="admin_id" required>
							<option><?php echo 'Selecciona un usuario...'; ?></option>
							<?php
							// $nutriologos		=	$this->db->get('admin')->result_array();
							$this->db->select('admin_id,nombre,aPaterno,tipo');
							$this->db->from('admin');
							$this->db->where('tipo', 'nutriologo');
							$query = $this->db->get();
							$nutriologos = $query->result_array();
							foreach ($nutriologos as $row2) :
							?>
								<option value="<?php echo $row2['admin_id']; ?>" <?php if ($row['admin_id'] == $row2['admin_id']) echo 'seleccionar'; ?>>
									<?php echo $row2['nombre'].' '.$row2['aPaterno']; ?></option>
							<?php
							endforeach;
							?>
						</select>
					</div>
				</div>


				<div class="form-group">
					<label for="field-2" class="col-sm-4 control-label"><?php echo 'Estatus'; ?></label>
					<div class="col-sm-7">
						<select class="selectboxit" name="estatus" required title="Seleccione alguna opción">
							<option value="" selected disabled hidden>Seleccione una opción...</option>
							<option value="activa">activa</option>
							<option value="deshabilitada">deshabilitada</option>
						</select>
					</div>
				</div>



				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-7">
						<button type="submit" class="btn btn-info" id="submit-button"><?php echo 'Agregar Empresa'; ?></button>
						<span id="preloader-form"></span>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<script>
	// url for refresh data after ajax form submission
	var post_refresh_url = '<?php echo site_url('admin/reload_staff_list'); ?>';
	var post_message = 'Empresa Registrada Correctamente';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>