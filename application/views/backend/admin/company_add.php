<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('Agregar alimento'); ?>
				</div>
			</div>
			<div class="panel-body">

				<?php echo form_open(site_url('admin/company/create'), array('class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data')); ?>

				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Nombre'); ?></label>

					<div class="col-sm-7">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-coffee"></i></span>
							<input type="text" class="form-control" name="nombreAlimento" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Porcion'); ?></label>

					<div class="col-sm-7">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-coffee"></i></span>
							<input type="text" class="form-control" name="proporcionAlimento">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Cantidad'); ?></label>

					<div class="col-sm-7">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-coffee"></i></span>
							<input type="text" class="form-control" name="cantidadAlimento">
						</div>
					</div>
				</div>


				<div class="form-group">
					<label for="field-2" class="col-sm-4 control-label"><?php echo 'Tipo de alimento'; ?></label>
					<div class="col-sm-7">
						<select class="selectboxit" name="tipoAlimento">
							<option value="" selected disabled hidden>Seleccione el tipo...</option>
							<option value="verduras">Verdura</option>
							<option value="fruta">Fruta</option>
							<option value="cereal">Cereal</option>
							<option value="producto animal">Producto animal</option>
							<option value="grasa proteina">Grasa con proteina</option>
							<option value="grasa">Grasas</option>
							<option value="leguminosa">Leguminosas</option>
							<option value="libre">LIbre</option>
						</select>
					</div>
				</div>

				<!-- <div class="form-group">
					<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Tipo'); ?></label>

					<div class="col-sm-7">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-coffee"></i></span>
							<input type=" text" class="form-control" pattern="[0-9]{10}" name="tipoAlimento">
						</div>
					</div>
				</div> -->

				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-7">
						<button type="submit" class="btn btn-info" id="submit-button"><?php echo get_phrase('Agregar alimento'); ?></button>
						<span id="preloader-form"></span>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<script>
	// url for refresh data after ajax form submission
	var post_refresh_url = '<?php echo site_url('admin/reload_company_list'); ?>';
	var post_message = 'Alimento creado correctamente';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>