<?php $edit_data	=	$this->db->get_where('empresas', array('idEmpresa' => $param2))->result_array();
foreach ($edit_data as $row) :
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						<?php echo 'Editar Información' ?>
					</div>
				</div>
				<div class="panel-body">

					<?php echo form_open(site_url('admin/staff/edit/' . $row['idEmpresa']), array('class' => 'form-horizontal form-groups-bordered ajax-submit', 'enctype' => 'multipart/form-data')); ?>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('Nombre'); ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
								<input type="text" class="form-control" name="name" value="<?php echo $row['nombreEmpresa']; ?>" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'RFC'; ?></label>

						<div class="col-sm-7">
							<div class="input-group">
								<span class="input-group-addon"><i class="entypo-doc-text"></i></span>
								<input type="text" class="form-control" name="rfc" value="<?php echo $row['rfc']; ?>" required>
							</div>
						</div>
					</div>

					<!-- 	<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('account_role'); ?></label>
						<div class="col-sm-7">
                          <select class="selectboxit" name="account_role_id">
                              <option><?php echo get_phrase('select_a_role'); ?></option>
                              <?php
								$account_roles		=	$this->db->get('account_role')->result_array();
								foreach ($account_roles as $row2) :
								?>
                                      <option value="<?php echo $row2['account_role_id']; ?>"
                                      	<?php if ($row['account_role_id'] == $row2['account_role_id']) echo 'selected'; ?>>
												<?php echo $row2['name']; ?></option>
                               <?php
								endforeach;
								?>
                       	</select>
						</div>
					</div> -->

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'Domicilio'; ?></label>
						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-location"></i></span>
								<input type="text" class="form-control" name="domicilio" value="<?php echo $row['domicilio']; ?>" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'Tiempo De Contrato'; ?></label>
						<div class="col-sm-7">
							<div class="input-group ">
								<span class="input-group-addon"><i class="entypo-clock"></i></span>
								<input type="date" class="form-control" name="tiempoContrato" value="<?php echo $row['tiempoContrato']; ?>" required>
							</div>
						</div>
					</div>








					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo 'Nutriologo asignado'; ?></label>
						<div class="col-sm-7">
							<select class="selectboxit" name="admin_id" id="admin_id" required>
								<option><?php echo 'Cambiar usuario...'; ?></option>
								<?php
								// $nutriologos		=	$this->db->get('admin')->result_array();
								$this->db->select('admin_id,nombre,aPaterno,tipo');
								$this->db->from('admin');
								$this->db->where('tipo', 'nutriologo');
								$query = $this->db->get();
								$nutriologos = $query->result_array();
								foreach ($nutriologos as $row2) :
								?>
									<option value="<?php echo $row2['admin_id']; ?>" <?php if ($row['admin_id'] == $row2['admin_id']) echo 'seleccionar'; ?>>
										<?php echo $row2['nombre'].' '.$row2['aPaterno']; ?></option>
								<?php
								endforeach;
								?>
							</select>
						</div>
					</div>






					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label">Tipo</label>
						<div class="col-sm-7">
							<select class="selectboxit" name="estatus">
								<option value="<?php echo $row['estatus']; ?>" selected disabled hidden><?php echo $row['estatus']; ?></option>
								<option value="activa">activa</option>
								<option value="deshabilitada">deshabilitada</option>
							</select>
						</div>
					</div>



					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-7">
							<button type="submit" class="btn btn-info" id="submit-button"><?php echo 'Editar Empresa'; ?></button>
							<span id="preloader-form"></span>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<script>
	// url for refresh data after ajax form submission
	var post_refresh_url = '<?php echo site_url('admin/reload_staff_list'); ?>';
	var post_message = 'Empresa Editada Correctamente';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>