<div class="sidebar-menu">
    <header class="logo-env">
        <!-- logo collapse icon -->
        <!-- <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div> -->

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

        <div style="text-align: -webkit-center;" id="branding_element">
            <img src="<?php echo base_url('assets/logo.png'); ?>" style="max-height:35px;" />
            <h4 style="color: #a2a3b7;text-align: -webkit-center;margin-bottom: 25px;font-weight: 600;
            margin-top: 10px; letter-spacing: 4px; font-size: 18px;">
                GÉNESIS<?php //echo $system_name;
                        ?>
            </h4>
        </div>

        <!-- SEARCH FORM -->
        <!--   <li id="search">
            <?php echo form_open(site_url('admin/search'), array('onsubmit' => 'return validate()')); ?>
                <input id="search_input" type="text" name="search_key" class="search-input" placeholder="Search ..."/>
                <button type="submit">
                    <i class="entypo-search"></i>
                </button>
            </form>
        </li> -->

        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?>">
            <a href="<?php echo site_url('admin/dashboard'); ?>">
                <i class="fa fa-home"></i>
                <span><?php echo 'Inicio'; ?></span>
            </a>
        </li>

        <!-- MANAGE CLIENTS AND COMPANY -->
        <!-- <li class="<?php if (
                            $page_name == 'client' ||
                            $page_name == 'pending_client' ||
                            $page_name == 'company'
                        )
                            echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="entypo-trophy"></i>
                <span><?php echo get_phrase('client'); ?></span>
            </a>
            <ul>
                <li class="<?php if (
                                $page_name == 'client' ||
                                $page_name == 'pending_client'
                            )
                                echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/client'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('person'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'company') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/company'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('company'); ?></span>
                    </a>
                </li>
            </ul>
        </li> -->

        <!-- Alta de usuarios y empresas  -->
        <li class="<?php if (
                        $page_name == 'staff' ||
                        // $page_name == 'account_role' ||
                        $page_name == 'admins'
                    )
                        echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="fa fa-plus-square"></i>
                <span><?php echo 'Altas'; ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'admins') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/admins'); ?>">
                        <i class="fa fa-user-plus"></i>
                        <span><?php echo 'Usuarios'; ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'staff') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/staff'); ?>">
                        <i class="fa fa-building"></i>
                        <span><?php echo 'Empresas'; ?></span>
                    </a>
                </li>
                <!--  <li class="<?php if ($page_name == 'account_role') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/account_role'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('permission'); ?></span>
                    </a>
                </li> -->
            </ul>
        </li>


        <!-- Agregar alimentos -->

        <li class="<?php if ($page_name == 'company') echo 'active'; ?>">
            <a href="<?php echo site_url('admin/company'); ?>">
                <i class="fa fa-coffee"></i>
                <span><?php echo 'Alimentos'; ?></span>
            </a>
        </li>






        <!-- <li class="<?php if (
                            $page_name == 'team_task' ||
                            $page_name == 'team_task_archived' ||
                            $page_name == 'team_task_view'
                        )
                            echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="entypo-traffic-cone"></i>
                <span><?php echo 'Agregar ingredientes'; ?></span>
            </a> -->
        <!--   <ul>
                <li class="<?php if ($page_name == 'team_task') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/team_task'); ?>">
                        <i class="entypo-list"></i>
                        <span><?php echo 'Asignar a empresa'; ?></span>
                    </a>
                </li>
               
            </ul> -->
        </li>

        <!-- Agregar recetas -->
        <li class="<?php if (
                        $page_name == 'team_task' ||
                        $page_name == 'team_task_archived' ||
                        $page_name == 'team_task_view'
                    )
                        echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="fa fa-cutlery"></i>
                <span><?php echo 'Agregar recetas'; ?></span>
            </a>
            <!--   <ul>
                <li class="<?php if ($page_name == 'team_task') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/team_task'); ?>">
                        <i class="entypo-list"></i>
                        <span><?php echo 'Asignar a empresa'; ?></span>
                    </a>
                </li>
               
            </ul> -->
        </li>


        <!-- Plantillas  -->

        <li class="<?php if (
                        $page_name == 'project_add' ||
                        $page_name == 'project' ||
                        $page_name == 'project_room' ||
                        $page_name == 'project_quote' || $page_name == 'project_quote_view'
                    )
                        echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="fa fa-file-text"></i>
                <span><?php echo 'Plantillas'; ?></span>
            </a>
            <ul>
                <!--   <li class="<?php if ($page_name == 'project') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/project'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo 'Ingredientes'; ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'project_add') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/project_add'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo 'Recetas'; ?></span>
                    </a>
                </li> -->
                <!-- <li class="<?php if ($page_name == 'project_quote' || $page_name == 'project_quote_view') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/project_quote'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('project_quote'); ?></span>
                    </a>
                </li> -->
            </ul>
        </li>


        <!-- Plantillas -->

        <!--      <li class="<?php if ($page_name == 'calendar') echo 'active'; ?>">
            <a href="<?php echo site_url('admin/calendar'); ?>">
                <i class="entypo-calendar"></i>
                <span><?php echo 'Plantillas'; ?></span>
            </a>
        </li>
 -->


        <!-- SETTINGS -->

        <!--    <li class="<?php if (
                                $page_name == 'system_settings' ||
                                $page_name == 'manage_language' ||
                                $page_name == 'email_settings' ||
                                $page_name == 'about' ||
                                $page_name == 'payment_settings' ||
                                $page_name == 'smtp_settings'
                            )
                                echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="entypo-tools"></i>
                <span><?php echo get_phrase('settings'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/system_settings'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('system_settings'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'email_settings') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/email_settings'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('email_template'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'smtp_settings') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/smtp_settings'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('smtp_settings'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'payment_settings') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/payment_settings'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('payment_settings'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'manage_language') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/manage_language'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('language_settings'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'about') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/about'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('about'); ?></span>
                    </a>
                </li>
            </ul>
        </li> -->
    </ul>

</div>

<script type="text/javascript">
    function validate() {
        var search_string = $('#search_input').val();
        var search_string_length = search_string.length;
        if (search_string_length < 2) {
            toastr.error("Please enter minimum 2 characters", "Error");
            return false;
        }
    }
</script>