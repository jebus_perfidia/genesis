<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo 'Editar información'; ?>
                </div>
            </div>
            <div class="panel-body">
                <?php
                foreach ($edit_data as $row) : ?>
                    <?php echo form_open(site_url('admin/manage_profile/update_profile_info'), array(
                        'class' => 'form-horizontal form-groups-bordered', 'enctype' => 'multipart/form-data'
                    )); ?>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Usuario'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="usuario" value="<?php echo $row['usuario']; ?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Nombre'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="nombre" value="<?php echo $row['nombre']; ?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Apellido Paterno'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="aPaterno" value="<?php echo $row['aPaterno']; ?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Apellido Materno'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="aMaterno" value="<?php echo $row['aMaterno']; ?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Correo'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="email" value="<?php echo $row['email']; ?>" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Teléfono 1'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="telefono1" value="<?php echo $row['telefono1']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Teléfono 2'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="telefono2" value="<?php echo $row['telefono2']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Dirección'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="domicilio" value="<?php echo $row['domicilio']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'CURP'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="curp" value="<?php echo $row['curp']; ?>" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'RFC'; ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="rfc" value="<?php echo $row['rfc']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('Actualizar informaci&oacute;n'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>
</div>


<!--password-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo 'Cambiar Contraseña'; ?>
                </div>
            </div>
            <div class="panel-body">
                <?php
                foreach ($edit_data as $row) :
                ?>
                    <?php echo form_open(site_url('admin/manage_profile/change_password'), array('class' => 'form-horizontal form-groups validate', 'target' => '_top')); ?>
                    <input type="hidden" name="mailChangePassword" value="<?php echo $row['email']; ?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Contraseña Actual'; ?></label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" name="password" id="" value="" />
                        </div>
                    </div>
                    <br>
                    <center>
                        <label id="mensaje"></label>
                    </center>
                    <div class=" form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Nuava Contraseña'; ?></label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" name="new_password" id="passwd" pattern="{5,150}" placeholder="Tu contraseña debe tener al menos 6 caracteres" title="Este campo es obligatorio" value="" onchange="validar_pass()" required/>
                        </div>
                    </div>
                    <div class=" form-group">
                        <label class="col-sm-3 control-label"><?php echo 'Confirmar Nueva Contraseña'; ?></label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" name="confirm_new_password" id="passwd2" pattern="{5,150}" placeholder="Confirma tu nueva contraseña." value="" onchange="validar_pass()" requiered/>
                        </div>
                    </div>
                    <div class=" form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="boton" class="btn btn-info"><?php echo 'Cambiar Contraseña'; ?></button>
                        </div>
                    </div>
                    </form>
                <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function validar_pass() {

        var primerInput = document.getElementById("passwd").value;
        var segundoInput = document.getElementById("passwd2").value;


        if (primerInput === segundoInput) {
            $('#mensaje').removeClass('text-danger').addClass('text-success').html("<b>Las contraseñas coinciden</b>");
            $('#boton').attr("disabled", false);

        } else {
            $('#mensaje').removeClass('text-success').addClass('text-danger').html("<b>Las contraseñas no coinciden</b>");
            $('#boton').attr("disabled", true);


        }
    }
</script>