<div class="container">
    <p class="h4 text-justify"><strong>Responde cada pregunta seleccionando verdadero o falso. Esta evaluación determinará si eres
            deficiente en cualquiera de los 4 neurotrasmisores. Muchas de las preguntas se relacionan con los sintomas
            que pudieras estar experimentando. Responde a las preguntas en términos de cómo te sientes justo ahora
            no importa cuanto tiempo has estado experimentando estos sintomas, incluso si ocurieron por primera vez.
        </strong></p>
    <br>
    <p class="h4 text-justify"><strong>ES MUY IMPORTANTE QUE CONTESTES CADA AFIRMACIÓN CON LO PRIMERO
            QUE TE VENGA A LA MENTE, SIN JUSTIFICARSE A SI MISMO, SIN ANALIZAR Y SOBRE TODO, SIN AUTOENGAÑARSE.
        </strong></p>

    <hr>
    <h3>Dopamina</h3>
    <hr>
    <h4><strong>Memoría y atención</strong></h4>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo problemas para poner mucha atención y concentrarme'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="problemaAtencion" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Necesito cafeina para despertarme'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="necesitarCafe" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo pensar lo suficientemente rápido'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="pensarRapido" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'No tengo buen rango de atención'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="rangoAtencion" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo problemas para terminar una tarea, incluso cuando me interesa'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="problemaTerminarTarea" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Soy lento para aprender nuevas ideas'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="lentoAprenderIdeas" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <hr>


    <h4><strong>Físico</strong></h4>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo antojos de azúcar'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="antojoAzucar" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Ha disminuido mi líbido'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="dismuidoLibido" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Duermo demasiado'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="dormirDemasiado" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo un historial de alcohol y adicción'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="historialAlcohol" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="si">Sí</option>
                <option value="no">No</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Recientemente me he sentido cansado sin razón aparente'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="recientementeCansado" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces experimento cansansio total sin haberme ejercitado'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="cansancioSinEjercicio" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>

    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Siempre he tenido problemas para controlar mi peso'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="problemaControlarPeso" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo poca motivación para las experiencias sexuales'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="pocaMotivacionSexual" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo problemas para levantarme de la cama por las mañanas'; ?></label>
        <div class="col-md-5">
            <select class="selectboxit" name="problemasLevantarseCama" title="Seleccione si o no">
                <option value="" selected disabled hidden></option>
                <option value="1">Verdadero</option>
                <option value="0">Falso</option>
            </select>

        </div>
    </div>


    <hr>
    <h4><strong>Personalidad</strong>
        <h4>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Solo me siento bien cuando sigo a los demás'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="sentirseBienDemas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'La gente parece tener ventaja de mí'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="genteVentaja" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me estoy sintiendo muy decaído o deprimido'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="sentirseDeprimido" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'La gente me ha dicho que soy demasiado tranquilo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="demasiadoTranquilo" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo pocas urgencias'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="pocasUrgencias" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Dejo que la gente me critique'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="reActFisHC" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <hr>
            <h4><strong>Carácter</strong></h4>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'He perdido mis capacidades de razonamiento'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="perderCapacidadRazonamiento" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo tomar buenas decisiones'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tomarBuenasDecisiones" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>




            <h3>Acetilcolina</h3>
            <hr>
            <h4><strong>Memoría y atención</strong></h4>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me falta imaginación'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="faltaImaginacion" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo dificultad para recordar nombres cuando conozco gente'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="dificultadRecordarNombres" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'He notado que mi capacidad de memoria está disminuyendo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="capacidadMemoriaDisminuye" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Otros me dicen que no tengo pensamientos románticos'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noPensamientosRomanticos" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo recordar los cumpleaños de mis amigos'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noRecordarcumpleanios" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'He perdido parte de mi creatividad'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="perderParteCreatividad" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>

            <h4><strong>Físico</strong></h4>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo insomnio'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerInsomnio" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'He perdido tono muscular'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="perderTonoMuscular" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Ya no hago ejercicio'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noHagoEjercicio" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo antojos de alimentos grasosos'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="antojoAlimentoGrasoso" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'He probado alucinógenos, LSD y otras drogas ilícitas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="probadoAlucinogenos" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Siento como mi cuerpo se estuviera separando'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="cuerpoSentirSeparado" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo respirar fácilmente'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noRespirarFacil" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>

            <h4><strong>Personalidad</strong></h4>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No me siento muy feliz muy seguido'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noFelizSeguido" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me siento desesperado'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="sentirseDesesperado" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Evito que los demás me hagan daño al no decirles mucho sobre mí'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noDecirMucho" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Se me hace más cómodo hacer las cosas sólo que en un grupo grande'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="hacerCosasSolo" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Otras personas se enojan por las cosas que hago'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="personasEnojanCosas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me rindo fácilmente y tiendo a ser sumiso'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="rendirFacilmente" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Raramente me siento apasionado por algo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="reActFisHC" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me gusta la rutina'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="gustaRutina" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>


            <h4><strong>Carácter</strong></h4>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No me preocupan las historias de los demás sólo las mías'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noPreocupaHistoriaOtros" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No pongo atención a los sentimientos de las personas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noAtencionSentimientosOtros" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No me siento optimista'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noSentirseOptimista" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Estoy obsesionado con mis deficiencias'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="obsesionadoDeficiencias" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>

            <h3>Gaba</h3>
            <hr>
            <h4><strong>Memoría y atención</strong></h4>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Se me hace difícil concentrarme porque soy nervioso'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="dificlConcentrarNervioso" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo recordar número telefónicos'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noRecordarTelefonos" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo problemas para encontrar la "palabra correcta'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="problemasEncontrarPalabra" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Se me dificulta recordar las cosas que son importantes'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="dificultaRecordarCosas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Sé que soy inteligente, pero me es difícil demostrarlo a los demás'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="inteligenteDificilMostrar" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Mi capacidad para concentrarme va y viene'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="capacidadConcentrarme" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Cuando leo, tengo que regresarme al mismo párrafo algunas veces para captar la información'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="leerRegresarCaptar" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Soy un pensador rápido, pero no siempre puedo explicar lo que puedo decir'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="pensarRapidoNoExplicar" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Soy un pensador rápido, pero no siempre puedo explicar lo que puedo decir'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="pensarRapidono" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <hr>

            <h4><strong>Físico</strong></h4>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me siento nervioso'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="sentirseNerviosos" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces tiemblo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="algunasVecesTemblar" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo dolores frecuentes de cabeza y/o espalda'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerDolorFrecuenteCabeza" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a tener dificultad para respirar'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="dificultadParaRespirar" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a tener palpitaciones cardiacas aceleradas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerPalpitacionesAceleradas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a tener las manos frías'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerManosFrias" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="si">Sí</option>
                        <option value="no">No</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces sudo demasiado'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="sudarDemasiado" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'En ocasiones me mareo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="ocasionesMareo" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Usualmente tengo tensión muscular'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerTensionMuscular" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a tener mariposas en el estómago'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerMariposasEstomago" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Siento antojo por alimentos agridulces'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="antojoAlimentosAgridulces" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Usualmente estoy nervioso'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="usualmenteNervioso" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me gusta el yoga porque me ayuda a relajarme'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="gustarYoga" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Comunmente me siento fatigado, incluso aunque haya dormido bien durante la noche'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="sentirseFatigadoAunDormir" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Como en exceso'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="comerExceso" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>

            <h4><strong>Personalidad</strong></h4>
            <hr>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo cambios en mi estado de ánimo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="cambiosEstadoAnimo" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me gusta hacer muchas cosas en un mismo momento, pero se me hace difícil decidir cuál haré primero'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="muchasCosasMomento" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a hacer cosas sólo porque pienso que deben ser divertidas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="cosasSoloDivertido" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Cuando las cosas están aburridas, siempre intento poner algo de emoción'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="aburridasPonerEmocion" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a ser inconstante, cambiando frecuentemente de estado de ánimo y de pensamientos'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="inconstanteAnimoPensamiento" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a emocionarme en exceso en las cosas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="emocionarseExceso" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Mis impulsos tienden a meterme en muchos problemas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="impulsosMeterseProblemas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a ser teatral y atraer la atención hacia mi mismo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="teatralAtraerAtencion" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Hablo con mi mente, sin importar la reacción que puedan tener los demás'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="hablarMente" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces tengo ataques de ira y después me siento terriblemente culpable'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="ataquesIraCulpable" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'En ocasiones digo mentiras para salir de los problemas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="ocasionesMentirasProblemas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Siempre tengo menos interés en el sexo que las personas promedio'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="interesSexoPromedio" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>

            <h4><strong>Carácter</strong></h4>
            <hr>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Yo no sigo las reglas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noSeguirReglas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>


                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'He perdido muchos amigos'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="perderAmigos" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo conservar las relaciones románticas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noConservarRelaciones" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Considero arbitrario a la ley sin tener alguna razón'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="arbitrarioLeyRazon" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Considero inútiles las reglas que puedo seguir'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="inutilesReglas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <hr>
            <h3>Serotonina</h3>
            <hr>
            <h4>Memoría y atención</h4>
            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No soy muy perceptivo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noMuyPerceptivo" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo recordar las cosas que he visto en el pasado'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noRecordarCosasPasado" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo un lento tiempo de reacción'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="lentoTiempoReaccion" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo poco sentido de la dirección'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="pocoSentidoDireccion" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <hr>

            <h4><strong>Físico</strong></h4>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo sudoraciones nocturnas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerSudiracionNocturna" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo insomnio'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerInsomnio" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a dormir en muchas posiciones diferentes para sentirme más cómodo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="dormirMuchasPosiciones" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Siempre despierto temprano por las mañanas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="despertarTemprano" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No me puedo relajar'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noPoderRelajar" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me despierto al menos dos veces por las noches'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="despertarDosVecesNoche" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Es difícil para mi volverme a quedar dormido cuando me despierto '; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="dificilDormirCuandoDespierto" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo un antojo de sal'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="tenerAntojoSal" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo menos energía para ejercitarme'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="menosEnergiaEjercicio" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>

            <h4><strong>Personalidad</strong></h4>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo ansiedad crónica'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="ansiedadCronica" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me irrito fácilmente'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="irritarFacilmente" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo pensamientos de autodestrucción'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="pensamientosAutodestruccion" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'He tenido pensamientos suicidas en mi vida'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="pensamientoSuicidioVida" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a mortificarme mucho en mis ideas'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="mortificarseIdeas" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces soy tan cuadrado(a) que me he vuelto inflexible'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="serCuadradoInflexible" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'Mi imaginación me domina'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="imaginacionDomina" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'El miedo me apasiona'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="miedoApasiona" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <hr>

            <h4><strong>Carácter</strong></h4>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo dejar de pensar en el significado de mi vida'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="pensarSignificadoVida" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'No deseo tomar riesgos por mucho tiempo'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="noDeseoTomarRiesgos" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-md-7 control-label"><?php echo 'La falta de significado en la vida es dolorosa para mí'; ?></label>
                <div class="col-md-5">
                    <select class="selectboxit" name="significadoVidaDoloroso" title="Seleccione si o no">
                        <option value="" selected disabled hidden></option>
                        <option value="1">Verdadero</option>
                        <option value="0">Falso</option>
                    </select>

                </div>
            </div>
            <br><br><br><br><br><br>
            </div>