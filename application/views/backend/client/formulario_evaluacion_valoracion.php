<div class="container">
    <p class="h4 text-justify"><strong>Es momento de evaluarte y valorarte con el fin de hacer conciencia
            todo tu camino en estos días. Recuerda que el autoconocimiento nos permite saber nuestros
            logros, tropiezos, dificultadoes o facilidades particulares.
        </strong></p>
    <br>
    <p class="h4 text-justify"><strong>Es muy importante que seas lo más sincero(a) y honesto(a) posible en el siguiente
            cuestionario, ya que, permitirá saber más de ti, para realizar los ajustes correspondientes a tu
            plan de alimentación de seguimiento, continuar apoyándote y estar a tu lado en el logro de los
            objetivos.
        </strong></p>
    <hr>

    <style>
        .flex-container {
            display: flex;
            justify-content: center;
        }

        label {
            display: block;
            text-align: center;
            /* line-height: 150%;
                                            font-size: .85em; */
        }
    </style>
    <p class="h4 text-center"> 1. ¿Cómo consideras que te fue?</p>
    <div class="row">
        <div class="flex-container">
            <br>
            <div class="col-sm-2">
                <label for="formItem">Excelente</label>
                <input type="radio" value="excelente" name="comoTeFue" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label for="formItem">Bien</label>
                <input type="radio" value="bien" name="comoTeFue" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label for="formItem">Regular</label>
                <input type="radio" value="regular" name="comoTeFue" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label for="formItem">Mal</label>
                <input type="radio" value="mal" name="comoTeFue" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label for="formItem">Muy mal</label>
                <input type="radio" value="muyMal" name="comoTeFue" class="form-control input-sm">
            </div>
        </div>
    </div>

    <hr>
    <p class="h4 text-center"> 2. ¿Cómo te sentiste con el plan de alimentación?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Excelente</label>
                <input type="radio" value="excelente" name="comoTeSentiste" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Bien</label>
                <input type="radio" value="bien" name="comoTeSentiste" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Regular</label>
                <input type="radio" value="regular" name="comoTeSentiste" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Mal</label>
                <input type="radio" value="mal" name="comoTeSentiste" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Muy mal</label>
                <input type="radio" value="muyMal" name="comoTeSentiste" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? (Sentiste dolor de cabeza, mareo, hambre, quedabas no satisfecho(a),
                    te sentiste o no con energía, tuviste ansiedad, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="comoTeSentistePQ">
            </div>
        </div>
    </div>
    <hr>

    <p class="h4 text-center"> 3. ¿Te costó trabajo llevar a cabo el plan de alimentación?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Sí</label>
                <input type="radio" value="si" name="costoTrabajoAlimentacion" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>No</label>
                <input type="radio" value="no" name="costoTrabajoAlimentacion" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? (Se te hizo complicado, no contabas con ciertos
                    alimentos, tuviste reuniones, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="costoTrabajoAlimentacionPQ">
            </div>
        </div>
    </div>
    <hr>

    <p class="h4 text-center"> 4. ¿Te gustó la dinámica de plan de alimentación?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Sí</label>
                <input type="radio" value="si" name="dinamicaPlanAlimentacion" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>No</label>
                <input type="radio" value="no" name="dinamicaPlanAlimentacion" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? (Se te hizo complicado, no contabas con ciertos
                    elementos, tuviste reuniones, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="dinamicaPlanAlimentacionPQ">
            </div>
        </div>
    </div>
    <hr>

    <p class="h4 text-center"> 5. ¿Te gustaron las recetas?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Sí</label>
                <input type="radio" value="si" name="gustarRecetas" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>No</label>
                <input type="radio" value="no" name="gustarRecetas" class="form-control input-sm">
            </div>
        </div>
    </div>


    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? (Pudiste prepararlas sin problemas, te gustó
                    o no el resultado al cocincarlas, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="gustarRecetasPQ">
            </div>
        </div>
    </div>
    <hr>

    <p class="h4 text-center"> 6. ¿Sentiste algún cambio en tu cuerpo?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Sí</label>
                <input type="radio" value="si" name="cambioCuerpo" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>No</label>
                <input type="radio" value="no" name="cambioCuerpo" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="h4 text-center">¿Por qué? ¿Cuál o cuáles? (Te probaste ropa y te quedó mejor,
                    tu cuertpo lo ves mejor o no en el espejo, tus brazos o abdomen o piernas se ven mejor, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="cambioCuerpoPQ">
            </div>
        </div>
    </div>
    <hr>

    <p class="h4 text-center"> 7. Toma las siguientes medidas de preferencia por la mañana y con ropa deportiva o interior</p>
    <br>
    <div class="row">
        <div class="flex-container">
            <!-- <p class="h4 text-center"> 2. ¿Cómo te sentiste con el plan de alimentación?</p> -->
            <div class="col-sm-3">
                <label>Peso en kg</label>
                <input type="input" name="peso" class="form-control input-sm">
            </div>
            <div class="col-sm-3">
                <label>Cintura en cm</label>
                <input type="input" name="cintura" class="form-control input-sm">
            </div>
            <div class="col-sm-3">
                <label>Abdomen en cm</label>
                <input type="input" name="abdomen" class="form-control input-sm">
            </div>
            <div class="col-sm-3">
                <label>Cadera en cm</label>
                <input type="input" name="cadera" class="form-control input-sm">
            </div>
        </div>
    </div>

    <hr>
    <p class="h4 text-center"> 8. ¿Cómo consideras que te fue?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Excelente</label>
                <input type="radio" value="excelente" name="comoConsiderasTeFue" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Bien</label>
                <input type="radio" value="bien" name="comoConsiderasTeFue" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Regular</label>
                <input type="radio" value="regular" name="comoConsiderasTeFue" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Mal</label>
                <input type="radio" value="mal" name="comoConsiderasTeFue" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Muy mal</label>
                <input type="radio" value="muyMal" name="comoConsiderasTeFue" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? (Te motivo o no te motivo, tuviste o no
                    tiempo, te daba o no flojera, decidia, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="comoConsiderasTeFuePQ">
            </div>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">
                    9. ¿Cuántas veces a la semana realizaste la rutina de entrenamiento, qué días y en qué horario?
                </label>
                <input type="text" class="form-control input-sm" name="cuantasVecesRutina">
            </div>
        </div>
    </div>
    <hr>


    <p class="h4 text-center"> 10. ¿Cómo te sentiste con el plan de entrenamiento?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Excelente</label>
                <input type="radio" value="excelente" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Bien</label>
                <input type="radio" value="bien" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Regular</label>
                <input type="radio" value="regular" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Mal</label>
                <input type="radio" value="mal" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>Muy mal</label>
                <input type="radio" value="muyMal" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? (Sentiste dolor de cabeza, mareo, hambre,
                    quedabas o no satisfecho(a), te sentiste o no con energía, tuviste ansiedad, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="comoSentistePlanEntrenamientoPQ">
            </div>
        </div>
    </div>
    <hr>

    <p class="h4 text-center"> 11. ¿Te gustó el plan de entrenamiento?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Sí</label>
                <input type="radio" value="si" name="teGustoPlanEntrenamiento" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>No</label>
                <input type="radio" value="no" name="teGustoPlanEntrenamiento" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? (Le entendiste o no, crees o no que
                    trabajaste adecuadamente, te costó, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="teGustoPlanEntrenamientoPQ">
            </div>
        </div>
    </div>

    <hr>

    <p class="h4 text-center"> 12. ¿Te costó trabajo llevar a cabo el plan de entrenamiento?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Sí</label>
                <input type="radio" value="si" name="costoTrabajoEntrenamiento" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>No</label>
                <input type="radio" value="no" name="costoTrabajoEntrenamiento" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? (Se te hizo complicado, difícil
                    o fácil, tuviste reuniones, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="costoTrabajoEntrenamientoPQ">
            </div>
        </div>
    </div>

    <hr>

    <p class="h4 text-center"> 13. ¿Sentiste algún cambio en tu cuerpo?</p>
    <div class="row">
        <div class="flex-container">
            <div class="col-sm-2">
                <label>Sí</label>
                <input type="radio" value="si" name="sentirCambioCuerpo" class="form-control input-sm">
            </div>
            <div class="col-sm-2">
                <label>No</label>
                <input type="radio" value="no" name="sentirCambioCuerpo" class="form-control input-sm">
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="flex-container">
            <div class="col-sm-12">
                <label class="text-center h4">¿Por qué? ¿Cuál o cuáles? (Sientes o no más firmeza
                    en alguna zona del cuerpo, tienes más condición, etc.)
                </label>
                <input type="text" class="form-control input-sm" name="sentirCambioCuerpoPQ">
            </div>
        </div>
    </div>
</div>