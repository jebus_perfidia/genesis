 <div class="container">
 <h4 class="text-justify"><strong>Marque la respuesta correcta. Puede seleccionar más de una.
         Selecciones las veces de la respuesta marcada, así como la frecuencia, en relación a la periodicidad con la que tiene
         dichos síntomas.
         <br><br>
         Esta evaluación determinará si cuentas con alguna afección o enfermedad psico-emocional. Muchas de las preguntas
         se relacionan con los sintomas que estás experimentando. Responde a las preguntas en términos de cómo te sientes justo ahora;
         No importa cuanto tiempo has estado experimentando estos síntomas, incluso si ocurrieron hoy por primera vez.
         <br><br>
         ES MUY IMPORTANTE QUE CONTESTE CADA AFIRMACIÓN CON LO PRIMERO QUE LE VENGA A LA MENTE, SIN JUSTIFICARSE
         A SI MISMO, SIN ANALIZAR Y SOBRE TODO, SIN AUTOENGAÑARSE.
     </strong></h4>
 <hr>

 <p class="h4"><strong>Sintomas</strong></p>
 <hr>

 <h4>Dolores intensos de cabeza</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="doloresCabeza">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDoloresCabeza" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDoloresCabeza" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Mareos o vértigos</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="mareoVertigo">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesMareoVertigo" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaMareoVertigo" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Gases y molestias digestivas</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="gasesMolestia">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesGasesMolestia" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaGasesMolestia" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Disnea o dificultad para respirar</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="Disnea">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDisnea" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDisnea" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Taquicardia</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="taquicardia">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesTaquicardia" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaTaquicardia" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Fatiga o debilidad</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="fatiga">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesFatiga" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaFatiga" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Dolor en la parte superior de la espalda</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="dolorSuperiorEspalda">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDolorSuperiorEspalda" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDolorSuperiorEspalda" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>


 <h4>Dolor en la mitad de la espalda</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="dolorMitadEspalda">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDolorMitadEspalda" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDolorMitadEspalda" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Dolor en la parte baja de la espalda</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="dolorBajaEspalda">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDolorBajaEspalda" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDolorBajaEspalda" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Dolor de rodillas</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="dolorRodillas">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDolorRodillas" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDolorRodillas" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Dolor de tobillos</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="dolorTobillos">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDolorTobillos" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDolorTobillos" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Dolor de brazos al abrirlos</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="dolorBrazos">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesBrazos" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaBrazos" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Pulmones, pecho, gripa</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="pulmonesPechoGripa">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesPulmonesPechoGripa" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaPulmonesPechoGripa" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Ardor, dolor en la boca del estómago</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="ardorEstomago">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesArdorEstomago" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaArdorEstomago" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Órganos sexuales</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="organosSexuales">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesOrganosSexuales" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaOrganosSexuales" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Dolor de pecho en mujeres</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="dolorPechoMujer">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDolorPechoMujer" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDolorPechoMujer" class="form-control">
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Problemas visuales, pérdida de vista</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="problemasVisuales">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesProblemasVisuales" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaProblemasVisuales" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4><strong>Enfermedades con componentes psicológicos</strong></h4>
 <hr>

 <h4>Hipertensición</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="hipertensionECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesHipertensionECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaHipertensionECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Asma o rinitis alérgica</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="asmaRinitisECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAsmaRinitisECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAsmaRinitisECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Intestino irritable</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="intestinoECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesIntestinoECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaIntestinoECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Cefalea tensional</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="cefaleaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesCefaleaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaCefaleaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Úlceras de estómago: Angustia y enojo</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="ulcerasECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesUlcerasECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaUlcerasECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Impotencia y disfunciones sexuales</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="impotenciaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="veceImpotenciaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaImpotenciaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Alopecia</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="AlopeciaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAlopeciaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAlopeciaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Psioriasis o problemas dérmicos (acné, dermatitis, etc)</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="psioriasisECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesPsioriasisECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaPsioriasisECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Insomnio</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="insomnioECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesInsomnioECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaInsomnioECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Los riñones</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="rinionesECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesRinionesECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaRinionesECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Huesos y músculos: fracaso</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="huesosMusculoECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesHuesosMusculoECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaHuesosMusculoECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Garganta</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="gargantaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesGargantaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaGargantaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Dolor de pecho: Amor</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="dolorPechoECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesDolorPechoECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaDolorPechoECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Aborto espontáneo</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="abortoEspontaneoECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAbortoEspontaneoECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAbortoEspontaneoECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Accidentes</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="accidentesECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAccidentesECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAccidentesECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Acidez</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="acidezECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAcidezECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAcidezECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Adicciones</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="adiccionesECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAdiccionesECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="FrecuenciaAdiccionesECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Alcoholismo</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="alcoholismoECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAlcoholismoECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAlcoholismoECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Alergias</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="alergiasECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAlergiasECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAlergiasECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Alzheimer</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="alzheimerECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAlzheimerECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAlzheimerECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Amigdalitis</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="amigdalitisECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Cantidad</label>
         <select name="cantidadAmigdalitisECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAmigdalitisECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Amnesia</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="amnesiaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAmnesiaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAmnesiaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Anemia</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="anemiaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAnemiaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAnemiaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>


 <h4>Anorexia</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="anorexiaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAnorexiaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAnorexiaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Ansiedad</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="ansiedadECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAnsiedadECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAnsiedadECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Apendicitis</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="apendicitisECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesApendicitisECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaApendicitisECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Artritis</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="artritisECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesArtritisECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaArtritisECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Asma</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="asmaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesAsmaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaAsmaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Problemas del bazo</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="problemasBazoECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesProblemasBazoECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaProblemasBazoECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Boca</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="bocaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesBocaECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaBocaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>


 <h4>Brazos</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="brazosECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesBrazosECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaBrazosECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Bronquitis</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="bronquitisECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesBronquitisECP" class="form-control">
             <option selected></option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaBronquitisECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 <hr>

 <h4>Cadera</h4>
 <div class="row">
     <div class="col-md-2">
         <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
         <input type="checkbox" class="form-control" name="caderaECP">
     </div>
     <div class="col-md-4">
         <label for="field-1" class="col-md-4 control-label">Veces</label>
         <select name="vecesCaderaECP" class="form-control">
             <option selected> </option>
             <option value="ninguna">Ninguna</option>
             <option value="una">Una vez</option>
             <option value="varias">Varias veces</option>
         </select>
     </div>
     <div class="col-md-5">
         <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
         <select name="frecuenciaCaderaECP" class="form-control">
             <option selected></option>
             <option value="dia">Al dia</option>
             <option value="semana">A la semana</option>
             <option value="quincena">A la quincena</option>
             <option value="mes">Al mes</option>
             <option value="anio">Al año</option>
         </select>
     </div>
 </div>
 </div>