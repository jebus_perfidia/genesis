<ul class="nav nav-tabs">
    <li class="active"><a class="btn btn-light" href="#frm1" data-toggle="tab"><h3>Historial clínico</h3></a></li>
    <li><a class="btn btn-light" href="#frm2" data-toggle="tab"><h3>Enfermedades psicosomáticas</h3></a></li>
    <li><a class="btn btn-light" href="#frm3" data-toggle="tab"><h3>Frecuencia de consumo por grupo de alimento</h3></a></li>
    <li><a class="btn btn-light" href="#frm4" data-toggle="tab"><h3>Test neurotransmisores</h3></a></li>
    <li><a class="btn btn-light" href="#frm5" data-toggle="tab"><h3>Evaluación y valoración</h3></a></li>
</ul>
<div id="tabContent" class="tab-content">
    <div class="tab-pane fade in active" id="frm1">
        <div class="main_data">
            <?php include 'formulario_historia_clinica.php'; ?>
        </div>
    </div>
    <div class="tab-pane fade in" id="frm2">
        <div class="main_data">
            <?php include 'formulario_enfermedades_psicosomaticas.php'; ?>
        </div>
    </div>
    <div class="tab-pane fade in" id="frm3">
        <div class="main_data">
            <?php include 'formulario_frecuencia_consumo_alimento.php'; ?>
        </div>
    </div>
    <div class="tab-pane fade in" id="frm4">
        <div class="main_data">
            <?php include 'formulario_test_neurotransmisores.php'; ?>
        </div>
    </div>
    <div class="tab-pane fade in" id="frm5">
        <div class="main_data">
            <?php include 'formulario_evaluacion_valoracion.php'; ?>
        </div>
    </div>
</div>


<!-- <div class="main_data">
    <?php include 'payment_history_1.php'; ?>
</div> -->