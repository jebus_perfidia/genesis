<?php

$datos    =    $this->db->get_where('admin', array('admin_id' => ($this->session->userdata('login_user_id'))))->result_array();
foreach ($datos as $row) :


    $fechaNacimiento = $row['fechaNacimiento'];

    function calculaEdad($fechaInicio, $fechaFin)
    {

        $datetime1 = date_create($fechaInicio);
        $datetime2 = date_create($fechaFin);
        $interval = date_diff($datetime1, $datetime2);

        $edad = array();

        foreach ($interval as $row) {
            echo $edad[] = $row;
        }

        return $edad;
    }



    $datos2 = calculaEdad($fechaActual, $fechaNacimiento);


?>
    <!-- <style>
        .abs-center {
            display: flex;
            align-items: center;
            justify-content: center;
            min-height: 100vh;
        }

        .form {
            width: 450px;
        }
    </style>
    <div class="container">
        <div class="abs-center"> -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="entypo-plus-circled"></i>
                        <?php
                        //obtener fecha actual
                        $fechaActual = date('Y-m-d');
                        ?>
                        <?php echo get_phrase('Nueva Consulta  -  Fecha: ' . $fechaActual); ?>
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo form_open(site_url('client/payment_history/create'), array('class' => 'form-horizontal form-groups-bordered ajax-submit', 'enctype' => 'multipart/form-data')); ?>


                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <!--Colocamos los elementos, en este caso son 5 
                       De igual manera, colocamos el tipo de panel, ya que tal cual lo es, solo cambia por el efecto -->
                        <div class="panel panel-primary">
                            <!-- declaramos igualmente la clase para el titulo, con otros valores para el acordeon -->
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h5 class="panel-title">
                                    <!-- agregamos el elementos que va a colapsar el acordeon -->
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <p class="h4" style="color:white">Historia Clínica</p>
                                    </a>
                                </h5>
                            </div>
                            <!-- colocamos los elementos que se van a ocultar -->
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">

                                    <h4 class="text-justify"><strong>Lea y complete el siguiente formulario según corresponda. Es muy importante que contestes lo que se te solicita:</strong></h4>
                                    <hr>
                                    <h4><strong>Datos Personales</strong></h4>
                                    <!-- Guardamos la fecha en un input tipo hidden -->
                                    <input type="hidden" name="fechaHC" val="<?php echo $fechaActual; ?>">


                                    <!-- Obtenemos algunos valores desde la tabla de usuarios, son meramente informativos -->
                                    <!-- Obtenemos el nombre completo del paciente -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-2 control-label">Nombre</label>
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                                <input type="text" class="form-control" name="" value="<?php echo $row['nombre'] . ' ' . $row['aPaterno'] . ' ' . $row['aMaterno'] ?>" autofocus readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Edad y fecha de nacimiento -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-2 control-label">Edad</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                                <input type="text" class="form-control" name="" value="<?php
                                                                                                        echo $datos2[0]
                                                                                                        ?>" autofocus readonly>
                                            </div>
                                        </div>
                                        <label for="field-1" class="col-sm-2 control-label">Fecha de nacimiento</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                                <input type="text" class="form-control" name="" value="<?php echo $row['fechaNacimiento'] ?>" autofocus readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Domicilio-->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-2 control-label">Domicilio</label>
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-location"></i></span>
                                                <input type="text" class="form-control" name="" value="<?php echo $row['domicilio'] ?>" autofocus readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Teléfono e email -->

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-2 control-label">Teléfono</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-phone"></i></span>
                                                <input type="text" class="form-control" name="" value="<?php echo $row['telefono1'] ?>" autofocus readonly>
                                            </div>
                                        </div>
                                        <label for="field-1" class="col-sm-1 control-label">Email</label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-mail"></i></span>
                                                <input type="text" class="form-control" name="" value="<?php echo $row['email'] ?>" autofocus readonly>
                                            </div>
                                        </div>
                                    </div>

                                <?php endforeach; ?>


                                <!-- Datos del formulario -->

                                <!-- Estado civil -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">Estado civil</label>
                                    <div class="col-sm-10">
                                        <select id="estadoCivil" class="selectboxit" name="estadoCivil" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="soltero">Soltero</option>
                                            <option value="casado">Casado</option>
                                        </select>
                                    </div>
                                </div>


                                <!-- Hijos y cuántos -->

                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">¿Tienes hijos?</label>
                                    <div class="col-sm-4">
                                        <select id="pacienteHijos" class="selectboxit" name="pacienteHijos" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                    <!-- Habilitar este campo, solo si se coloca que sí, en el campo anterior -->
                                    <label id="lblCuantosHijos" for="field-1" class="col-sm-2 control-label">¿Cuántos?</label>
                                    <div id="divCantidadHijos" class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input id="cantidadHijos" type="text" class="form-control" name="cantidadHijos" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Género -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">Género</label>
                                    <div class="col-sm-10">
                                        <select id="pacienteGenero" class="selectboxit" name="pacienteGenero" required title="Seleccione el género">
                                            <option value="" selected disabled hidden></option>
                                            <option value="masculino">Masculino</option>
                                            <option value="femenino">Femenino</option>
                                        </select>
                                    </div>
                                </div>

                                
                                <!-- Habilitar campos solo si es sexo femenino -->
                                <!-- Embarazo y semana -->
                                <div id="divEmbarazo" class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">¿Tienes embarazo?</label>
                                    <div class="col-sm-4">
                                        <select id="pacienteEmbarazo" class="selectboxit" name="pacienteEmbarazo" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                    <!-- Habilitar campos solo si tiene embarazo -->
                                    <div id="divSemanaEmbarazo">
                                        <label for="field-1" class="col-sm-2 control-label">Indique la semana</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                                <input id="semanaEmbarazo" type="text" class="form-control" name="semanaEmbarazo" value="" autofocus title="Este campo es obligatorio." required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                                <!-- Habilitar campos solo si es sexo femenino -->
                                <!-- Lactancia y semana -->
                                <div id="divLactancia" class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">¿Tiene lactancia?</label>
                                    <div class="col-sm-4">
                                        <select id="pacienteLactancia" class="selectboxit" name="pacienteLactancia" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                    <!-- Habilitar este campo, solo si está en lactancia -->
                                    <div id="divSemanaLactancia">
                                        <label for="field-1" class="col-sm-2 control-label">Indique la semana</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                                <input id="semanaLactancia" type="text" class="form-control" name="semanaLactancia" value="" autofocus title="Este campo es obligatorio." required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Ciudad y estado -->

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Ciudad</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-location"></i></span>
                                            <input type="text" class="form-control" name="pacienteCiudad" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                    <label for="field-1" class="col-sm-2 control-label">Estado</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-location"></i></span>
                                            <input type="text" class="form-control" name="pacienteEstado" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Escolaridad y actividad laboral -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Escolaridad</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-graduation-cap"></i></span>
                                            <input type="text" class="form-control" name="pacienteEscolaridad" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Actividad laboral</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-briefcase"></i></span>
                                            <input type="text" class="form-control" name="actividadLaboral" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Objetivo de la consulta</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="objetivoConsulta" value="" autofocus title="Menciona tu objetivo de la asesoría (perder grasa corporal, mejorar habitos, aumenta músculo, etc.)." required>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <!-- Apartado de antecedentes Heredo Familiares -->
                                <h4 class="text-justify"><strong>Menciona si cuentas con antecedentes de enfermedades en tu familia, desde los abuelos, abuelas, papás, hermanos, hermanas
                                        tios, tías (favor de mencionar si es familia del padreo a la madre). Usa las siguientes abreviaciones para resolverlo:
                                        <br><br>
                                        Aba - Abuelo, Abo - Abuelo, M - Madre, P - Padre, To - Tío, Ta - Tía, Ho - Hermano, Ha - Hermana,
                                        p - Paterno, m - Materno. </strong></h4>
                                <hr>
                                <h4><strong>Antecedentes Heredo Familiares</strong></h4>

                                <!-- Sobrepeso y obesidad -->

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Sobrepeso y/o obesidad</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="sobrepesoObesidad" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Diabetes mellitus -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Diabetes mellitus</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="diabetesMellitus" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Hipertensión Arterial -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Hipertensión arterial</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="hipertensionArterial" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Dislipidemias -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Dislipidemias</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="dislipidemias" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Osteoporosis -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Osteoporosis</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="osteoporosis" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Padecimientos reumáticos -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Padecimientos reumáticos</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="padecimientosReumaticos" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Cáncer -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Cáncer</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="cancer" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Colítis -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Colítis</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="colitis" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Gastrítis -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Gastrítis</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="gastritis" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Estreñimiento -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Estreñimiento</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="estrenimiento" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Enfermedades renales -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Enfermedades renales</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="enfermedadRenal" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Enfermedades cardiacas -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Enfermedades cardiacas</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="enfermedadCardiaca" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Enfermedades Hepáticas -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Enfermedades Hepáticas</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="enfermedadHepatica" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Enfermedades respiratorias -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Enfermedades respiratorias</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="enfermedadRespiratoria" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Enfermedad tiroidea  -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Enfermedad tiroidea</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="enfermedadTiroidea" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Enfermedades del intestino -->


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Enfermedades del intestino</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                                            <input type="text" class="form-control" name="enfermedadIntestino" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <h4><strong>Antecedentes Personales Patológicos (APP)</strong></h4>

                                <!-- Gastrítis App-->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">Gastritis</label>
                                    <div class="col-sm-3">
                                        <select class="selectboxit" name="gastritisApp" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Sintomas</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="sintomasGastritisApp" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Estriñimiento App-->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">Estriñimiento</label>
                                    <div class="col-sm-3">
                                        <select class="selectboxit" name="estrenimientoApp" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Sintomas</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="sintomasEstrenimientoApp" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Agruras / Acidez -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">Agruras / acidez</label>
                                    <div class="col-sm-3">
                                        <select class="selectboxit" name="AgrurasAcidezApp" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Sintomas</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="sintomasAgrurasAcidezApp" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Inflamación abdominal / intestinal -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">Inflamación abdominal / intestinal</label>
                                    <div class="col-sm-3">
                                        <select class="selectboxit" name="InflamacionAbdominalApp" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Sintomas</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="sintomasInflamacionAbdominalApp" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Otros -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Otras (cefalea, mareos, alteración del apetito,
                                        nauseas, vómito, diarrea, flatulencias
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="otrasApp" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Desde cundo lo padece -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">¿Desde cuándo lo padece?</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="desdeCuandoPadeceApp" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Alergia o intolerancia a algún alimento o medicamente -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Alergia o intolerancia a algún alimento o medicamente</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="AlergiaAlimentoMedicamentoApp" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Enfermedad diagonsticada, medicamento que ingiere (tipo, cantidad y frecuencia) -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Enfermedad diagonsticada, medicamento que ingiere (tipo, cantidad y frecuencia)</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="EnfermedadDiagnosticadaApp" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Cirugias realizadas -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Cirugias realizadas</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="cirugiasRealizadasApp" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Lesiones -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Lesiones</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="lesionesApp" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Consumo de suplementos, complementos o vitaminas -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Consumo de suplementos, complementos o vitaminas</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="consumoSuplementosApp" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Tipo y cantidad -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Tipo y cantidad</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="consumoSuplementosTipoCantidadApp" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>
                                <hr>


                                <h4><strong>Antescedentes Personales No Patológicos</strong></h4>

                                <!-- Fumar y edad -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">¿Fumas?</label>
                                    <div class="col-sm-4">
                                        <select class="selectboxit" name="fumar" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Edad de inicio</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="edadFumar" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Frecuencia y cantidad -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Frecuencia</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="frecuenciaFumar" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Cantidad</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="cantidadFumar" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Alcohol y frecuencia -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">¿Tomas alcohol?</label>
                                    <div class="col-sm-4">
                                        <select class="selectboxit" name="alcohol" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Frecuencia</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="frecuenciaAlcohol" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- cantidad y tipo -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Cantidad</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="cantidadAlcohol" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Tipo</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="tipoAlcohol" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Drogas y frecuencia -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-2 control-label">¿Consumes drogas?</label>
                                    <div class="col-sm-4">
                                        <select class="selectboxit" name="drogas" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Frecuencia</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="frecuenciaDrogas" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Cantidad y tipo -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Cantidad</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="cantidadDrogas" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Tipo</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                            <input type="text" class="form-control" name="tipoDrogas" value="" autofocus title="Este campo es obligatorio." required>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <!-- Antecedentes Gineco-Obstétricos -->
                                <div id="DivGineObs">
                                    <h4><strong>Antecedentes Gineco-Obstétricos</strong></h4>

                                    <!-- Última mestruación -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-2 control-label">Edad de menarca</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                                <input id="edadMenarca" type="text" class="form-control" name="edadMenarca" value="" autofocus title="Este campo es obligatorio." required>
                                            </div>
                                        </div>

                                        <label for="field-1" class="col-sm-2 control-label">Última mestruación</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                                <input id="ultimaMestruacion" type="text" class="form-control" name="ultimaMestruacion" value="" autofocus title="Este campo es obligatorio." required>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Eres regular -->
                                    <div class="form-group">
                                        <label for="field-2" class="col-sm-2 control-label">¿Eres regular?</label>
                                        <div class="col-sm-4">
                                            <select id="regular" class="selectboxit" name="regular" title="Seleccione una opción">
                                                <option value="" selected disabled hidden></option>
                                                <option value="si">Sí</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    

                                    <!-- Anticonceptivos y tipo -->
                                    <div class="form-group">
                                        <label for="field-2" class="col-sm-2 control-label">¿Usas anticonceptivos?</label>
                                        <div class="col-sm-4">
                                            <select id="anticonceptivos" class="selectboxit" name="anticonceptivos" title="Seleccione una opción">
                                                <option value="" selected disabled hidden></option>
                                                <option value="si">Sí</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>

                                        <div id="divTipoAnticonceptivos">
                                            <label for="field-1" class="col-sm-2 control-label">Tipo</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                                    <input id="tipoAnticonceptivos" type="text" class="form-control" name="tipoAnticonceptivos" value="" autofocus title="Este campo es obligatorio." required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Menopausia -->
                                    <div class="form-group">
                                        <label for="field-2" class="col-sm-2 control-label">¿Tienes menopausia?</label>
                                        <div class="col-sm-4">
                                            <select id="menopausia" class="selectboxit" name="menopausia" title="Seleccione una opción">
                                                <option value="" selected disabled hidden></option>
                                                <option value="si">Sí</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>

                                        <div id="divFechaMenopausia">
                                            <label for="field-1" class="col-sm-2 control-label">Fecha de inicio</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                                                    <input id="fechaMenopausia" type="text" class="form-control" name="fechaMenopausia" value="" autofocus title="Este campo es obligatorio." required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div> <!-- Fin DivGineObs-->
                                
                                <h4><strong>Historial De Peso/Talla</strong></h4>

                                <!-- Peso alto y bajo -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Peso más bajo (kg)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="pesoBajo" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Peso más alto (kg)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="pesoAlto" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Peso habitual y actual  -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Peso habitual (kg)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="pesoHabitual" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Peso actual (kg)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="pesoActual" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Campo habilitado solo si se seleccionó la opción de embarazo-->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Peso previo al embarazo (kg)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="pesoPrevioEmbarazo" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Peso al nacer (kg)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="pesoNacer" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>
                                </div>


                                <!-- Campos habilitados solo si el paciente es menor de 18 años -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Talla del padre (cm)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="tallaPadre" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Talla de la madre (cm)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="tallaMadre" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Talla esperada (cm)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="tallaEsperada" value="" autofocus title="A partir de los 21." required>
                                        </div>
                                    </div>
                                </div>
                                <hr>


                                <h4><strong>Favor De Proporcionar Las Siguientes Medidas</strong></h4>
                                <!-- Estarura y cintura -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Estatura (cm)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="estatura" value="" autofocus title="" required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Cintura (cm)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="cintura" value="" autofocus title="Con una cinta métrica, colocarla alrededor de la parte más angosta del abdomen, generalmente es arriba del ombligo." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Abdomen y cadera -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-2 control-label">Abdomen (cm)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="abdomen" value="" autofocus title="Con una cinta métrica, colocarla alrededor de la parte más angosta del abdomen, generalmente es arriba del ombligo." required>
                                        </div>
                                    </div>

                                    <label for="field-1" class="col-sm-2 control-label">Cadera (cm)</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="cadera" value="" autofocus title="Con una cinta métrica, colocarla alrededor de la parte más ancha, alrededor de los glúteos." required>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h4><strong>Actividad Física</strong></h4>
                                <!-- Actividad física -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-3 control-label">¿Realizas actividad física?</label>
                                    <div class="col-sm-9">
                                        <select class="selectboxit" name="actividadFisica" title="Seleccione una opción">
                                            <option value="" selected disabled hidden></option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                </div>

                                <!-- Define actividad física -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Definela</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="definirActividadFisica" value="" autofocus title="Define cúal, duración y cada cuánto lo realizas." required>
                                        </div>
                                    </div>
                                </div>

                                <!-- Describe tu día -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-3 control-label">Describes qué haces en tu día</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="entypo-user"></i></span>
                                            <input type="text" class="form-control" name="describeActividadFisica" value="" autofocus title="" required>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>




























                        <!-- Formulario 2 -->

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <p class="h4" style="color:white">Enfermedades Psícosomaticas</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <h4 class="text-justify"><strong>Marque la respuesta correcta. Puede seleccionar más de una.
                                            Selecciones las veces de la respuesta marcada, así como la frecuencia, en relación a la periodicidad con la que tiene
                                            dichos síntomas.
                                            <br><br>
                                            Esta evaluación determinará si cuentas con alguna afección o enfermedad psico-emocional. Muchas de las preguntas
                                            se relacionan con los sintomas que estás experimentando. Responde a las preguntas en términos de cómo te sientes justo ahora;
                                            No importa cuanto tiempo has estado experimentando estos síntomas, incluso si ocurrieron hoy por primera vez.
                                            <br><br>
                                            ES MUY IMPORTANTE QUE CONTESTE CADA AFIRMACIÓN CON LO PRIMERO QUE LE VENGA A LA MENTE, SIN JUSTIFICARSE
                                            A SI MISMO, SIN ANALIZAR Y SOBRE TODO, SIN AUTOENGAÑARSE.
                                        </strong></h4>
                                    <hr>

                                    <p class="h4"><strong>Sintomas</strong></p>
                                    <hr>

                                    <h4>Dolores intensos de cabeza</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="doloresCabeza">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDoloresCabeza" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDoloresCabeza" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Mareos o vértigos</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="mareoVertigo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesMareoVertigo" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaMareoVertigo" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Gases y molestias digestivas</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="gasesMolestia">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesGasesMolestia" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaGasesMolestia" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Disnea o dificultad para respirar</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="Disnea">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDisnea" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDisnea" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Taquicardia</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="taquicardia">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesTaquicardia" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaTaquicardia" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Fatiga o debilidad</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="fatiga">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesFatiga" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaFatiga" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Dolor en la parte superior de la espalda</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="dolorSuperiorEspalda">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDolorSuperiorEspalda" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDolorSuperiorEspalda" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>


                                    <h4>Dolor en la mitad de la espalda</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="dolorMitadEspalda">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDolorMitadEspalda" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDolorMitadEspalda" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Dolor en la parte baja de la espalda</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="dolorBajaEspalda">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDolorBajaEspalda" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDolorBajaEspalda" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Dolor de rodillas</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="dolorRodillas">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDolorRodillas" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDolorRodillas" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Dolor de tobillos</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="dolorTobillos">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDolorTobillos" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDolorTobillos" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Dolor de brazos al abrirlos</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="dolorBrazos">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesBrazos" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaBrazos" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Pulmones, pecho, gripa</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="pulmonesPechoGripa">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesPulmonesPechoGripa" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaPulmonesPechoGripa" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Ardor, dolor en la boca del estómago</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="ardorEstomago">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesArdorEstomago" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaArdorEstomago" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Órganos sexuales</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="organosSexuales">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesOrganosSexuales" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaOrganosSexuales" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Dolor de pecho en mujeres</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="dolorPechoMujer">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDolorPechoMujer" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDolorPechoMujer" class="form-control">
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Problemas visuales, pérdida de vista</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="problemasVisuales">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesProblemasVisuales" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaProblemasVisuales" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4><strong>Enfermedades con componentes psicológicos</strong></h4>
                                    <hr>

                                    <h4>Hipertensición</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="hipertensionECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesHipertensionECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaHipertensionECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Asma o rinitis alérgica</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="asmaRinitisECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAsmaRinitisECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAsmaRinitisECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Intestino irritable</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="intestinoECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesIntestinoECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaIntestinoECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Cefalea tensional</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="cefaleaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesCefaleaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaCefaleaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Úlceras de estómago: Angustia y enojo</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="ulcerasECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesUlcerasECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaUlcerasECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Impotencia y disfunciones sexuales</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="impotenciaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="veceImpotenciaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaImpotenciaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Alopecia</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="AlopeciaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAlopeciaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAlopeciaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Psioriasis o problemas dérmicos (acné, dermatitis, etc)</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="psioriasisECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesPsioriasisECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaPsioriasisECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Insomnio</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="insomnioECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesInsomnioECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaInsomnioECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Los riñones</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="rinionesECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesRinionesECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaRinionesECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Huesos y músculos: fracaso</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="huesosMusculoECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesHuesosMusculoECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaHuesosMusculoECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Garganta</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="gargantaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesGargantaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaGargantaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Dolor de pecho: Amor</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="dolorPechoECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesDolorPechoECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaDolorPechoECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Aborto espontáneo</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="abortoEspontaneoECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAbortoEspontaneoECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAbortoEspontaneoECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Accidentes</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="accidentesECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAccidentesECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAccidentesECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Acidez</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="acidezECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAcidezECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAcidezECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Adicciones</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="adiccionesECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAdiccionesECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="FrecuenciaAdiccionesECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Alcoholismo</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="alcoholismoECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAlcoholismoECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAlcoholismoECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Alergias</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="alergiasECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAlergiasECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAlergiasECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Alzheimer</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="alzheimerECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAlzheimerECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAlzheimerECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Amigdalitis</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="amigdalitisECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Cantidad</label>
                                            <select name="cantidadAmigdalitisECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAmigdalitisECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Amnesia</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="amnesiaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAmnesiaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAmnesiaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Anemia</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="anemiaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAnemiaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAnemiaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>


                                    <h4>Anorexia</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="anorexiaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAnorexiaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAnorexiaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Ansiedad</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="ansiedadECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAnsiedadECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAnsiedadECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Apendicitis</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="apendicitisECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesApendicitisECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaApendicitisECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Artritis</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="artritisECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesArtritisECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaArtritisECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Asma</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="asmaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesAsmaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaAsmaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Problemas del bazo</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="problemasBazoECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesProblemasBazoECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaProblemasBazoECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Boca</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="bocaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesBocaECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaBocaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>


                                    <h4>Brazos</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="brazosECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesBrazosECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaBrazosECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Bronquitis</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="bronquitisECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesBronquitisECP" class="form-control">
                                                <option selected></option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaBronquitisECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>

                                    <h4>Cadera</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="field-1" class="col-md-2 control-label invisible">Marcar</label>
                                            <input type="checkbox" class="form-control" name="caderaECP">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Veces</label>
                                            <select name="vecesCaderaECP" class="form-control">
                                                <option selected> </option>
                                                <option value="ninguna">Ninguna</option>
                                                <option value="una">Una vez</option>
                                                <option value="varias">Varias veces</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Frecuencia</label>
                                            <select name="frecuenciaCaderaECP" class="form-control">
                                                <option selected></option>
                                                <option value="dia">Al dia</option>
                                                <option value="semana">A la semana</option>
                                                <option value="quincena">A la quincena</option>
                                                <option value="mes">Al mes</option>
                                                <option value="anio">Al año</option>
                                            </select>
                                        </div>
                                    </div>
                                    

                                </div>
                            </div>
                        </div>












                        <!-- Formulario 3 -->
                        <div class="panel panel-danger">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <p class="h4" style="color:white">Frecuencia de consumo por grupo de alimento</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p class="h4 text-justify"><strong>Mencione el tipo de alimentos que consume en cada grupo, la frecuencia y una cantidad con números:</strong></p>
                                    <hr>
                                    <h4>Verdura</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="verduraTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="verduraFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="verduraCantidad">
                                        </div>
                                    </div>

                                    <hr>

                                    <h4>Frutas</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="frutaTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="frutaFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="frutaCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Tortilla</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="tortillaTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="tortillaFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="torCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Pasta/arroz</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="pastaArrozTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="pastaArrozFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="pastaArrozCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Papa</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="papaTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="papaFrecuencia" class=" form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="papaCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Pan dulce/galletas</h4>
                                    <div class=" row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="panGalletaTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="panGalletaFrencuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="panGalletaCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Frijoles/lentejas/habas</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="frijolLentejaTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="frijolLentejaFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="friLenHabCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Carne res/pollo/pescado</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="resPolloPezTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="resPolloPezFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="resPollPesCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Queso fresco</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="quesoTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="quesoFrecuancia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="quesoCantida">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Huevo/jamón de pavo</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="huevoJamonTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="huevoJamonFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="huevoJamonCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Carnitas/bíceras/cortes</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="caritasBicerasTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="caritasBicerasFrecuancia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="caritasBicerasCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Leche/yogurt</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="lecheYogurtTipo">
                                        </div>
                                        <div class=" col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select ame="lecheYogurtFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" ame="lecheYogurtCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Aceite de maíz</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="aceiteMaizTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="aceiteMaizFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="aceiteMaizCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Manteca</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="mantecaTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="mantecaFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name=mantecaCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Mantequilla/crema</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="mantequillaCremaTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="mantequillaCremaFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="mantequillaCremaCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Semillas (cacahuates, nueces, almendras)</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="semillasTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="semillaFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="semillaCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Azúcar/mermelada/miel</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="azucarMermeladaTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="azucarMermeladaFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="azucarMermeladaCantidad">
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>Refresco de sabor/jugos</h4>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="field-1" class="col-md-5 control-label">Tipo</label>
                                            <input type="text" class="form-control" name="refrescoJugoTipo">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
                                            <select name="refrescoJugoFrecuencia" class="form-control">
                                                <option selected></option>
                                                <option value="verDiario">Diario</option>
                                                <option value="verSemana">Semanal</option>
                                                <option value="verQuincena">Cada 15 dias/option>
                                                <option value="verMEnsual">Mensualmente</option>
                                                <option value="verNunca">Nunca</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
                                            <input type="text" class="form-control" name="refrescoJugoCantidad">
                                        </div>
                                    </div>
                                    <hr>


                                    <p class="h4"><strong>Dieta habitual</strong></p>

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿A qué hora te levantas?'; ?></label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                                <input type="text" class="form-control" name="dietaHabitual" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿A qué hora te duermes?'; ?></label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                                <input type="text" class="form-control" name="horaDormir" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿Comes solo o acompañado?'; ?></label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                                <input type="text" class="form-control" name="comerSoloAcompaniado" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿A qué hora te vas a trabajar/escuela?'; ?></label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                                <input type="text" class="form-control" name="horaTrabajoEscuela" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿Comes rápido o lento?'; ?></label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                                <input type="text" class="form-control" name="comerRapidoLento" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿Acostumbras a comer en la calle y con qué frecuencia?'; ?></label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                                <input type="text" class="form-control" name="comerCalleFrecuencia" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <p class="h4 text-justify"><strong>De un día común, menciona los alimentos que consumiste por tiempo. Describe platillo, cantidad y tipo de alimentos que incluiste:</strong></p>
                                    <hr>
                                    <div class="row">

                                        <div class="col-md-2">
                                            <label>Tiempo</label>
                                            <input type="text" class="form-control" name="tiempo1">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Lugar</label>
                                            <input type="text" class="form-control" name="lugar1">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Hora</label>
                                            <input type="text" class="form-control" name="hora1">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Duración</label>
                                            <input type="text" class="form-control" name="duracion1">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Platillo</label>
                                            <input type="text" class="form-control" name="platillo1">
                                        </div>
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Tiempo</label>
                                            <input type="text" class="form-control" name="tiempo2">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Lugar</label>
                                            <input type="text" class="form-control" name="lugar2">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Hora</label>
                                            <input type="text" class="form-control" name="hora2">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Duración</label>
                                            <input type="text" class="form-control" name="duracion2">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Platillo</label>
                                            <input type="text" class="form-control" name="platillo2">
                                        </div>
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Tiempo</label>
                                            <input type="text" class="form-control" name="tiempo3">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Lugar</label>
                                            <input type="text" class="form-control" name="lugar3">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Hora</label>
                                            <input type="text" class="form-control" name="hora3">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Duración</label>
                                            <input type="text" class="form-control" name="duracion3">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Platillo</label>
                                            <input type="text" class="form-control" name="platillo3">
                                        </div>
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Tiempo</label>
                                            <input type="text" class="form-control" name="tiempo4">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Lugar</label>
                                            <input type="text" class="form-control" name="lugar4">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Hora</label>
                                            <input type="text" class="form-control" name="hora4">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Duración</label>
                                            <input type="text" class="form-control" name="duracion4">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Platillo</label>
                                            <input type="text" class="form-control" name="platillo4">
                                        </div>
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Tiempo</label>
                                            <input type="text" class="form-control" name="tiempo5">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Lugar</label>
                                            <input type="text" class="form-control" name="lugar5">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Hora</label>
                                            <input type="text" class="form-control" name="hora5">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Duración</label>
                                            <input type="text" class="form-control" name="duracion5">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Platillo</label>
                                            <input type="text" class="form-control" name="platillo5">
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

















                        <div class=" panel panel-warning">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <p class="h4" style="color:dark">Test de neurotransmisores</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <p class="h4 text-justify"><strong>Responde cada pregunta seleccionando verdadero o falso. Esta evaluación determinará si eres
                                            deficiente en cualquiera de los 4 neurotrasmisores. Muchas de las preguntas se relacionan con los sintomas
                                            que pudieras estar experimentando. Responde a las preguntas en términos de cómo te sientes justo ahora
                                            no importa cuanto tiempo has estado experimentando estos sintomas, incluso si ocurieron por primera vez.
                                        </strong></p>
                                    <br>
                                    <p class="h4 text-justify"><strong>ES MUY IMPORTANTE QUE CONTESTES CADA AFIRMACIÓN CON LO PRIMERO
                                            QUE TE VENGA A LA MENTE, SIN JUSTIFICARSE A SI MISMO, SIN ANALIZAR Y SOBRE TODO, SIN AUTOENGAÑARSE.
                                        </strong></p>

                                    <hr>
                                    <h3>Dopamina</h3>
                                    <hr>
                                    <h4><strong>Memoría y atención</strong></h4>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo problemas para poner mucha atención y concentrarme'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="problemaAtencion" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Necesito cafeina para despertarme'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="necesitarCafe" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo pensar lo suficientemente rápido'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="pensarRapido" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'No tengo buen rango de atención'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="rangoAtencion" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo problemas para terminar una tarea, incluso cuando me interesa'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="problemaTerminarTarea" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Soy lento para aprender nuevas ideas'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="lentoAprenderIdeas" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <hr>


                                    <h4><strong>Físico</strong></h4>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo antojos de azúcar'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="antojoAzucar" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Ha disminuido mi líbido'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="dismuidoLibido" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Duermo demasiado'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="dormirDemasiado" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo un historial de alcohol y adicción'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="historialAlcohol" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="si">Sí</option>
                                                <option value="no">No</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Recientemente me he sentido cansado sin razón aparente'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="recientementeCansado" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces experimento cansansio total sin haberme ejercitado'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="cansancioSinEjercicio" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Siempre he tenido problemas para controlar mi peso'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="problemaControlarPeso" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo poca motivación para las experiencias sexuales'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="pocaMotivacionSexual" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo problemas para levantarme de la cama por las mañanas'; ?></label>
                                        <div class="col-md-5">
                                            <select class="selectboxit" name="problemasLevantarseCama" title="Seleccione si o no">
                                                <option value="" selected disabled hidden></option>
                                                <option value="1">Verdadero</option>
                                                <option value="0">Falso</option>
                                            </select>

                                        </div>
                                    </div>


                                    <hr>
                                    <h4><strong>Personalidad</strong>
                                        <h4>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Solo me siento bien cuando sigo a los demás'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="sentirseBienDemas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'La gente parece tener ventaja de mí'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="genteVentaja" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me estoy sintiendo muy decaído o deprimido'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="sentirseDeprimido" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'La gente me ha dicho que soy demasiado tranquilo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="demasiadoTranquilo" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo pocas urgencias'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="pocasUrgencias" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Dejo que la gente me critique'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="reActFisHC" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <hr>
                                            <h4><strong>Carácter</strong></h4>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'He perdido mis capacidades de razonamiento'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="perderCapacidadRazonamiento" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo tomar buenas decisiones'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tomarBuenasDecisiones" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>




                                            <h3>Acetilcolina</h3>
                                            <hr>
                                            <h4><strong>Memoría y atención</strong></h4>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me falta imaginación'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="faltaImaginacion" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo dificultad para recordar nombres cuando conozco gente'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="dificultadRecordarNombres" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'He notado que mi capacidad de memoria está disminuyendo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="capacidadMemoriaDisminuye" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Otros me dicen que no tengo pensamientos románticos'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noPensamientosRomanticos" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo recordar los cumpleaños de mis amigos'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noRecordarcumpleanios" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'He perdido parte de mi creatividad'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="perderParteCreatividad" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>

                                            <h4><strong>Físico</strong></h4>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo insomnio'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerInsomnio" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'He perdido tono muscular'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="perderTonoMuscular" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Ya no hago ejercicio'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noHagoEjercicio" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo antojos de alimentos grasosos'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="antojoAlimentoGrasoso" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'He probado alucinógenos, LSD y otras drogas ilícitas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="probadoAlucinogenos" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Siento como mi cuerpo se estuviera separando'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="cuerpoSentirSeparado" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo respirar fácilmente'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noRespirarFacil" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>

                                            <h4><strong>Personalidad</strong></h4>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No me siento muy feliz muy seguido'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noFelizSeguido" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me siento desesperado'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="sentirseDesesperado" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Evito que los demás me hagan daño al no decirles mucho sobre mí'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noDecirMucho" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Se me hace más cómodo hacer las cosas sólo que en un grupo grande'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="hacerCosasSolo" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Otras personas se enojan por las cosas que hago'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="personasEnojanCosas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me rindo fácilmente y tiendo a ser sumiso'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="rendirFacilmente" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Raramente me siento apasionado por algo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="reActFisHC" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me gusta la rutina'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="gustaRutina" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>


                                            <h4><strong>Carácter</strong></h4>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No me preocupan las historias de los demás sólo las mías'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noPreocupaHistoriaOtros" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No pongo atención a los sentimientos de las personas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noAtencionSentimientosOtros" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No me siento optimista'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noSentirseOptimista" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Estoy obsesionado con mis deficiencias'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="obsesionadoDeficiencias" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>

                                            <h3>Gaba</h3>
                                            <hr>
                                            <h4><strong>Memoría y atención</strong></h4>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Se me hace difícil concentrarme porque soy nervioso'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="dificlConcentrarNervioso" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo recordar número telefónicos'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noRecordarTelefonos" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo problemas para encontrar la "palabra correcta'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="problemasEncontrarPalabra" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Se me dificulta recordar las cosas que son importantes'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="dificultaRecordarCosas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Sé que soy inteligente, pero me es difícil demostrarlo a los demás'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="inteligenteDificilMostrar" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Mi capacidad para concentrarme va y viene'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="capacidadConcentrarme" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Cuando leo, tengo que regresarme al mismo párrafo algunas veces para captar la información'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="leerRegresarCaptar" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Soy un pensador rápido, pero no siempre puedo explicar lo que puedo decir'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="pensarRapidoNoExplicar" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Soy un pensador rápido, pero no siempre puedo explicar lo que puedo decir'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="pensarRapidono" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <hr>

                                            <h4><strong>Físico</strong></h4>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me siento nervioso'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="sentirseNerviosos" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces tiemblo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="algunasVecesTemblar" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo dolores frecuentes de cabeza y/o espalda'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerDolorFrecuenteCabeza" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a tener dificultad para respirar'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="dificultadParaRespirar" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a tener palpitaciones cardiacas aceleradas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerPalpitacionesAceleradas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a tener las manos frías'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerManosFrias" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="si">Sí</option>
                                                        <option value="no">No</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces sudo demasiado'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="sudarDemasiado" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'En ocasiones me mareo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="ocasionesMareo" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Usualmente tengo tensión muscular'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerTensionMuscular" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a tener mariposas en el estómago'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerMariposasEstomago" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Siento antojo por alimentos agridulces'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="antojoAlimentosAgridulces" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Usualmente estoy nervioso'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="usualmenteNervioso" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me gusta el yoga porque me ayuda a relajarme'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="gustarYoga" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Comunmente me siento fatigado, incluso aunque haya dormido bien durante la noche'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="sentirseFatigadoAunDormir" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Como en exceso'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="comerExceso" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>

                                            <h4><strong>Personalidad</strong></h4>
                                            <hr>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo cambios en mi estado de ánimo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="cambiosEstadoAnimo" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me gusta hacer muchas cosas en un mismo momento, pero se me hace difícil decidir cuál haré primero'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="muchasCosasMomento" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a hacer cosas sólo porque pienso que deben ser divertidas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="cosasSoloDivertido" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Cuando las cosas están aburridas, siempre intento poner algo de emoción'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="aburridasPonerEmocion" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a ser inconstante, cambiando frecuentemente de estado de ánimo y de pensamientos'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="inconstanteAnimoPensamiento" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a emocionarme en exceso en las cosas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="emocionarseExceso" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Mis impulsos tienden a meterme en muchos problemas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="impulsosMeterseProblemas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a ser teatral y atraer la atención hacia mi mismo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="teatralAtraerAtencion" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Hablo con mi mente, sin importar la reacción que puedan tener los demás'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="hablarMente" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces tengo ataques de ira y después me siento terriblemente culpable'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="ataquesIraCulpable" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'En ocasiones digo mentiras para salir de los problemas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="ocasionesMentirasProblemas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Siempre tengo menos interés en el sexo que las personas promedio'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="interesSexoPromedio" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>

                                            <h4><strong>Carácter</strong></h4>
                                            <hr>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Yo no sigo las reglas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noSeguirReglas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>


                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'He perdido muchos amigos'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="perderAmigos" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo conservar las relaciones románticas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noConservarRelaciones" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Considero arbitrario a la ley sin tener alguna razón'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="arbitrarioLeyRazon" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Considero inútiles las reglas que puedo seguir'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="inutilesReglas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <hr>
                                            <h3>Serotonina</h3>
                                            <hr>
                                            <h4>Memoría y atención</h4>
                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No soy muy perceptivo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noMuyPerceptivo" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo recordar las cosas que he visto en el pasado'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noRecordarCosasPasado" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo un lento tiempo de reacción'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="lentoTiempoReaccion" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo poco sentido de la dirección'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="pocoSentidoDireccion" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <hr>

                                            <h4><strong>Físico</strong></h4>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo sudoraciones nocturnas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerSudiracionNocturna" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo insomnio'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerInsomnio" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a dormir en muchas posiciones diferentes para sentirme más cómodo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="dormirMuchasPosiciones" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Siempre despierto temprano por las mañanas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="despertarTemprano" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No me puedo relajar'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noPoderRelajar" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me despierto al menos dos veces por las noches'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="despertarDosVecesNoche" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Es difícil para mi volverme a quedar dormido cuando me despierto '; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="dificilDormirCuandoDespierto" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo un antojo de sal'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="tenerAntojoSal" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo menos energía para ejercitarme'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="menosEnergiaEjercicio" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>

                                            <h4><strong>Personalidad</strong></h4>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo ansiedad crónica'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="ansiedadCronica" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Me irrito fácilmente'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="irritarFacilmente" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tengo pensamientos de autodestrucción'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="pensamientosAutodestruccion" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'He tenido pensamientos suicidas en mi vida'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="pensamientoSuicidioVida" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Tiendo a mortificarme mucho en mis ideas'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="mortificarseIdeas" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Algunas veces soy tan cuadrado(a) que me he vuelto inflexible'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="serCuadradoInflexible" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'Mi imaginación me domina'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="imaginacionDomina" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'El miedo me apasiona'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="miedoApasiona" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <hr>

                                            <h4><strong>Carácter</strong></h4>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No puedo dejar de pensar en el significado de mi vida'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="pensarSignificadoVida" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'No deseo tomar riesgos por mucho tiempo'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="noDeseoTomarRiesgos" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-2" class="col-md-7 control-label"><?php echo 'La falta de significado en la vida es dolorosa para mí'; ?></label>
                                                <div class="col-md-5">
                                                    <select class="selectboxit" name="significadoVidaDoloroso" title="Seleccione si o no">
                                                        <option value="" selected disabled hidden></option>
                                                        <option value="1">Verdadero</option>
                                                        <option value="0">Falso</option>
                                                    </select>

                                                </div>
                                            </div>
                                </div>
                            </div>
                        </div>






                        <div class="panel panel-success">

                            <div class="panel-heading" role="tab" id="headingFourt">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <p class="h4" style="color:dark">Evaluación y valoración</p>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <p class="h4 text-justify"><strong>Es momento de evaluarte y valorarte con el fin de hacer conciencia
                                            todo tu camino en estos días. Recuerda que el autoconocimiento nos permite saber nuestros
                                            logros, tropiezos, dificultadoes o facilidades particulares.
                                        </strong></p>
                                    <br>
                                    <p class="h4 text-justify"><strong>Es muy importante que seas lo más sincero(a) y honesto(a) posible en el siguiente
                                            cuestionario, ya que, permitirá saber más de ti, para realizar los ajustes correspondientes a tu
                                            plan de alimentación de seguimiento, continuar apoyándote y estar a tu lado en el logro de los
                                            objetivos.
                                        </strong></p>
                                    <hr>

                                    <style>
                                        .flex-container {
                                            display: flex;
                                            justify-content: center;
                                        }

                                        label {
                                            display: block;
                                            text-align: center;
                                            /* line-height: 150%;
                                            font-size: .85em; */
                                        }
                                    </style>
                                    <p class="h4 text-center"> 1. ¿Cómo consideras que te fue?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <br>
                                            <div class="col-sm-2">
                                                <label for="formItem">Excelente</label>
                                                <input type="radio" value="excelente" name="comoTeFue" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="formItem">Bien</label>
                                                <input type="radio" value="bien" name="comoTeFue" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="formItem">Regular</label>
                                                <input type="radio" value="regular" name="comoTeFue" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="formItem">Mal</label>
                                                <input type="radio" value="mal" name="comoTeFue" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="formItem">Muy mal</label>
                                                <input type="radio" value="muyMal" name="comoTeFue" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <p class="h4 text-center"> 2. ¿Cómo te sentiste con el plan de alimentación?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Excelente</label>
                                                <input type="radio" value="excelente" name="comoTeSentiste" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Bien</label>
                                                <input type="radio" value="bien" name="comoTeSentiste" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Regular</label>
                                                <input type="radio" value="regular" name="comoTeSentiste" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Mal</label>
                                                <input type="radio" value="mal" name="comoTeSentiste" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Muy mal</label>
                                                <input type="radio" value="muyMal" name="comoTeSentiste" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? (Sentiste dolor de cabeza, mareo, hambre, quedabas no satisfecho(a),
                                                    te sentiste o no con energía, tuviste ansiedad, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="comoTeSentistePQ">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <p class="h4 text-center"> 3. ¿Te costó trabajo llevar a cabo el plan de alimentación?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Sí</label>
                                                <input type="radio" value="si" name="costoTrabajoAlimentacion" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>No</label>
                                                <input type="radio" value="no" name="costoTrabajoAlimentacion" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? (Se te hizo complicado, no contabas con ciertos
                                                    alimentos, tuviste reuniones, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="costoTrabajoAlimentacionPQ">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <p class="h4 text-center"> 4. ¿Te gustó la dinámica de plan de alimentación?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Sí</label>
                                                <input type="radio" value="si" name="dinamicaPlanAlimentacion" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>No</label>
                                                <input type="radio" value="no" name="dinamicaPlanAlimentacion" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? (Se te hizo complicado, no contabas con ciertos
                                                    elementos, tuviste reuniones, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="dinamicaPlanAlimentacionPQ">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <p class="h4 text-center"> 5. ¿Te gustaron las recetas?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Sí</label>
                                                <input type="radio" value="si" name="gustarRecetas" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>No</label>
                                                <input type="radio" value="no" name="gustarRecetas" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>


                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? (Pudiste prepararlas sin problemas, te gustó
                                                    o no el resultado al cocincarlas, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="gustarRecetasPQ">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <p class="h4 text-center"> 6. ¿Sentiste algún cambio en tu cuerpo?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Sí</label>
                                                <input type="radio" value="si" name="cambioCuerpo" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>No</label>
                                                <input type="radio" value="no" name="cambioCuerpo" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="h4 text-center">¿Por qué? ¿Cuál o cuáles? (Te probaste ropa y te quedó mejor,
                                                    tu cuertpo lo ves mejor o no en el espejo, tus brazos o abdomen o piernas se ven mejor, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="cambioCuerpoPQ">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <p class="h4 text-center"> 7. Toma las siguientes medidas de preferencia por la mañana y con ropa deportiva o interior</p>
                                    <br>
                                    <div class="row">
                                        <div class="flex-container">
                                            <!-- <p class="h4 text-center"> 2. ¿Cómo te sentiste con el plan de alimentación?</p> -->
                                            <div class="col-sm-3">
                                                <label>Peso en kg</label>
                                                <input type="input" name="peso" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Cintura en cm</label>
                                                <input type="input" name="cintura" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Abdomen en cm</label>
                                                <input type="input" name="abdomen" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Cadera en cm</label>
                                                <input type="input" name="cadera" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <p class="h4 text-center"> 8. ¿Cómo consideras que te fue?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Excelente</label>
                                                <input type="radio" value="excelente" name="comoConsiderasTeFue" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Bien</label>
                                                <input type="radio" value="bien" name="comoConsiderasTeFue" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Regular</label>
                                                <input type="radio" value="regular" name="comoConsiderasTeFue" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Mal</label>
                                                <input type="radio" value="mal" name="comoConsiderasTeFue" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Muy mal</label>
                                                <input type="radio" value="muyMal" name="comoConsiderasTeFue" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? (Te motivo o no te motivo, tuviste o no
                                                    tiempo, te daba o no flojera, decidia, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="comoConsiderasTeFuePQ">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>


                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">
                                                    9. ¿Cuántas veces a la semana realizaste la rutina de entrenamiento, qué días y en qué horario?
                                                </label>
                                                <input type="text" class="form-control input-sm" name="cuantasVecesRutina">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>


                                    <p class="h4 text-center"> 10. ¿Cómo te sentiste con el plan de entrenamiento?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Excelente</label>
                                                <input type="radio" value="excelente" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Bien</label>
                                                <input type="radio" value="bien" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Regular</label>
                                                <input type="radio" value="regular" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Mal</label>
                                                <input type="radio" value="mal" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Muy mal</label>
                                                <input type="radio" value="muyMal" name="comoSentistePlanEntrenamiento" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? (Sentiste dolor de cabeza, mareo, hambre,
                                                    quedabas o no satisfecho(a), te sentiste o no con energía, tuviste ansiedad, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="comoSentistePlanEntrenamientoPQ">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <p class="h4 text-center"> 11. ¿Te gustó el plan de entrenamiento?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Sí</label>
                                                <input type="radio" value="si" name="teGustoPlanEntrenamiento" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>No</label>
                                                <input type="radio" value="no" name="teGustoPlanEntrenamiento" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? (Le entendiste o no, crees o no que
                                                    trabajaste adecuadamente, te costó, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="teGustoPlanEntrenamientoPQ">
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <p class="h4 text-center"> 12. ¿Te costó trabajo llevar a cabo el plan de entrenamiento?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Sí</label>
                                                <input type="radio" value="si" name="costoTrabajoEntrenamiento" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>No</label>
                                                <input type="radio" value="no" name="costoTrabajoEntrenamiento" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? (Se te hizo complicado, difícil
                                                    o fácil, tuviste reuniones, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="costoTrabajoEntrenamientoPQ">
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <p class="h4 text-center"> 13. ¿Sentiste algún cambio en tu cuerpo?</p>
                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-2">
                                                <label>Sí</label>
                                                <input type="radio" value="si" name="sentirCambioCuerpo" class="form-control input-sm">
                                            </div>
                                            <div class="col-sm-2">
                                                <label>No</label>
                                                <input type="radio" value="no" name="sentirCambioCuerpo" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="flex-container">
                                            <div class="col-sm-12">
                                                <label class="text-center h4">¿Por qué? ¿Cuál o cuáles? (Sientes o no más firmeza
                                                    en alguna zona del cuerpo, tienes más condición, etc.)
                                                </label>
                                                <input type="text" class="form-control input-sm" name="sentirCambioCuerpoPQ">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-7">
                                <br><br>
                                <button type="submit" class="btn btn-info" id="submit-button"><?php echo get_phrase('Agregar registro'); ?></button>
                                <span id="preloader-form"></span>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- </div>
        </div> -->

<script type="text/javascript">
    // url for refresh data after ajax form submission
    var post_refresh_url = '<?php echo site_url('client/reload_payment_history_list'); ?>';
    var post_message = 'Usuario creado correctamente';
    var fechaActual = '<?php echo $fechaActual; ?>';
    var fechaNacimiento = '<?php echo $fechaNacimiento; ?>';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/client/payment_history_add.js'); ?>"></script>