<div class="container">
    <p class="h4 text-justify"><strong>Mencione el tipo de alimentos que consume en cada grupo, la frecuencia y una cantidad con números:</strong></p>
    <hr>
    <h4>Verdura</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="verduraTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="verduraFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="verduraCantidad">
        </div>
    </div>

    <hr>

    <h4>Frutas</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="frutaTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="frutaFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="frutaCantidad">
        </div>
    </div>

    <hr>
    <h4>Tortilla</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="tortillaTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="tortillaFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="torCantidad">
        </div>
    </div>

    <hr>
    <h4>Pasta/arroz</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="pastaArrozTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="pastaArrozFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="pastaArrozCantidad">
        </div>
    </div>

    <hr>
    <h4>Papa</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="papaTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="papaFrecuencia" class=" form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="papaCantidad">
        </div>
    </div>

    <hr>
    <h4>Pan dulce/galletas</h4>
    <div class=" row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="panGalletaTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="panGalletaFrencuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="panGalletaCantidad">
        </div>
    </div>

    <hr>
    <h4>Frijoles/lentejas/habas</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="frijolLentejaTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="frijolLentejaFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="friLenHabCantidad">
        </div>
    </div>

    <hr>
    <h4>Carne res/pollo/pescado</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="resPolloPezTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="resPolloPezFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="resPollPesCantidad">
        </div>
    </div>

    <hr>
    <h4>Queso fresco</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="quesoTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="quesoFrecuancia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="quesoCantida">
        </div>
    </div>

    <hr>
    <h4>Huevo/jamón de pavo</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="huevoJamonTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="huevoJamonFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="huevoJamonCantidad">
        </div>
    </div>

    <hr>
    <h4>Carnitas/bíceras/cortes</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="caritasBicerasTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="caritasBicerasFrecuancia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="caritasBicerasCantidad">
        </div>
    </div>

    <hr>
    <h4>Leche/yogurt</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="lecheYogurtTipo">
        </div>
        <div class=" col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select ame="lecheYogurtFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" ame="lecheYogurtCantidad">
        </div>
    </div>

    <hr>
    <h4>Aceite de maíz</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="aceiteMaizTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="aceiteMaizFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="aceiteMaizCantidad">
        </div>
    </div>

    <hr>
    <h4>Manteca</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="mantecaTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="mantecaFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name=mantecaCantidad">
        </div>
    </div>

    <hr>
    <h4>Mantequilla/crema</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="mantequillaCremaTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="mantequillaCremaFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="mantequillaCremaCantidad">
        </div>
    </div>

    <hr>
    <h4>Semillas (cacahuates, nueces, almendras)</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="semillasTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="semillaFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="semillaCantidad">
        </div>
    </div>

    <hr>
    <h4>Azúcar/mermelada/miel</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="azucarMermeladaTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="azucarMermeladaFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="azucarMermeladaCantidad">
        </div>
    </div>

    <hr>
    <h4>Refresco de sabor/jugos</h4>
    <div class="row">
        <div class="col-md-5">
            <label for="field-1" class="col-md-5 control-label">Tipo</label>
            <input type="text" class="form-control" name="refrescoJugoTipo">
        </div>
        <div class="col-md-4">
            <label for="field-1" class="col-md-4 control-label">Frecuencia</label>
            <select name="refrescoJugoFrecuencia" class="form-control">
                <option selected></option>
                <option value="verDiario">Diario</option>
                <option value="verSemana">Semanal</option>
                <option value="verQuincena">Cada 15 dias/option>
                <option value="verMEnsual">Mensualmente</option>
                <option value="verNunca">Nunca</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="field-1" class="col-md-3 control-label">Cantidad</label>
            <input type="text" class="form-control" name="refrescoJugoCantidad">
        </div>
    </div>
    <hr>


    <p class="h4"><strong>Dieta habitual</strong></p>

    <div class="form-group">
        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿A qué hora te levantas?'; ?></label>
        <div class="col-sm-7">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-user"></i></span>
                <input type="text" class="form-control" name="dietaHabitual" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿A qué hora te duermes?'; ?></label>
        <div class="col-sm-7">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-user"></i></span>
                <input type="text" class="form-control" name="horaDormir" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿Comes solo o acompañado?'; ?></label>
        <div class="col-sm-7">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-user"></i></span>
                <input type="text" class="form-control" name="comerSoloAcompaniado" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿A qué hora te vas a trabajar/escuela?'; ?></label>
        <div class="col-sm-7">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-user"></i></span>
                <input type="text" class="form-control" name="horaTrabajoEscuela" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿Comes rápido o lento?'; ?></label>
        <div class="col-sm-7">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-user"></i></span>
                <input type="text" class="form-control" name="comerRapidoLento" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="field-1" class="col-sm-4 control-label"><?php echo '¿Acostumbras a comer en la calle y con qué frecuencia?'; ?></label>
        <div class="col-sm-7">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-user"></i></span>
                <input type="text" class="form-control" name="comerCalleFrecuencia" value="" autofocus placeholder="Introduzca la ciudad donde habita" title="Este campo es obligatorio" required>
            </div>
        </div>
    </div>
    <hr>

    <p class="h4 text-justify"><strong>De un día común, menciona los alimentos que consumiste por tiempo. Describe platillo, cantidad y tipo de alimentos que incluiste:</strong></p>
    <hr>
    <div class="row">

        <div class="col-md-2">
            <label>Tiempo</label>
            <input type="text" class="form-control" name="tiempo1">
        </div>
        <div class="col-md-2">
            <label>Lugar</label>
            <input type="text" class="form-control" name="lugar1">
        </div>
        <div class="col-md-2">
            <label>Hora</label>
            <input type="text" class="form-control" name="hora1">
        </div>
        <div class="col-md-2">
            <label>Duración</label>
            <input type="text" class="form-control" name="duracion1">
        </div>
        <div class="col-md-4">
            <label>Platillo</label>
            <input type="text" class="form-control" name="platillo1">
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-2">
            <label>Tiempo</label>
            <input type="text" class="form-control" name="tiempo2">
        </div>
        <div class="col-md-2">
            <label>Lugar</label>
            <input type="text" class="form-control" name="lugar2">
        </div>
        <div class="col-md-2">
            <label>Hora</label>
            <input type="text" class="form-control" name="hora2">
        </div>
        <div class="col-md-2">
            <label>Duración</label>
            <input type="text" class="form-control" name="duracion2">
        </div>
        <div class="col-md-4">
            <label>Platillo</label>
            <input type="text" class="form-control" name="platillo2">
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-2">
            <label>Tiempo</label>
            <input type="text" class="form-control" name="tiempo3">
        </div>
        <div class="col-md-2">
            <label>Lugar</label>
            <input type="text" class="form-control" name="lugar3">
        </div>
        <div class="col-md-2">
            <label>Hora</label>
            <input type="text" class="form-control" name="hora3">
        </div>
        <div class="col-md-2">
            <label>Duración</label>
            <input type="text" class="form-control" name="duracion3">
        </div>
        <div class="col-md-4">
            <label>Platillo</label>
            <input type="text" class="form-control" name="platillo3">
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-2">
            <label>Tiempo</label>
            <input type="text" class="form-control" name="tiempo4">
        </div>
        <div class="col-md-2">
            <label>Lugar</label>
            <input type="text" class="form-control" name="lugar4">
        </div>
        <div class="col-md-2">
            <label>Hora</label>
            <input type="text" class="form-control" name="hora4">
        </div>
        <div class="col-md-2">
            <label>Duración</label>
            <input type="text" class="form-control" name="duracion4">
        </div>
        <div class="col-md-4">
            <label>Platillo</label>
            <input type="text" class="form-control" name="platillo4">
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-2">
            <label>Tiempo</label>
            <input type="text" class="form-control" name="tiempo5">
        </div>
        <div class="col-md-2">
            <label>Lugar</label>
            <input type="text" class="form-control" name="lugar5">
        </div>
        <div class="col-md-2">
            <label>Hora</label>
            <input type="text" class="form-control" name="hora5">
        </div>
        <div class="col-md-2">
            <label>Duración</label>
            <input type="text" class="form-control" name="duracion5">
        </div>
        <div class="col-md-4">
            <label>Platillo</label>
            <input type="text" class="form-control" name="platillo5">
        </div>
    </div>
</div>