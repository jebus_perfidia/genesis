<div class="sidebar-menu">
    <header class="logo-env">
        <!-- logo collapse icon -->
        <!-- <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div> -->

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

        <div style="text-align: -webkit-center;" id="branding_element">
            <img src="<?php echo base_url('assets/logo.png'); ?>" style="max-height:35px;" />
            <h4 style="color: #a2a3b7;text-align: -webkit-center;margin-bottom: 25px;font-weight: 600;
            margin-top: 10px; letter-spacing: 4px; font-size: 18px;">
                Génesis<?php //echo $system_name;
                        ?>
            </h4>
        </div>

        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?>">
            <a href="<?php echo site_url('client/dashboard'); ?>">
                <i class="fa fa-home"></i>
                <span><?php echo get_phrase('Inicio'); ?></span>
            </a>
        </li>

        <!-- MANAGE PROJECTS -->

        <!--  <li class="<?php if (
                                $page_name == 'project' ||
                                $page_name == 'project_room' ||
                                $page_name == 'project_quote' ||
                                $page_name == 'project_milestone_stripe_pay' ||
                                $page_name == 'project_quote_view'
                            )
                                echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="entypo-paper-plane"></i>
                <span><?php echo get_phrase('project'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'project') echo 'active'; ?>">
                    <a href="<?php echo site_url('client/project'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('project_list'); ?></span>
                    </a>
                </li> -->
        <!-- <li class="<?php if ($page_name == 'project_quote' || $page_name == 'project_quote_view') echo 'active'; ?>">
                    <a href="<?php echo site_url('client/project_quote'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('project_quote'); ?></span>
                    </a>
                </li> -->
        <!--   </ul>
        </li>
 -->
        <!-- PAYMENT HISTORY -->

        <li class="<?php if ($page_name == 'payments_history') echo 'active'; ?>">
            <a href="<?php echo site_url('client/payment_history'); ?>">
                <i class="entypo-credit-card"></i>
                <span><?php echo get_phrase('M&oacute;dulo de diagn&oacute;stico'); ?></span>
            </a>
        </li>

        <!-- NOTEs -->

        <li class="<?php if ($page_name == 'note') echo 'active'; ?>">
            <a href="<?php echo site_url('client/note'); ?>">
                <i class="entypo-doc-text"></i>
                <span><?php echo get_phrase('Ver sistema de intercambio y equivalencias'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'noticeboard') echo 'active'; ?>">
            <a href="<?php echo site_url('client/noticeboard'); ?>">
                <i class="entypo-newspaper"></i>
                <span><?php echo get_phrase('Plan de adaptaci&oacute;n familiar'); ?></span>
            </a>
        </li>

        <!-- MESSAGE -->

        <li class="<?php if ($page_name == 'message') echo 'active'; ?>">
            <a href="<?php echo site_url('client/message'); ?>">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('Ver alimentaci&oacute;n'); ?></span>
            </a>
        </li>


        <!-- MESSAGE -->

        <li class="<?php if ($page_name == 'message') echo 'active'; ?>">
            <a href="<?php echo site_url('client/message'); ?>">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('Ver recomendaciones y suplementaci&oacute;n'); ?></span>
            </a>
        </li>

        <!-- MESSAGE -->

        <li class="<?php if ($page_name == 'message') echo 'active'; ?>">
            <a href="<?php echo site_url('client/message'); ?>">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('Ver actividad f&iacute;sica'); ?></span>
            </a>
        </li>



        <!-- MESSAGE -->

        <li class="<?php if ($page_name == 'message') echo 'active'; ?>">
            <a href="<?php echo site_url('client/message'); ?>">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('Descargar Ebook "Plan de contingencia"'); ?></span>
            </a>
        </li>



        <!-- MESSAGE -->

        <li class="<?php if ($page_name == 'message') echo 'active'; ?>">
            <a href="<?php echo site_url('client/message'); ?>">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('Edicion de empresa'); ?></span>
            </a>
        </li>


        <!-- MESSAGE -->

        <li class="<?php if ($page_name == 'message') echo 'active'; ?>">
            <a href="<?php echo site_url('client/message'); ?>">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('Usuarios'); ?></span>
            </a>
        </li>


        <!-- MESSAGE -->

        <li class="<?php if ($page_name == 'message') echo 'active'; ?>">
            <a href="<?php echo site_url('client/message'); ?>">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('Convenio'); ?></span>
            </a>
        </li>

        <!-- MESSAGE -->

        <?php if ($this->session->userdata('client_type') == 1) { ?>
            <li class="<?php if ($page_name == 'message') echo 'active'; ?>">
                <a href="<?php echo site_url('client/admin_empresa'); ?>">
                    <i class="entypo-network"></i>
                    <span><?php echo get_phrase('Gestionar empresa'); ?></span>
                </a>
            </li>
        <?php }; ?>


        <!-- SUPPORT TICKET -->

        <!--      <li class="<?php if (
                                    $page_name == 'support_ticket_create' ||
                                    $page_name == 'support_ticket' ||
                                    $page_name == 'support_ticket_view'
                                )
                                    echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="entypo-lifebuoy"></i>
                <span><?php echo get_phrase('support'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'support_ticket') echo 'active'; ?>">
                    <a href="<?php echo site_url('client/support_ticket'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('ticket_list'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'support_ticket_create') echo 'active'; ?>">
                    <a href="<?php echo site_url('client/support_ticket_create'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('create_ticket'); ?></span>
                    </a>
                </li>
            </ul>
        </li> -->

        <!-- ACCOUNT -->

        <!-- <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?>">
            <a href="<?php echo site_url('client/manage_profile'); ?>">
                <i class="entypo-lock"></i>
                <span><?php echo get_phrase('account'); ?></span>
            </a>
        </li>

    </ul> -->

</div>