<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo 'Editar información';?>
                </div>
            </div>
            <div class="panel-body">
                <?php 
                foreach($edit_data as $row): ?>
                    <?php echo form_open(site_url('client/manage_profile/update_profile_info'), array(
                    'class' => 'form-horizontal form-groups-bordered' , 'enctype' => 'multipart/form-data'));?>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo 'Nombre de la empresa';?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="usuario" value="<?php echo $row['nombreEmpresa'];?>" readonly="readonly" required/>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo 'Razón social';?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="nombre" value="<?php echo $row['razonSocial'];?>" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo 'Domicilio';?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="aPaterno" value="<?php echo $row['domicilio'];?>" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo 'R.F.C.';?></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="aMaterno" value="<?php echo $row['rfc'];?>" required/>
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-5">
                              <button type="submit" class="btn btn-info"><?php echo get_phrase('Actualizar informaci&oacute;n');?></button>
                          </div>
                        </div>
                    <?php echo form_close();?>
                    <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>
</div>
