<?php

$datos    =    $this->db->get_where('admin', array('admin_id' => ($this->session->userdata('login_user_id'))))->result_array();
foreach ($datos as $row) :


    $fechaNacimiento = $row['fechaNacimiento'];

    function calculaEdad($fechaInicio, $fechaFin)
    {

        $datetime1 = date_create($fechaInicio);
        $datetime2 = date_create($fechaFin);
        $interval = date_diff($datetime1, $datetime2);

        $edad = array();

        foreach ($interval as $row) {
            echo $edad[] = $row;
        }

        return $edad;
    }



    $datos2 = calculaEdad($fechaActual, $fechaNacimiento);


?>
    <div class="container">

        <form>
            <h4 class="text-justify"><strong>Lea y complete el siguiente formulario según corresponda. Es muy importante que contestes lo que se te solicita:</strong></h4>
            <hr>
            <h4><strong>Datos Personales</strong></h4>
            <!-- Guardamos la fecha en un input tipo hidden -->
            <input type="hidden" name="fechaHC" val="<?php echo $fechaActual; ?>">


            <!-- Obtenemos algunos valores desde la tabla de usuarios, son meramente informativos -->
            <!-- Obtenemos el nombre completo del paciente -->
            <div class="form-group">
                <label for="field-1" class="col-sm-2 control-label">Nombre</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-user"></i></span>
                        <input type="text" class="form-control" name="" value="<?php echo $row['nombre'] . ' ' . $row['aPaterno'] . ' ' . $row['aMaterno'] ?>" autofocus readonly>
                    </div>
                </div>
            </div>

            <!-- Edad y fecha de nacimiento -->
            <div class="form-group">
                <label for="field-1" class="col-sm-2 control-label">Edad</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-user"></i></span>
                        <input type="text" class="form-control" name="" value="<?php
                                                                                echo $datos2[0]
                                                                                ?>" autofocus readonly>
                    </div>
                </div>
                <label for="field-1" class="col-sm-2 control-label">Fecha de nacimiento</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                        <input type="text" class="form-control" name="" value="<?php echo $row['fechaNacimiento'] ?>" autofocus readonly>
                    </div>
                </div>
            </div>

            <!-- Domicilio-->
            <div class="form-group">
                <label for="field-1" class="col-sm-2 control-label">Domicilio</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-location"></i></span>
                        <input type="text" class="form-control" name="" value="<?php echo $row['domicilio'] ?>" autofocus readonly>
                    </div>
                </div>
            </div>

            <!-- Teléfono e email -->

            <div class="form-group">
                <label for="field-1" class="col-sm-2 control-label">Teléfono</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-phone"></i></span>
                        <input type="text" class="form-control" name="" value="<?php echo $row['telefono1'] ?>" autofocus readonly>
                    </div>
                </div>
                <label for="field-1" class="col-sm-1 control-label">Email</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-mail"></i></span>
                        <input type="text" class="form-control" name="" value="<?php echo $row['email'] ?>" autofocus readonly>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>


        <!-- Datos del formulario -->

        <!-- Estado civil -->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">Estado civil</label>
            <div class="col-sm-10">
                <select id="estadoCivil" class="selectboxit" name="estadoCivil" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="soltero">Soltero</option>
                    <option value="casado">Casado</option>
                </select>
            </div>
        </div>


        <!-- Hijos y cuántos -->

        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">¿Tienes hijos?</label>
            <div class="col-sm-4">
                <select id="pacienteHijos" class="selectboxit" name="pacienteHijos" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>
            <!-- Habilitar este campo, solo si se coloca que sí, en el campo anterior -->
            <label id="lblCuantosHijos" for="field-1" class="col-sm-2 control-label">¿Cuántos?</label>
            <div id="divCantidadHijos" class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input id="cantidadHijos" type="text" class="form-control" name="cantidadHijos" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>


        <!-- Género -->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">Género</label>
            <div class="col-sm-10">
                <select id="pacienteGenero" class="selectboxit" name="pacienteGenero" required title="Seleccione el género">
                    <option value="" selected disabled hidden></option>
                    <option value="masculino">Masculino</option>
                    <option value="femenino">Femenino</option>
                </select>
            </div>
        </div>


        <!-- Habilitar campos solo si es sexo femenino -->
        <!-- Embarazo y semana -->
        <div id="divEmbarazo" class="form-group">
            <label for="field-2" class="col-sm-2 control-label">¿Tienes embarazo?</label>
            <div class="col-sm-4">
                <select id="pacienteEmbarazo" class="selectboxit" name="pacienteEmbarazo" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>
            <!-- Habilitar campos solo si tiene embarazo -->
            <div id="divSemanaEmbarazo">
                <label for="field-1" class="col-sm-2 control-label">Indique la semana</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                        <input id="semanaEmbarazo" type="text" class="form-control" name="semanaEmbarazo" value="" autofocus title="Este campo es obligatorio." required>
                    </div>
                </div>
            </div>
        </div>


        <!-- Habilitar campos solo si es sexo femenino -->
        <!-- Lactancia y semana -->
        <div id="divLactancia" class="form-group">
            <label for="field-2" class="col-sm-2 control-label">¿Tiene lactancia?</label>
            <div class="col-sm-4">
                <select id="pacienteLactancia" class="selectboxit" name="pacienteLactancia" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>
            <!-- Habilitar este campo, solo si está en lactancia -->
            <div id="divSemanaLactancia">
                <label for="field-1" class="col-sm-2 control-label">Indique la semana</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                        <input id="semanaLactancia" type="text" class="form-control" name="semanaLactancia" value="" autofocus title="Este campo es obligatorio." required>
                    </div>
                </div>
            </div>
        </div>

        <!-- Ciudad y estado -->

        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Ciudad</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-location"></i></span>
                    <input type="text" class="form-control" name="pacienteCiudad" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
            <label for="field-1" class="col-sm-2 control-label">Estado</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-location"></i></span>
                    <input type="text" class="form-control" name="pacienteEstado" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <!-- Escolaridad y actividad laboral -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Escolaridad</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-graduation-cap"></i></span>
                    <input type="text" class="form-control" name="pacienteEscolaridad" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Actividad laboral</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-briefcase"></i></span>
                    <input type="text" class="form-control" name="actividadLaboral" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>


        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Objetivo de la consulta</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="objetivoConsulta" value="" autofocus title="Menciona tu objetivo de la asesoría (perder grasa corporal, mejorar habitos, aumenta músculo, etc.)." required>
                </div>
            </div>
        </div>
        <hr>

        <!-- Apartado de antecedentes Heredo Familiares -->
        <h4 class="text-justify"><strong>Menciona si cuentas con antecedentes de enfermedades en tu familia, desde los abuelos, abuelas, papás, hermanos, hermanas
                tios, tías (favor de mencionar si es familia del padreo a la madre). Usa las siguientes abreviaciones para resolverlo:
                <br><br>
                Aba - Abuelo, Abo - Abuelo, M - Madre, P - Padre, To - Tío, Ta - Tía, Ho - Hermano, Ha - Hermana,
                p - Paterno, m - Materno. </strong></h4>
        <hr>
        <h4><strong>Antecedentes Heredo Familiares</strong></h4>

        <!-- Sobrepeso y obesidad -->

        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Sobrepeso y/o obesidad</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="sobrepesoObesidad" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Diabetes mellitus -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Diabetes mellitus</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="diabetesMellitus" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Hipertensión Arterial -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Hipertensión arterial</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="hipertensionArterial" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Dislipidemias -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Dislipidemias</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="dislipidemias" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Osteoporosis -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Osteoporosis</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="osteoporosis" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Padecimientos reumáticos -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Padecimientos reumáticos</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="padecimientosReumaticos" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Cáncer -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Cáncer</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="cancer" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Colítis -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Colítis</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="colitis" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Gastrítis -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Gastrítis</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="gastritis" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Estreñimiento -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Estreñimiento</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="estrenimiento" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Enfermedades renales -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Enfermedades renales</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="enfermedadRenal" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Enfermedades cardiacas -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Enfermedades cardiacas</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="enfermedadCardiaca" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Enfermedades Hepáticas -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Enfermedades Hepáticas</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="enfermedadHepatica" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Enfermedades respiratorias -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Enfermedades respiratorias</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="enfermedadRespiratoria" value="" autofocus title="" required>
                </div>
            </div>
        </div>


        <!-- Enfermedad tiroidea  -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Enfermedad tiroidea</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="enfermedadTiroidea" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Enfermedades del intestino -->


        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Enfermedades del intestino</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-flow-tree"></i></span>
                    <input type="text" class="form-control" name="enfermedadIntestino" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <hr>
        <h4><strong>Antecedentes Personales Patológicos (APP)</strong></h4>

        <!-- Gastrítis App-->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">Gastritis</label>
            <div class="col-sm-3">
                <select class="selectboxit" name="gastritisApp" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Sintomas</label>
            <div class="col-sm-5">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="sintomasGastritisApp" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <!-- Estriñimiento App-->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">Estriñimiento</label>
            <div class="col-sm-3">
                <select class="selectboxit" name="estrenimientoApp" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Sintomas</label>
            <div class="col-sm-5">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="sintomasEstrenimientoApp" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>


        <!-- Agruras / Acidez -->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">Agruras / acidez</label>
            <div class="col-sm-3">
                <select class="selectboxit" name="AgrurasAcidezApp" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Sintomas</label>
            <div class="col-sm-5">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="sintomasAgrurasAcidezApp" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <!-- Inflamación abdominal / intestinal -->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">Inflamación abdominal / intestinal</label>
            <div class="col-sm-3">
                <select class="selectboxit" name="InflamacionAbdominalApp" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Sintomas</label>
            <div class="col-sm-5">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="sintomasInflamacionAbdominalApp" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <!-- Otros -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Otras (cefalea, mareos, alteración del apetito,
                nauseas, vómito, diarrea, flatulencias
            </label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="otrasApp" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Desde cundo lo padece -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">¿Desde cuándo lo padece?</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="desdeCuandoPadeceApp" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Alergia o intolerancia a algún alimento o medicamente -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Alergia o intolerancia a algún alimento o medicamente</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="AlergiaAlimentoMedicamentoApp" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Enfermedad diagonsticada, medicamento que ingiere (tipo, cantidad y frecuencia) -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Enfermedad diagonsticada, medicamento que ingiere (tipo, cantidad y frecuencia)</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="EnfermedadDiagnosticadaApp" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Cirugias realizadas -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Cirugias realizadas</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="cirugiasRealizadasApp" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Lesiones -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Lesiones</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="lesionesApp" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Consumo de suplementos, complementos o vitaminas -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Consumo de suplementos, complementos o vitaminas</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="consumoSuplementosApp" value="" autofocus title="" required>
                </div>
            </div>
        </div>

        <!-- Tipo y cantidad -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Tipo y cantidad</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="consumoSuplementosTipoCantidadApp" value="" autofocus title="" required>
                </div>
            </div>
        </div>
        <hr>


        <h4><strong>Antescedentes Personales No Patológicos</strong></h4>

        <!-- Fumar y edad -->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">¿Fumas?</label>
            <div class="col-sm-4">
                <select class="selectboxit" name="fumar" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Edad de inicio</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="edadFumar" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <!-- Frecuencia y cantidad -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Frecuencia</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="frecuenciaFumar" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Cantidad</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="cantidadFumar" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <!-- Alcohol y frecuencia -->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">¿Tomas alcohol?</label>
            <div class="col-sm-4">
                <select class="selectboxit" name="alcohol" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Frecuencia</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="frecuenciaAlcohol" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <!-- cantidad y tipo -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Cantidad</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="cantidadAlcohol" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Tipo</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="tipoAlcohol" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>


        <!-- Drogas y frecuencia -->
        <div class="form-group">
            <label for="field-2" class="col-sm-2 control-label">¿Consumes drogas?</label>
            <div class="col-sm-4">
                <select class="selectboxit" name="drogas" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Frecuencia</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="frecuenciaDrogas" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>

        <!-- Cantidad y tipo -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Cantidad</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="cantidadDrogas" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Tipo</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-heart"></i></span>
                    <input type="text" class="form-control" name="tipoDrogas" value="" autofocus title="Este campo es obligatorio." required>
                </div>
            </div>
        </div>
        <hr>

        <!-- Antecedentes Gineco-Obstétricos -->
        <div id="DivGineObs">
            <h4><strong>Antecedentes Gineco-Obstétricos</strong></h4>

            <!-- Última mestruación -->
            <div class="form-group">
                <label for="field-1" class="col-sm-2 control-label">Edad de menarca</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-heart"></i></span>
                        <input id="edadMenarca" type="text" class="form-control" name="edadMenarca" value="" autofocus title="Este campo es obligatorio." required>
                    </div>
                </div>

                <label for="field-1" class="col-sm-2 control-label">Última mestruación</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-heart"></i></span>
                        <input id="ultimaMestruacion" type="text" class="form-control" name="ultimaMestruacion" value="" autofocus title="Este campo es obligatorio." required>
                    </div>
                </div>
            </div>

            <!-- Eres regular -->
            <div class="form-group">
                <label for="field-2" class="col-sm-2 control-label">¿Eres regular?</label>
                <div class="col-sm-4">
                    <select id="regular" class="selectboxit" name="regular" title="Seleccione una opción">
                        <option value="" selected disabled hidden></option>
                        <option value="si">Sí</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </div>


            <!-- Anticonceptivos y tipo -->
            <div class="form-group">
                <label for="field-2" class="col-sm-2 control-label">¿Usas anticonceptivos?</label>
                <div class="col-sm-4">
                    <select id="anticonceptivos" class="selectboxit" name="anticonceptivos" title="Seleccione una opción">
                        <option value="" selected disabled hidden></option>
                        <option value="si">Sí</option>
                        <option value="no">No</option>
                    </select>
                </div>

                <div id="divTipoAnticonceptivos">
                    <label for="field-1" class="col-sm-2 control-label">Tipo</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                            <input id="tipoAnticonceptivos" type="text" class="form-control" name="tipoAnticonceptivos" value="" autofocus title="Este campo es obligatorio." required>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Menopausia -->
            <div class="form-group">
                <label for="field-2" class="col-sm-2 control-label">¿Tienes menopausia?</label>
                <div class="col-sm-4">
                    <select id="menopausia" class="selectboxit" name="menopausia" title="Seleccione una opción">
                        <option value="" selected disabled hidden></option>
                        <option value="si">Sí</option>
                        <option value="no">No</option>
                    </select>
                </div>

                <div id="divFechaMenopausia">
                    <label for="field-1" class="col-sm-2 control-label">Fecha de inicio</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="entypo-heart"></i></span>
                            <input id="fechaMenopausia" type="text" class="form-control" name="fechaMenopausia" value="" autofocus title="Este campo es obligatorio." required>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div> <!-- Fin DivGineObs-->

        <h4><strong>Historial De Peso/Talla</strong></h4>

        <!-- Peso alto y bajo -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Peso más bajo (kg)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="pesoBajo" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Peso más alto (kg)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="pesoAlto" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>
        </div>

        <!-- Peso habitual y actual  -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Peso habitual (kg)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="pesoHabitual" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Peso actual (kg)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="pesoActual" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>
        </div>


        <!-- Campo habilitado solo si se seleccionó la opción de embarazo-->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Peso previo al embarazo (kg)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="pesoPrevioEmbarazo" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Peso al nacer (kg)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="pesoNacer" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>
        </div>


        <!-- Campos habilitados solo si el paciente es menor de 18 años -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Talla del padre (cm)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="tallaPadre" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Talla de la madre (cm)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="tallaMadre" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Talla esperada (cm)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="tallaEsperada" value="" autofocus title="A partir de los 21." required>
                </div>
            </div>
        </div>
        <hr>


        <h4><strong>Favor De Proporcionar Las Siguientes Medidas</strong></h4>
        <!-- Estarura y cintura -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Estatura (cm)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="estatura" value="" autofocus title="" required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Cintura (cm)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="cintura" value="" autofocus title="Con una cinta métrica, colocarla alrededor de la parte más angosta del abdomen, generalmente es arriba del ombligo." required>
                </div>
            </div>
        </div>

        <!-- Abdomen y cadera -->
        <div class="form-group">
            <label for="field-1" class="col-sm-2 control-label">Abdomen (cm)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="abdomen" value="" autofocus title="Con una cinta métrica, colocarla alrededor de la parte más angosta del abdomen, generalmente es arriba del ombligo." required>
                </div>
            </div>

            <label for="field-1" class="col-sm-2 control-label">Cadera (cm)</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="cadera" value="" autofocus title="Con una cinta métrica, colocarla alrededor de la parte más ancha, alrededor de los glúteos." required>
                </div>
            </div>
        </div>
        <hr>
        <h4><strong>Actividad Física</strong></h4>
        <!-- Actividad física -->
        <div class="form-group">
            <label for="field-2" class="col-sm-3 control-label">¿Realizas actividad física?</label>
            <div class="col-sm-9">
                <select class="selectboxit" name="actividadFisica" title="Seleccione una opción">
                    <option value="" selected disabled hidden></option>
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>
        </div>

        <!-- Define actividad física -->
        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Definela</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="definirActividadFisica" value="" autofocus title="Define cúal, duración y cada cuánto lo realizas." required>
                </div>
            </div>
        </div>

        <!-- Describe tu día -->
        <div class="form-group">
            <label for="field-2" class="col-sm-3 control-label">Describes qué haces en tu día</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-user"></i></span>
                    <input type="text" class="form-control" name="describeActividadFisica" value="" autofocus title="" required>
                </div>
            </div>
        </div>
        <hr>
        </form>
    </div>