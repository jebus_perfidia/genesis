		<!-- <?php
				$tipo = $this->db->get_where('admin', array(
					'admin_id' => $this->session->userdata('login_user_id')
				))->row()->tipo;
				?> -->
		<div class="panel panel-primary" id="charts_env" style="padding: 0px !important; margin: 0px;">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="fa fa-user-md fa-lg"></i>
					<?php echo 'Historia Clínica'; ?>
				</div>
			</div>
			<div class="panel-default">
				<div class="table-responsive">
					<table class="table datatable table-hover specialCollapse" id="table_export">
						<thead>
							<tr>
								<th style="width:30px;"></th>
								<th>
									<div><?php echo get_phrase('Fecha de consulta'); ?></div>
								</th>
								<th>
									<div><?php echo get_phrase('Estatus'); ?></div>
								</th>
								<th>
									<div><?php echo 'Nutriólogo asignado'; ?></div>
								</th>
								<th>
									<div><?php echo get_phrase('Opciones'); ?></div>
								</th>
							</tr>
						</thead>
						<tbody>
							<!-- Obtener los datos el historial del paciente de manera resumida, con el nombre del nutriólogo-->
							<?php
							$this->db->select('historiaclinica.idHC, historiaclinica.fechaHC, historiaclinica.estatusHC, historiaclinica.nutriologoId, historiaclinica.clienteId, admin.nombre,admin.aPaterno');
							$this->db->from('historiaclinica');
							$this->db->join('admin', 'historiaclinica.nutriologoId=admin.admin_id', 'left');
							/* $this->db->where('tbl_usercategory','admin'); */
							$query = $this->db->get();
							$historiasclinicas = $query->result_array();
							$counter = 1;
							foreach ($historiasclinicas as $row) :
							?>
								<tr>
									<td style="width:30px;">
										<?php echo $counter++; ?>
									</td>
									<td><?php echo $row['fechaHC']; ?></td>
									<td><?php echo $row['estatusHC']; ?></td>
									<td><?php echo $row['nombre']; ?></td>

									<td>
										<div class="btn-group">
											<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
												Acciones <span class="caret"></span>
											</button>
											<ul class="dropdown-menu dropdown-default pull-right" role="menu">

												<!-- PROFILE LINK -->
												<li>
													<a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/payment_history_profile/' . $row['aidConsulta']); ?>');">
														<i class="entypo-user"></i>
														<?php echo get_phrase('Perfil'); ?>
													</a>
												</li>


											</ul>
										</div>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>




		<!-- calling ajax form submission plugin for specific form -->
		<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/neon-custom-ajax.js'); ?>"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				//convert all checkboxes before converting datatable
				replaceCheckboxes();

				// Highlighted rows
				$("#table_export tbody input[type=checkbox]").each(function(i, el) {
					var $this = $(el),
						$p = $this.closest('tr');

					$(el).on('change', function() {
						var is_checked = $this.is(':checked');

						$p[is_checked ? 'addClass' : 'removeClass']('highlight');
					});
				});

				// convert datatable
				var datatable = $("#table_export").dataTable({
					"scrollX": true,
					"sPaginationType": "bootstrap",
					/*  "scrollX": true, */
					// "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
					// "aoColumns": [
					// 	{ "bSortable": false}, 	//0,checkbox
					// 	{ "bVisible": true},		//1,name
					// 	{ "bVisible": true},		//2,role
					// 	{ "bVisible": true},		//3,contact
					// 	{ "bVisible": true}		//4,option
					// ],
					"oTableTools": {
						"aButtons": [

							{
								"sExtends": "xls",
								"mColumns": [1, 2, ]
							},
							{
								"sExtends": "pdf",
								"mColumns": [1, 2]
							},
							{
								"sExtends": "print",
								"fnSetText": "Press 'esc' to return",
								"fnClick": function(nButton, oConfig) {
									datatable.fnSetColumnVis(0, false);
									datatable.fnSetColumnVis(3, false);
									datatable.fnSetColumnVis(6, false);

									this.fnPrint(true, oConfig);

									window.print();

									$(window).keyup(function(e) {
										if (e.which == 27) {
											datatable.fnSetColumnVis(0, true);
											datatable.fnSetColumnVis(3, true);
											datatable.fnSetColumnVis(6, true);
										}
									});
								},

							},
						]
					},

				});

				//customize the select menu
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});




			});
		</script>