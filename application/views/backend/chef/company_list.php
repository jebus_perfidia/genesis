<div class="panel panel-primary" id="charts_env">
	<div class="panel-heading">
		<div class="panel-title">
			<i class="fa fa-coffee"></i>
			<?php echo get_phrase('Alimento'); ?>
		</div>
	</div>
	<div class="panel-body">
		<table class="table  datatable" id="table_export">
			<thead>
				<tr>
					<th style="width:30px;"></th>
					<th>
						<div><?php echo get_phrase('Nombre'); ?></div>
					</th>
					<th>
						<div><?php echo 'Proporción'; ?></div>
					</th>
					<th>
						<div><?php echo get_phrase('Cantidad'); ?></div>
					</th>
					<th>
						<div><?php echo get_phrase('Tipo'); ?></div>
					</th>
					<th>
						<div><?php echo get_phrase('Opciones'); ?></div>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$counter = 1;
				$alimentos	=	$this->db->get('alimentos')->result_array();
				foreach ($alimentos as $row) :
				?>
					<tr>
						<td style="width:30px;">
							<?php echo $counter++; ?>
						</td>
						<td><?php echo $row['nombreAlimento']; ?></td>
						<td><?php echo $row['proporcionAlimento']; ?></td>
						<td><?php echo $row['cantidadAlimento']; ?></td>
						<td><?php echo $row['tipoAlimento']; ?></td>

						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
									Acción <span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-default pull-right" role="menu">

									<!-- <li>
										<a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/company_profile/' . $row['company_id']); ?>');">
											<i class="entypo-star"></i>
											<?php echo get_phrase('profile'); ?>
										</a>
									</li> -->

									<!-- EDITING LINK -->
									<li>
										<a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/company_edit/' . $row['idAlimento']); ?>');">
											<i class="entypo-pencil"></i>
											<?php echo get_phrase('Editar'); ?>
										</a>
									</li>
									<li class="divider"></li>

									<!-- DELETION LINK -->
									<li>
										<a href="#" onclick="confirm_modal('<?php echo site_url('chef/company/delete/' . $row['idAlimento']); ?>' , '<?php echo site_url('chef/reload_company_list'); ?>');">
											<i class="entypo-trash"></i>
											<?php echo get_phrase('Eliminar'); ?>
										</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>


<!-- calling ajax form submission plugin for specific form -->
<script src="<?php echo base_url('assets/js/ajax-form-submission.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/neon-custom-ajax.js'); ?>"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		//convert all checkboxes before converting datatable
		replaceCheckboxes();

		// Highlighted rows
		$("#table_export tbody input[type=checkbox]").each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');

			$(el).on('change', function() {
				var is_checked = $this.is(':checked');

				$p[is_checked ? 'addClass' : 'removeClass']('highlight');
			});
		});

		// convert datatable
		var datatable = $("#table_export").dataTable({
			"scrollX": true,
			"sPaginationType": "bootstrap",
			/*  "scrollX": true, */
			// "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
			// "aoColumns": [
			// 	{ "bSortable": false}, 	//0,checkbox
			// 	{ "bVisible": true},		//1,name
			// 	{ "bVisible": true},		//2,role
			// 	{ "bVisible": true},		//3,contact
			// 	{ "bVisible": true}		//4,option
			// ],
			"oTableTools": {
				"aButtons": [

					{
						"sExtends": "xls",
						"mColumns": [1, 2, ]
					},
					{
						"sExtends": "pdf",
						"mColumns": [1, 2]
					},
					{
						"sExtends": "print",
						"fnSetText": "Press 'esc' to return",
						"fnClick": function(nButton, oConfig) {
							datatable.fnSetColumnVis(0, false);
							datatable.fnSetColumnVis(3, false);
							datatable.fnSetColumnVis(6, false);

							this.fnPrint(true, oConfig);

							window.print();

							$(window).keyup(function(e) {
								if (e.which == 27) {
									datatable.fnSetColumnVis(0, true);
									datatable.fnSetColumnVis(3, true);
									datatable.fnSetColumnVis(6, true);
								}
							});
						},

					},
				]
			},

		});

		//customize the select menu
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});




	});
</script>