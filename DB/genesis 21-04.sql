-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-04-2020 a las 23:18:29
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mcc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `account_permission`
--

CREATE TABLE `account_permission` (
  `account_permission_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `account_permission`
--

INSERT INTO `account_permission` (`account_permission_id`, `name`, `description`) VALUES
(1, 'Manage Assigned Project', 'User can view and manage only assigned projects to him. Project status update, document upload/view, project discussion will be available to assigned projects'),
(2, 'Manage All Projects', ''),
(3, 'Manage Clients', ''),
(4, 'Manage Staffs', ''),
(5, 'Manage Payment', ''),
(6, 'Manage Assigned Support Ticket', ''),
(7, 'Manage All Support Tickets', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `account_role`
--

CREATE TABLE `account_role` (
  `account_role_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_permissions` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `nombre` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aPaterno` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aMaterno` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono1` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono2` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `domicilio` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `cedulaProfesional` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `altaSat` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `curp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `rfc` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `adminEmpresa` tinyint(1) DEFAULT NULL,
  `idEmpresa` int(11) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `owner_status` int(11) NOT NULL DEFAULT 0 COMMENT '1 owner, 0 not owner'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`admin_id`, `nombre`, `aPaterno`, `aMaterno`, `email`, `usuario`, `password`, `telefono1`, `telefono2`, `domicilio`, `tipo`, `cedulaProfesional`, `altaSat`, `curp`, `rfc`, `adminEmpresa`, `idEmpresa`, `fechaNacimiento`, `owner_status`) VALUES
(1, 'Jesús Eduardo', 'Camargo', 'Ramírez', 'jebus_perfidia@outlook.com', 'jcamargo', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '4775628520', '4772099039', 'Madre Carmen 249 10 de mayo', 'administrador', '114477841232132', '213246546542321', 'CARJ931013HGTMMS04', 'CARJ931013NR6', 0, 0, '2020-03-26', 1),
(19, 'Jesús', 'Camargo', 'Ramírez', 'eduardo', 'eduardo', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '112', '112', 'eduardo', 'nutriologo', NULL, NULL, 'eduardo', 'eduardo', 1, 2, '2020-03-12', 1),
(20, 'a', 'a', 'a', 'a', 'dani', '3f547476e0ce8e681ded188f0322a4d5e0d56ec4', '1', '1a', 'a', 'chef', NULL, NULL, 'a', 'a', 1, 1, '2020-03-12', 1),
(38, 'zxc', 'zsxdc', 'zx', 'a@a', 'zxc', 'e28f2ebe7df6baf8bd89e470dd80b12601f03231', '1234567890', '', 'zxc', 'cliente', NULL, NULL, '', '', 0, 2, '2020-03-31', 1),
(37, 'raw', 'raw', 'raw', 'jesus.eduardo.camargo@outlook.com', 'raw', '01e761956d3775dd5cb62d2a0d123539965c9c0c', '1234567891', '', 'raw', 'cliente', NULL, NULL, '', '', 0, 5, '2020-04-02', 1),
(24, 'j', 'j', 'j', 'jesus.eduardo.camargo@outlook.com', 'j', '5c2dd944dde9e08881bef0894fe7b22a5c9c4b06', '4775628520', '4775628520', 'j', 'cliente', NULL, NULL, 'CARJ931013HGTMMS04', '', 0, 1, NULL, 1),
(25, 'n', 'n', 'n', 'jesus.eduardo.camargo@outlook.com', 'n', 'd1854cae891ec7b29161ccaf79a24b00c274bdaa', '4775628520', '4775628520', 'n', 'cliente', NULL, NULL, 'CARJ931013HGTMMS04', 'CARJ931013NR6', 0, 2, NULL, 1),
(26, 'v', 'v', 'v', 'jesus.eduardo.camargo@outlook.com', 'v', '7a38d8cbd20d9932ba948efaa364bb62651d5ad4', '4775628520', '4775628520', 'v', 'encargado', NULL, NULL, '', '', 0, 0, '2020-04-07', 1),
(27, 'r', 'r', 'r', 'jesus.eduardo.camargo@outlook.com', 'r', '4dc7c9ec434ed06502767136789763ec11d2c4b7', '1234567891', '1234567891', 'r', 'encargado', NULL, NULL, '', '', 0, NULL, '2020-04-01', 1),
(35, 're', 're', 're', 're@re.com', 're', '7c4a8d09ca3762af61e59520943dc26494f8941b', '1234567891', '', 're', 'chef', NULL, NULL, '', '', 1, 8, '2020-04-07', 1),
(29, 'mario', 'mario', 'mario', 'jesus.eduardo.camargo@outlook.com', 'mario', 'addb47291ee169f330801ce73520b96f2eaf20ea', '1234567891', '1234567891', 'mario', 'nutriologo', NULL, NULL, '', '', 0, 0, '0000-00-00', 1),
(30, 'w', 'w', 'w', 'jesus.eduardo.camargo@outlook.com', 'w', 'aff024fe4ab0fece4091de044c58c9ae4233383a', '1234567891', '1234567891', '', 'staff', NULL, NULL, '', '', 0, 0, '0000-00-00', 1),
(31, 'b', 'b', 'b', 'jesus.eduardo.camargo@outlook.com', 'jcamargo1', '7c4a8d09ca3762af61e59520943dc26494f8941b', '3216549877', '3216549877', 'b', 'staff', NULL, NULL, '', '', 0, 0, '0000-00-00', 1),
(32, 'Juan', 'Camargo', 'RamIrez', 'carlos.camargo.ramirez@gmail.com', 'ccamargo', 'f9f9f9986c56b1f2be502ca8820c0290f258c793', '4775628520', '4775628520', 'Madre Carmen 249 colonia 10 de mayo León, Guanajuato', 'cliente', NULL, NULL, 'CARJ931013HGTMMS04', 'CARJ931013NR6', 1, 2, '2020-04-06', 1),
(33, 'Luis Gonzálo', 'Camargo', 'Ramírez', 'jesus.eduardo.camargo@outlook.com', 'lcamargo', '54c92e5eef8592420fb5261892ea95274d1e6108', '4775628520', '4775628520', 'Madre Carmen #249 col. 10 de mayo', 'nutriologo', NULL, NULL, 'CARJ931013HGTMMS04', 'CARJ931013NR6', 0, 0, '2020-04-01', 1),
(36, 'bea', 'bea', 'bea', 'jesus.eduardo.camargo@outlook.com', 'bea', '7c4a8d09ca3762af61e59520943dc26494f8941b', '1234569789', '', 'bea', 'cliente', NULL, NULL, '', '', 0, 5, '2020-04-02', 1),
(39, 'bnm', 'bnm', 'bnm', 'jesus.eduardo.camargo@outlook.com', 'bnm', '96528cb1e6886b8cd57f1333664d505f0be8dc57', '1234567890', '', 'bnm', 'cliente', NULL, NULL, '', '', 0, 2, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alimentos`
--

CREATE TABLE `alimentos` (
  `idAlimento` int(11) NOT NULL,
  `nombreAlimento` longtext DEFAULT NULL,
  `proporcionAlimento` varchar(50) DEFAULT NULL,
  `cantidadAlimento` double DEFAULT NULL,
  `tipoAlimento` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `alimentos`
--

INSERT INTO `alimentos` (`idAlimento`, `nombreAlimento`, `proporcionAlimento`, `cantidadAlimento`, `tipoAlimento`) VALUES
(1, 'verdura', 'taza', 1, 'verduras');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bookmark`
--

CREATE TABLE `bookmark` (
  `bookmark_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendar_event`
--

CREATE TABLE `calendar_event` (
  `calendar_event_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_timestamp` int(11) DEFAULT NULL,
  `colour` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat`
--

CREATE TABLE `chat` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` varchar(255) NOT NULL DEFAULT '',
  `to` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0rmmj56r1kk8a6e54hoka793niblvh7q', '::1', 1587429075, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373432393037353b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('15ceobal18pqcpsli9cku04e0hff3nls', '::1', 1587430122, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433303132323b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('4ka0vckl9tnd7nva1n5ou6e5nc17gt57', '::1', 1587430762, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433303736323b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('9hkn3e6lfaa21kmej26cdaf2opsoq2id', '::1', 1587431455, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433313435353b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('a0iepvptt9mugq3s45huaum65mg4jsn6', '::1', 1587503888, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373530333838383b),
('c289ofv1rt12s1gug7edir5aq155u9ih', '::1', 1587432994, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433323939343b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('eja70m4v7ig94var5kgjo3k0dp1lpffg', '::1', 1587429463, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373432393436333b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('ieg7dob9d2n2snu3ubcb5ik4ejod5qmu', '::1', 1587432496, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433323439363b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('il2ildr8s4ktkm4do6fsu6gpm294di79', '::1', 1587436116, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433363130323b61646d696e5f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a313a2231223b6e616d657c4e3b6c6f67696e5f747970657c733a353a2261646d696e223b),
('l5e6046fdtlhijrfhrb08coa28o2kfis', '::1', 1587429771, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373432393737313b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('m690jsa1e6f0ndb730e7dgglojr8r8m4', '::1', 1587430447, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433303434373b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('q8b39a0114vnndscc2kqfhgi0tlge4f4', '::1', 1587435968, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433353936383b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b),
('v2mp7mdkvhee8c5ls5nluup95qqb8v01', '::1', 1587494118, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373439333937353b61646d696e5f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a313a2231223b6e616d657c4e3b6c6f67696e5f747970657c733a353a2261646d696e223b),
('v3dqr5hc20jtju6bc2h49122ulqha67n', '::1', 1587431818, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373433313831383b6c6173745f706167657c733a35373a22687474703a2f2f6c6f63616c686f73742f67656e657369732f696e6465782e7068702f636c69656e742f7061796d656e745f686973746f7279223b636c69656e745f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a323a223332223b6e616d657c4e3b6c6f67696e5f747970657c733a363a22636c69656e74223b7461626c617c733a353a2261646d696e223b);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `client_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `skype_id` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_profile_link` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin_profile_link` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_profile_link` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_note` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `chat_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'offline'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client_pending`
--

CREATE TABLE `client_pending` (
  `client_pending_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `currency`
--

CREATE TABLE `currency` (
  `currency_id` int(11) NOT NULL,
  `currency_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_symbol` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_name` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `currency`
--

INSERT INTO `currency` (`currency_id`, `currency_code`, `currency_symbol`, `currency_name`) VALUES
(1, 'USD', '$', 'US dollar'),
(2, 'GBP', '£', 'Pound'),
(3, 'EUR', '€', 'Euro'),
(4, 'AUD', '$', 'Australian Dollar'),
(5, 'CAD', '$', 'Canadian Dollar'),
(6, 'JPY', '¥', 'Japanese Yen'),
(7, 'NZD', '$', 'N.Z. Dollar'),
(8, 'CHF', 'Fr', 'Swiss Franc'),
(9, 'HKD', '$', 'Hong Kong Dollar'),
(10, 'SGD', '$', 'Singapore Dollar'),
(11, 'SEK', 'kr', 'Swedish Krona'),
(12, 'DKK', 'kr', 'Danish Krone'),
(13, 'PLN', 'zł', 'Polish Zloty'),
(14, 'HUF', 'Ft', 'Hungarian Forint'),
(15, 'CZK', 'Kč', 'Czech Koruna'),
(16, 'MXN', '$', 'Mexican Peso'),
(17, 'CZK', 'Kč', 'Czech Koruna'),
(18, 'MYR', 'RM', 'Malaysian Ringgit');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document`
--

CREATE TABLE `document` (
  `document_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_id` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_template`
--

CREATE TABLE `email_template` (
  `email_template_id` int(11) NOT NULL,
  `task` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `instruction` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `email_template`
--

INSERT INTO `email_template` (`email_template_id`, `task`, `subject`, `body`, `instruction`) VALUES
(1, 'new_project_opening', 'New project created', '<span>\r\n<div>Hello, [CLIENT_NAME], <br>we have created a new project with your account.<br><br>Project name : [PROJECT_NAME]<br>Please follow the link below to view status and updates of the project.<br>[PROJECT_LINK]</div></span>', ''),
(2, 'new_client_account_opening', 'Client account creation', '<span><div>Hi [CLIENT_NAME],</div></span>Your client account is created !<span>Please login to your client account panel here :&nbsp;<br></span>[SYSTEM_URL]<br>Login credential :<br>email : [CLIENT_EMAIL]<br>password : [CLIENT_PASSWORD]', ''),
(3, 'new_staff_account_opening', 'Staff account creation', '<span>\n<div><div>Hi [STAFF_NAME],</div>Your staff account is created !&nbsp;Please login to your staff account panel here :&nbsp;<br>[SYSTEM_URL]<br>Login credential :<br>email : [STAFF_EMAIL]<br>password : [STAFF_PASSWORD]<br></div></span>', ''),
(4, 'payment_completion_notification', 'Payment completion notification', '<span>\n<div>Your payment of invoice [INVOICE_NUMBER] is completed.<br>You can review your payment history here :<br>[SYSTEM_PAYMENT_URL]</div></span>', ''),
(5, 'new_support_ticket_notify_admin', 'New support ticket notification', 'Hi [ADMIN_NAME],<br>A new support ticket is submitted. Ticket code : [TICKET_CODE]<br><br>Review all opened support tickets here :<br>[SYSTEM_OPENED_TICKET_URL]<br>', ''),
(6, 'support_ticket_assign_staff', 'Support ticket assignment notification', 'Hi [STAFF_NAME],<br>A new support ticket is assigned. Ticket code : [TICKET_CODE]<br><br>Review all opened support tickets here :<br>[SYSTEM_OPENED_TICKET_URL]', ''),
(7, 'new_message_notification', 'New message notification.', 'A new message has been sent by [SENDER_NAME].<br><br><span class=\"wysiwyg-color-silver\">[MESSAGE]<br></span><br><span>To reply to this message, login to your account :<br></span>[SYSTEM_URL]', ''),
(8, 'password_reset_confirmation', 'Password reset notification', 'Hi [NAME],<br>Your password is reset. New password : [NEW_PASSWORD]<br>Login here with your new password :<br>[SYSTEM_URL]<br><br>You can change your password after logging in to your account.', ''),
(9, 'new_client_account_confirm', 'New Client account confirmed', '<span><div>Hi [CLIENT_NAME],</div></span>Your client account is confirmed!<span>Please login to your client account panel here :&nbsp;<br></span>[SYSTEM_URL]<br>', ''),
(10, 'new_admin_account_creation', 'Admin Account Creation', '<span><div>Hi [ADMIN_NAME],</div></span>Your admin account is created !<span>Please login to your admin account panel here :&nbsp;<br></span>[SYSTEM_URL]<br>Login credential :<br>email : [ADMIN_EMAIL]<br>password : [ADMIN_PASSWORD]', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `idEmpresa` int(11) NOT NULL,
  `nombreEmpresa` longtext DEFAULT NULL,
  `razonSocial` longtext NOT NULL,
  `domicilio` longtext DEFAULT NULL,
  `rfc` longtext DEFAULT NULL,
  `tiempoContrato` date DEFAULT NULL,
  `convenio` longtext DEFAULT NULL,
  `estatus` longtext DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`idEmpresa`, `nombreEmpresa`, `razonSocial`, `domicilio`, `rfc`, `tiempoContrato`, `convenio`, `estatus`, `admin_id`) VALUES
(1, 'Rollos y plásticos ab', '', 'Bien lejos', 'RYPDL963587D1RR', '2020-03-12', '213213546523', 'activa', 19),
(2, 'Compussel', '', 'República de chile 701 B', 'FOGC671129CD1', '2020-03-11', '998877445853', 'deshabilitada', 19),
(5, 'Luxma', '', 'Blvd. Valtierra 2890 ', 'LUXO09999', '2020-03-13', NULL, 'activa', 29),
(7, 'San Jorge', 'La mera ñonga', 'Bien Lejos Papu', 'CARJ931013NR6', '2020-04-14', NULL, 'activa', 19),
(8, 'Like Agua', 'Like Agua de León S.A de C.V.', 'Madre Patria 215 colonia 10 de mayo', 'CARJ931013NR6', '2020-04-02', NULL, 'activa', 33);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expense_category`
--

CREATE TABLE `expense_category` (
  `expense_category_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupoaportenutrimental`
--

CREATE TABLE `grupoaportenutrimental` (
  `idGrupoAporteNutrimental` int(11) NOT NULL,
  `grupo` longtext DEFAULT NULL,
  `subGrupo` longtext DEFAULT NULL,
  `energia` int(11) DEFAULT NULL,
  `protenia` int(11) DEFAULT NULL,
  `lipidos` int(11) DEFAULT NULL,
  `hidratosCarbono` varchar(10) DEFAULT NULL,
  `ConteoHidratosCarbono` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historiaclinica`
--

CREATE TABLE `historiaclinica` (
  `idHC` int(11) NOT NULL,
  `fechaHC` date DEFAULT NULL,
  `generoHC` varchar(50) DEFAULT NULL,
  `embarazoHC` varchar(50) DEFAULT NULL,
  `semEmbarazoHC` varchar(50) DEFAULT NULL,
  `estadoCivilHC` varchar(50) DEFAULT NULL,
  `escolaridadHC` varchar(50) DEFAULT NULL,
  `actLabHC` varchar(50) DEFAULT NULL,
  `estadoHC` varchar(50) DEFAULT NULL,
  `ciudadHC` varchar(50) DEFAULT NULL,
  `antHerFamHC` longtext DEFAULT NULL,
  `antPerPatHC` longtext DEFAULT NULL,
  `antPadActHC` longtext DEFAULT NULL,
  `antPerNoPatHC` longtext NOT NULL,
  `antGinObsHC` longtext DEFAULT NULL,
  `pesMasBajHC` double DEFAULT NULL,
  `pesMasAltHC` double DEFAULT NULL,
  `pesHabHC` double DEFAULT NULL,
  `pesActHC` double DEFAULT NULL,
  `embPesMasBajHC` double DEFAULT NULL,
  `embPesMasAltHC` double DEFAULT NULL,
  `embPesHabHC` double DEFAULT NULL,
  `embPesActHC` double DEFAULT NULL,
  `estaturaHC` double DEFAULT NULL,
  `cinturaHC` double DEFAULT NULL,
  `abdomenHC` double DEFAULT NULL,
  `caderaHC` double DEFAULT NULL,
  `reActFisHC` longtext DEFAULT NULL,
  `defAcFisHC` longtext DEFAULT NULL,
  `desDia` longtext DEFAULT NULL,
  `estatusHC` varchar(20) NOT NULL,
  `nutriologoId` int(11) NOT NULL,
  `clienteId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `historiaclinica`
--

INSERT INTO `historiaclinica` (`idHC`, `fechaHC`, `generoHC`, `embarazoHC`, `semEmbarazoHC`, `estadoCivilHC`, `escolaridadHC`, `actLabHC`, `estadoHC`, `ciudadHC`, `antHerFamHC`, `antPerPatHC`, `antPadActHC`, `antPerNoPatHC`, `antGinObsHC`, `pesMasBajHC`, `pesMasAltHC`, `pesHabHC`, `pesActHC`, `embPesMasBajHC`, `embPesMasAltHC`, `embPesHabHC`, `embPesActHC`, `estaturaHC`, `cinturaHC`, `abdomenHC`, `caderaHC`, `reActFisHC`, `defAcFisHC`, `desDia`, `estatusHC`, `nutriologoId`, `clienteId`) VALUES
(1, '2020-04-01', 'masculino', 'no', NULL, 'soltero', 'licenciatura', 'soporte tecnico', 'guanajuato', 'leon', 'ninguno', 'ninguno', 'ninguno', 'ninguno', 'ninguno', 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 'ninguno', 'ninguno', 'ninguno', 'activo', 1, 2),
(2, '2020-04-01', 'masculino', 'no', NULL, 'soltero', 'licenciatura', 'soporte tecnico', 'guanajuato', 'leon', 'ninguno', 'ninguno', 'ninguno', 'ninguno', 'ninguno', 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 'ninguno', 'ninguno', 'ninguno', 'activo', 1, 2),
(3, '0000-00-00', 'masculino', 'si', '33', 'soltero', NULL, '33', '33', '33', '33', '33', '33', '33', '33', 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 'si', '33', '33', '', 0, 0),
(4, '0000-00-00', 'masculino', 'si', '666', 'soltero', NULL, '666', '666', '666', '666', '666', '666', '666', '666', 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 'si', '666', '666', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `invoice_number` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_entries` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `due_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'paid or unpaid',
  `vat_percentage` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_amount` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `message`
--

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL,
  `message_thread_code` longtext DEFAULT NULL,
  `message` longtext DEFAULT NULL,
  `sender` longtext DEFAULT NULL,
  `timestamp` longtext DEFAULT NULL,
  `read_status` int(11) NOT NULL DEFAULT 0 COMMENT '0 unread 1 read'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `message_thread`
--

CREATE TABLE `message_thread` (
  `message_thread_id` int(11) NOT NULL,
  `message_thread_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `reciever` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_message_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `note`
--

CREATE TABLE `note` (
  `note_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_create` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp_last_update` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_by` int(11) DEFAULT NULL,
  `visible_for` int(11) NOT NULL DEFAULT 1 COMMENT '1-all, 2-staff, 3-client',
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `project_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT 0,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_method` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `expense_category_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE `project` (
  `project_id` int(11) NOT NULL,
  `project_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `demo_url` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_category_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `staffs` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `budget` int(11) NOT NULL DEFAULT 0,
  `timer_status` int(11) NOT NULL DEFAULT 0 COMMENT '1 running 0stopped',
  `timer_starting_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_time_spent` int(11) NOT NULL DEFAULT 0 COMMENT 'second',
  `progress_status` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp_start` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp_end` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 for running, 0 for archived',
  `project_note` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_bug`
--

CREATE TABLE `project_bug` (
  `project_bug_id` int(11) NOT NULL,
  `project_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `file` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0 for pending, 1 for solved',
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_staff` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_category`
--

CREATE TABLE `project_category` (
  `project_category_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_file`
--

CREATE TABLE `project_file` (
  `project_file_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `visibility_client` int(11) NOT NULL DEFAULT 1 COMMENT '1visible 0hidden',
  `visibility_staff` int(11) NOT NULL DEFAULT 1 COMMENT '1visible 0hidden',
  `size` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `uploader_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `timestamp_upload` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_message`
--

CREATE TABLE `project_message` (
  `project_message_id` int(11) NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `date` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message_file_name` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_milestone`
--

CREATE TABLE `project_milestone` (
  `project_milestone_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT 0,
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0 for unpaid, 1 for paid',
  `note` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_task`
--

CREATE TABLE `project_task` (
  `project_task_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `complete_status` int(11) DEFAULT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp_start` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp_end` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `task_color` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_timesheet`
--

CREATE TABLE `project_timesheet` (
  `project_timesheet_id` int(11) NOT NULL,
  `start_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quote`
--

CREATE TABLE `quote` (
  `quote_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `files` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quote_message`
--

CREATE TABLE `quote_message` (
  `quote_message_id` int(11) NOT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedule`
--

CREATE TABLE `schedule` (
  `schedule_id` int(11) NOT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `type` longtext DEFAULT NULL,
  `description` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`settings_id`, `type`, `description`) VALUES
(1, 'system_name', 'Génesis'),
(2, 'system_title', 'Génesis'),
(3, 'address', 'Sydney, Australia'),
(4, 'phone', '2560565'),
(5, 'paypal_email', ''),
(6, 'currency', 'usd'),
(7, 'system_email', 'admin@example.com'),
(8, 'buyer', '[ your-codecanyon-username-here ]'),
(9, 'purchase_code', '3f848de3-0cb0-488f-9165-c8e1fbaf1901'),
(10, 'language', 'english'),
(11, 'text_align', 'left-to-right'),
(12, 'system_currency_id', '16'),
(13, 'theme', NULL),
(14, 'stripe_publishable_key', ''),
(15, 'stripe_api_key', ''),
(16, 'dropbox_data_app_key', NULL),
(17, 'skin_colour', 'default'),
(18, 'paypal_type', 'live'),
(27, 'smtp_host', 'ssl://smtp.googlemail.com'),
(24, 'paypal', '[{\"active\":\"1\",\"mode\":\"sandbox\",\"sandbox_client_id\":\"AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R\",\"production_client_id\":\"SomeId\"}]'),
(25, 'stripe_keys', '[{\"active\":\"1\",\"testmode\":\"on\",\"public_key\":\"pk_test_c6VvBEbwHFdulFZ62q1IQrar\",\"secret_key\":\"sk_test_9IMkiM6Ykxr1LCe2dJ3PgaxS\",\"public_live_key\":\"pk_live_xxxxxxxxxxxxxxxxxxxxxxxx\",\"secret_live_key\":\"sk_live_xxxxxxxxxxxxxxxxxxxxxxxx\"}]'),
(26, 'version', '4.3'),
(28, 'smtp_port', '465'),
(34, 'protocol', 'smtp'),
(35, 'smtp_user', 'jebusxuy@gmail.com'),
(36, 'smtp_pass', 'Your real password');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_role_id` int(11) DEFAULT NULL,
  `phone` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `skype_id` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_profile_link` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_profile_link` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin_profile_link` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `chat_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'offline'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `support_canned_message`
--

CREATE TABLE `support_canned_message` (
  `support_canned_message_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tamañoequivalente`
--

CREATE TABLE `tamañoequivalente` (
  `idTamañoEquivalente` int(11) NOT NULL,
  `alimentoReferencia` longtext DEFAULT NULL,
  `PorcionTamaño` longtext DEFAULT NULL,
  `aporteProtenia` double DEFAULT NULL,
  `AporteLipidos` double DEFAULT NULL,
  `AporteHidratosCarbono` double DEFAULT NULL,
  `idGrupoAporteNutrimental` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `team_subtask`
--

CREATE TABLE `team_subtask` (
  `team_subtask_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_task_id` int(11) DEFAULT NULL,
  `subtask_status` int(11) NOT NULL DEFAULT 1 COMMENT '1 for incomplete , 0 for complete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `team_task`
--

CREATE TABLE `team_task` (
  `team_task_id` int(11) NOT NULL,
  `task_title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `task_note` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_staff_ids` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `due_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `task_status` int(11) NOT NULL DEFAULT 1 COMMENT '0 for archived, 1 for running'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `team_task_file`
--

CREATE TABLE `team_task_file` (
  `team_task_file_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_task_id` int(11) DEFAULT NULL,
  `upload_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `ticket_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticket_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'opened closed',
  `priority` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `assigned_staff_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket_message`
--

CREATE TABLE `ticket_message` (
  `ticket_message_id` int(11) NOT NULL,
  `ticket_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_type` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `todo`
--

CREATE TABLE `todo` (
  `todo_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `account_permission`
--
ALTER TABLE `account_permission`
  ADD PRIMARY KEY (`account_permission_id`);

--
-- Indices de la tabla `account_role`
--
ALTER TABLE `account_role`
  ADD PRIMARY KEY (`account_role_id`);

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indices de la tabla `alimentos`
--
ALTER TABLE `alimentos`
  ADD PRIMARY KEY (`idAlimento`);

--
-- Indices de la tabla `bookmark`
--
ALTER TABLE `bookmark`
  ADD PRIMARY KEY (`bookmark_id`);

--
-- Indices de la tabla `calendar_event`
--
ALTER TABLE `calendar_event`
  ADD PRIMARY KEY (`calendar_event_id`);

--
-- Indices de la tabla `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indices de la tabla `client_pending`
--
ALTER TABLE `client_pending`
  ADD PRIMARY KEY (`client_pending_id`);

--
-- Indices de la tabla `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indices de la tabla `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indices de la tabla `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`document_id`);

--
-- Indices de la tabla `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`idEmpresa`);

--
-- Indices de la tabla `expense_category`
--
ALTER TABLE `expense_category`
  ADD PRIMARY KEY (`expense_category_id`);

--
-- Indices de la tabla `grupoaportenutrimental`
--
ALTER TABLE `grupoaportenutrimental`
  ADD PRIMARY KEY (`idGrupoAporteNutrimental`);

--
-- Indices de la tabla `historiaclinica`
--
ALTER TABLE `historiaclinica`
  ADD PRIMARY KEY (`idHC`);

--
-- Indices de la tabla `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indices de la tabla `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indices de la tabla `message_thread`
--
ALTER TABLE `message_thread`
  ADD PRIMARY KEY (`message_thread_id`);

--
-- Indices de la tabla `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`note_id`);

--
-- Indices de la tabla `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indices de la tabla `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indices de la tabla `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indices de la tabla `project_bug`
--
ALTER TABLE `project_bug`
  ADD PRIMARY KEY (`project_bug_id`);

--
-- Indices de la tabla `project_category`
--
ALTER TABLE `project_category`
  ADD PRIMARY KEY (`project_category_id`);

--
-- Indices de la tabla `project_file`
--
ALTER TABLE `project_file`
  ADD PRIMARY KEY (`project_file_id`);

--
-- Indices de la tabla `project_message`
--
ALTER TABLE `project_message`
  ADD PRIMARY KEY (`project_message_id`);

--
-- Indices de la tabla `project_milestone`
--
ALTER TABLE `project_milestone`
  ADD PRIMARY KEY (`project_milestone_id`);

--
-- Indices de la tabla `project_task`
--
ALTER TABLE `project_task`
  ADD PRIMARY KEY (`project_task_id`);

--
-- Indices de la tabla `project_timesheet`
--
ALTER TABLE `project_timesheet`
  ADD PRIMARY KEY (`project_timesheet_id`);

--
-- Indices de la tabla `quote`
--
ALTER TABLE `quote`
  ADD PRIMARY KEY (`quote_id`);

--
-- Indices de la tabla `quote_message`
--
ALTER TABLE `quote_message`
  ADD PRIMARY KEY (`quote_message_id`);

--
-- Indices de la tabla `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`schedule_id`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indices de la tabla `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indices de la tabla `support_canned_message`
--
ALTER TABLE `support_canned_message`
  ADD PRIMARY KEY (`support_canned_message_id`);

--
-- Indices de la tabla `tamañoequivalente`
--
ALTER TABLE `tamañoequivalente`
  ADD PRIMARY KEY (`idTamañoEquivalente`);

--
-- Indices de la tabla `team_subtask`
--
ALTER TABLE `team_subtask`
  ADD PRIMARY KEY (`team_subtask_id`);

--
-- Indices de la tabla `team_task`
--
ALTER TABLE `team_task`
  ADD PRIMARY KEY (`team_task_id`);

--
-- Indices de la tabla `team_task_file`
--
ALTER TABLE `team_task_file`
  ADD PRIMARY KEY (`team_task_file_id`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indices de la tabla `ticket_message`
--
ALTER TABLE `ticket_message`
  ADD PRIMARY KEY (`ticket_message_id`);

--
-- Indices de la tabla `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`todo_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `account_permission`
--
ALTER TABLE `account_permission`
  MODIFY `account_permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `account_role`
--
ALTER TABLE `account_role`
  MODIFY `account_role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `alimentos`
--
ALTER TABLE `alimentos`
  MODIFY `idAlimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `bookmark`
--
ALTER TABLE `bookmark`
  MODIFY `bookmark_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `calendar_event`
--
ALTER TABLE `calendar_event`
  MODIFY `calendar_event_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `client_pending`
--
ALTER TABLE `client_pending`
  MODIFY `client_pending_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `currency`
--
ALTER TABLE `currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `document`
--
ALTER TABLE `document`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `email_template`
--
ALTER TABLE `email_template`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `idEmpresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `expense_category`
--
ALTER TABLE `expense_category`
  MODIFY `expense_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `grupoaportenutrimental`
--
ALTER TABLE `grupoaportenutrimental`
  MODIFY `idGrupoAporteNutrimental` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historiaclinica`
--
ALTER TABLE `historiaclinica`
  MODIFY `idHC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `message_thread`
--
ALTER TABLE `message_thread`
  MODIFY `message_thread_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `note`
--
ALTER TABLE `note`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project`
--
ALTER TABLE `project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project_bug`
--
ALTER TABLE `project_bug`
  MODIFY `project_bug_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project_category`
--
ALTER TABLE `project_category`
  MODIFY `project_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project_file`
--
ALTER TABLE `project_file`
  MODIFY `project_file_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project_message`
--
ALTER TABLE `project_message`
  MODIFY `project_message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project_milestone`
--
ALTER TABLE `project_milestone`
  MODIFY `project_milestone_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project_task`
--
ALTER TABLE `project_task`
  MODIFY `project_task_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project_timesheet`
--
ALTER TABLE `project_timesheet`
  MODIFY `project_timesheet_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `quote`
--
ALTER TABLE `quote`
  MODIFY `quote_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `quote_message`
--
ALTER TABLE `quote_message`
  MODIFY `quote_message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `schedule`
--
ALTER TABLE `schedule`
  MODIFY `schedule_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `support_canned_message`
--
ALTER TABLE `support_canned_message`
  MODIFY `support_canned_message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tamañoequivalente`
--
ALTER TABLE `tamañoequivalente`
  MODIFY `idTamañoEquivalente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `team_subtask`
--
ALTER TABLE `team_subtask`
  MODIFY `team_subtask_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `team_task`
--
ALTER TABLE `team_task`
  MODIFY `team_task_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `team_task_file`
--
ALTER TABLE `team_task_file`
  MODIFY `team_task_file_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ticket_message`
--
ALTER TABLE `ticket_message`
  MODIFY `ticket_message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `todo`
--
ALTER TABLE `todo`
  MODIFY `todo_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
